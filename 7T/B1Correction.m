% Example of Normalisation of Combined mode images
% In this example we work on T2w 3D SPACE images
% All images have been reformat with the same FOVz and orientation before procession!

% SPACE image extraction===================================================

pathname = 'C:\Users\EP247664\Documents\MATLAB\Sensitivity map\Test_EP\';

[b1mapIMG,b1mapHDR] = readNIFTI(fullfile(pathname,'bp160018_V01_b1map.nii'));
[dpIMG,dpHDR] = readNIFTI(fullfile(pathname,'bp160018_V01_dp_gre.nii'));
[maskIMG,maskHDR] = readNIFTI(fullfile(pathname,'bp160018_V01_t1_mask_2dp.nii'));

FittedB1Maps = Fitpoly({dpIMG}, maskIMG, 8, 1);
SensitivityMap=FittedB1Maps{1};
size(SensitivityMap)

CorrSensitivityMap=SensitivityMap./abs(b1mapIMG).*isfinite(SensitivityMap./abs(b1mapIMG)).*maskIMG.*threshold(SensitivityMap./abs(b1mapIMG),0,0).*threshold(SensitivityMap./abs(b1mapIMG),2,1);


writeNIFTI(fullfile(pathname,'bp160018_V01_reception_profile.nii'),CorrSensitivityMap,b1mapHDR);