function trk_writemeanresults(meantrack_file,effects_file,trackname, side,outfile)
%% Open a mean track and write results from R analysis stored in effects_file
%
%   trk_writemeanresults(meantrack_file,effects_file,trackname, side,outfile)
%
%   meantrack_file : track_vis mean file (normally saved using alongtracks
%   code
%   effects_file:  txt file saved after alongtracts R code, with pvalues, t
%   and corrected pvalues for tracks
%   trackname: the name of the track the in effects_file
%   side : the hemisphere side on the effects file
%  outfile: the output file name .trk





%% Load mean track
[header tracks] = trk_read(meantrack_file);
nbPoints=tracks(1).nPoints;

% Verify the number of tracks
if header.n_count>2
    warning('TRK:WrongNumber',' -- WARNING!! This is probably not a mean trakc!!!\n Too many tracks %d\n\n',ncount);
    
end

%% Load statistics
geffects=importdata(effects_file);

%% Create output values
checkpoints=zeros(nbPoints,1).*NaN;
tvalue     =zeros(nbPoints,1);
pvalue     =zeros(nbPoints,1);
pvaluecorr =zeros(nbPoints,1);


%% Find the track in the results

% read all the group effects
for l=1:size(geffects.data,1)
    
    % take side and trkname for each result
    % l+1 because there is the header line
    lside=char(geffects.textdata(l+1,2));
    ltrkname=char(geffects.textdata(l+1,1));
    
    % Get numeric values of the point
    point=geffects.data(l,1);
    tval=geffects.data(l,5);
    pval=geffects.data(l,6);
    if size(geffects.data,2)==7
        pvalcorr=geffects.data(l,7);
    else
        pvalcorr=1;
    end
        
    % check for side
    if ~ strcmp(side ,lside)
        continue
    end
    
    % check for trkname
    if ~ strcmp(trackname,ltrkname)
        continue
    end
    % Assing values
    checkpoints(point)=point;
    tvalue(point)=tval;
    pvalue(point)=pval;
    pvaluecorr(point)=pvalcorr;
    fprintf('%s,%s :: %1.3f\t%1.3f\t%1.3f\n',ltrkname,lside,tval,pval,pvalcorr)    
end

% Check there are no nans in the results
if sum(isnan(tvalue)) >0
    error(' -- Point %d ismissing check the size of the meantrack\n', find(isnan(checkpoints)));
end

%% Write the new mean track

% change structure of trakcs
otrk=trk_restruc(tracks);
% split meanfa from values
track_mean=otrk(:,1:3,1);
meanfa=[];
if size(otrk,2)>3
    meanfa    =otrk(:,4,1);
end

% combine track mean ans the scalar mean across tracks
track_mean_sc     = [track_mean meanfa tvalue pvalue pvaluecorr];
track_mean_sc(1:2,:,2) = [0 0 0   0 -10 0 0;  %min
                          0 0 0.1 1 10 1 1]; %max

%                          0 0 0.1 1]; %max
track_mean_sc_str = trk_restruc(track_mean_sc);
% copy original header, changing the number of fibers
header_mean_sc         = header;
header_mean_sc.n_scalars = 4;
header_mean_sc.n_count = 2;
header_mean_sc.n_properties= 0;
header_mean_sc.scalar_name(1,1:6)='meanfa';
header_mean_sc.scalar_name(2,1:6)='tvalue';
header_mean_sc.scalar_name(3,1:6)='pvalue';
header_mean_sc.scalar_name(4,1:8)='pvalcorr';
%% write the file
trk_write(header_mean_sc, track_mean_sc_str, outfile);


