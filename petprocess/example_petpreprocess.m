% Example of processing pet data

p=genpath('/home/dgarcia/src/cenir_irm/MATTIA_SOFTWARE/');
addpath(p);
clear p
%% Select all patients

outdir='./process/';
mkdir(outdir)

ids   =get_subdir_regex('../../data/raw',['.*']);
visits=get_subdir_regex(ids,['V01']);
dpa   =get_subdir_regex(visits,['^dpa']);
pets   =get_subdir_regex_files(dpa,['dpa.nii']);

%%
for x=1:size(char(pets),1)
    
    % create output folder and names
    petfile=char(pets(x));
    fprintf(' -- Doing %s \n',petfile);
    [path,name,ext]=fileparts(petfile);
    if numel(ext)==3
        system(sprintf('gunzip %s',petfile))
        [~,name,ext]=fileparts(name);
        petfile=fullfile(path,[name ext]);
    end
    [path,seq]=fileparts(path);
    [path,visit]=fileparts(path);
    [path,sid]=fileparts(path);

    pdir=fullfile(outdir,sid,visit)

    
    mkdir(fullfile(outdir,sid));
    mkdir(pdir);
    
    
    %% Smooth images
    smooth=fullfile(pdir,[name '_smooth' ext]);
    if ~ exist(smooth,'file')
        spm_smooth(petfile,smooth,[3 3 3]);
    else
        fprintf(' -- Skipping smooth %s \n',sid);
    end
    
    %% Normalize
    norm  =fullfile(pdir,[name '_norm' ext]);
    if ~exist(norm,'file')
        system(sprintf('fslmaths %s -inm 100 %s',smooth,norm));
        if exist([norm '.gz'],'file')
            system(sprintf('gunzip %s.gz',norm))
        end
    else
        fprintf(' -- Skipping norm %s \n',sid);
    end
    %% Realign
    fr_fixed =[ 1: 7];
    fr_moved =[ 8:27];
    [rfile,~,rother]=petRealign(norm,fr_fixed,fr_moved,smooth);

    
    %% Compute partial mean only moving frames?
    meanfile= fullfile(pdir,['r' name '_mean' ext]);
    if ~exist(meanfile)
        partialMean(rother,fr_moved,meanfile);
    else
        fprintf(' -- Skipping mean %s \n',sid);
    end
    
    %% Compute partial mean only moving frames?

    %% Reslice image without smoothing
    [~,~,rother]=petRealign(norm,fr_fixed,fr_moved,petfile);
    fr_suv =[ 23:27];
    suvfile= fullfile(pdir,['r' name '_suv' ext]);
    if ~exist(meanfile)
        partialMean(rother,fr_moved,suvfile);
    else
        fprintf(' -- Skipping suv %s \n',sid);
    end
end
