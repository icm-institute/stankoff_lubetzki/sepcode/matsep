function denoised=temporal_nlm_sd_perm(permPET, mask, beta,searchsize,tzero,tconst,mpet)
% DEPRECATED: included in tnlm_csd:
% Uses a constant sd to normalize frames
if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end
if ~exist('mpet','var')
    %% Create average image
    fsize=4;
    mpet = convn(permute(permPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
    mpet = single(permute(mpet,[4 1 2 3]));
    
end

%% 
dims   =size(permPET);

% output image
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 

%% Start processing voxels

for k=1:dims(4) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(3)
for i=1:dims(2)
    
    if mask(i,j,k)==0
        continue
    end
    
    tac =permPET(:,i,j,k);
    mtac= mpet(:,i,j,k);
    sdinv  = 1.0 ./(mtac .* tconst);
    

    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(2),i+searchsize); 
    Jmax=min(dims(3),j+searchsize);
    Kmax=min(dims(4),k+searchsize);
    %% First: estimate min distance
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && j==y && k==z
            continue
        end

        %d=sum(sum(sum(sum( sigma2(tzero:end) .* ((tac(tzero:end)-dynPET(x,y,z,tzero:end)-mtac(tzero:end)+mpet(x,y,z,tzero:end)).^2) ) ) ));
        d=((tac - permPET(:,x,y,z) - mtac + mpet(:,x,y,z)).^2).*sdinv;
        tmp=sum(d(tzero:end));
        if tmp < mindist
            mindist=tmp;
        end

    end
    end
    end

    %% Second: Apply weights
    wmax=0;
    dtac=zeros(dims(1),1);
    wmean=0;

    betamin=beta*mindist;
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && j==y && k==z
            continue
        end

        d=((tac - permPET(:,x,y,z)).^2)  .*sdinv;
        
        w=exp(-sum(d(tzero:end))./betamin);
        if w > wmax
            wmax=w;
        end

        dtac =dtac+ w .*  permPET(:,x,y,z);
        wmean=wmean + w;
        
    end
    end
    end
        
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) ./(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end

