function FMZQuantification2D(vfile,sujid,outdir,rawdir,mask)
%% Quantification of the Flumazenil pet image
% Input : 
% - vfile : pet image in .v or .nii/.nii.gz
% - sujid : label of your subject like C_XXX
% - outdir : directory where you want to store outputs
% default - if no outdir provided, outputs will be stored in the same
% directory as pet input
% - rawdir : raw directory containing Template directory with informations
% about pons on MNI for correction
% Output:
% - bmax : Quantification of changes in  benzodiazepine receptor density
% - kd : apparent FMZ equilibrium dissociation constante
    
    %% Test part
%     vfile = '/export/dataCENIR/users/emilie.poirion/FLUMATEP2/data/process/P_ABCHO/V01/fmzpreproc/P_ABCHO_V01_fmz_movcorr.nii.gz';
%     sujid = 'P_ABCHO_V01';
%     outdir = '/export/dataCENIR/users/emilie.poirion/FLUMATEP2/data/process/P_ABCHO/V01/fmzprocess/';
%     rawdir ='/export/dataCENIR/users/emilie.poirion/FLUMATEP2/data/raw';
%     mask = '/export/dataCENIR/users/emilie.poirion/FLUMATEP2/data/process/P_ABCHO/V01/fmzROI/P_ABCHO_V01_brainmask_to_fmz.nii.gz';
    
    disp('');
    disp(' ---------------------------------------- ');
    fprintf('Start quantification for %s \n',sujid);
    disp(' ---------------------------------------- ');
    disp('');
    tic;
    %% Input files
    [petv,pethd] = readNIFTI(char(vfile),0);
    [brainmask,maskhd] = readNIFTI(char(mask),0);
    brainmask = logical(brainmask);
    bmaxf = fullfile(char(outdir),strcat(sujid,'_fmz_bmax_2D.nii.gz'));
    kdf = fullfile(char(outdir),strcat(sujid,'_fmz_kd_2D.nii.gz'));
    
    CVbmaxf = fullfile(char(outdir),strcat(sujid,'_fmz_bmax_CV_2D.nii.gz'));
    CVkdf = fullfile(char(outdir),strcat(sujid,'_fmz_kd_CV_2D.nii.gz'));
    
    dosef = get_subdir_regex_files(char(regexprep(regexprep(outdir,'fmzprocess','fmz'),'process','raw')),'.*infos.dat');
    dose = load(char(dosef));
    ponsf = get_subdir_regex_files(outdir,'.*pons.*values.dat');
    pons = load(char(ponsf));
    ponscorrdir = get_subdir_regex(rawdir,'Template');
    ponscorr = get_subdir_regex_files(ponscorrdir,'corr_pons.dat');
    pons_corr = load(char(ponscorr));

    %% Informations from input files
    ras = dose(1);
    doseC = dose(2);
    doseF = dose(3);
    rap = (doseC + doseF)/doseC;

    %% Scatchard Method
    
    % Lissage du pons mesuré
    betamin     = 0.0001;
    betamax     = 0.5;
    ncomp       = 1000;
    betagrid    = logspace(log10(betamin),log10(betamax),ncomp)';
    betagrid(1) = 0;  
    % matrix construction
    G = zeros(length(pons(:,1)),ncomp);
    for j = 1 : ncomp
        G(:,j) = exp(-betagrid(j)*pons(:,1));
    end
    % weights
    w     = sqrt(abs(1./(pons(:,2).*exp(0.034*pons(:,1)))));
    GW    = G .* repmat(w,1,size(G,2));
    ponsW = pons(:,2).*w;
    % non-negative estimation
    B = lsqnonneg(GW,ponsW);    
    % fitted pons
    pons(:,2) = G*B;
    
    % Correction of pons values
    time = pons(:,1);
    F = pons(:,2);
    per_corr = interp1(pons_corr(:,1),pons_corr(:,2),time(:));
    F = ( 1 - per_corr ).*F;

    % RAS Correction
    petv = petv./ras;
    petv = petv.*rap;
    petv = petv/37.0;
    F = F./ras;
    F = F.*rap;

    % Parameter Calculation
    m_debut = 3;
    
    s1 = warning('error','MATLAB:nearlySingularMatrix');
	s2 = warning('error','MATLAB:singularMatrix');
    
    data2D = conv4Dto2D(petv,brainmask);
    [nt,nx] = size(data2D);
    
    %% Initialization of the initial parameter
    xo = [-0.4, 15];
    data2D_mean = mean(data2D,2);
    data2D_mean = data2D_mean -F;
    matt(:,1) = data2D_mean(m_debut:end);
    matt(:,2) = data2D_mean(m_debut:end) ./ F(m_debut:end);
    f = @(par)funps_residual(par,matt);
    [xo,RESNORM,RESIDUAL,EXITFLAG,OUTPUT,LAMBDA,JACOBIAN] = lsqnonlin(f,xo,[],[],optimset('MaxIter',500,'MaxFunEvals',5000,'Display','off','TolX',1e-2));
    
    bmax = zeros(nx,1);
    kd = zeros(nx,1);
    CVbmax = zeros(nx,1);
    CVkd = zeros(nx,1);
    
    flag = true;
    for kx = 1:nx
        avm = round(kx*100/nx);
        if mod(avm,5) == 0
            if flag
                disp([num2str(avm),' %'])
                flag = false;
            end
        else
            flag = true;
        end
        
        b = zeros(nt,1);
        b(:) = data2D(:,kx);
        b(:) = b(:) - F(:);
        b(b < 0 ) = 0;
        % suppression des regions vides
        xx = mean(b);
        if xx > 2
            mat = zeros(nt-m_debut+1,2);
            % Création de la courbe de Scatchard
            mat(:,1) = b(m_debut:end);
            mat(:,2) = b(m_debut:end) ./ F(m_debut:end);
            
            %% ------------ MINIMISATION ------------------------- %%
            % minimisation de (y - ax -b)^2 + (x -y/a +b/a)^2
%             f = @(par)funps(par,mat);
%             [sol,RESNORM,RESIDUAL,EXITFLAG,OUTPUT,LAMBDA,JACOBIAN] = lsqnonlin(f,xo,[],[],optimset('MaxIter',500,'MaxFunEvals',5000,'Algorithm','levenberg-marquardt','Display','off','TolX',1e-2));
%             bmax(kx,1) = -sol(2)/sol(1);
%             kd (kx,1) = -1/sol(1);

            f = @(par)funps_residual(par,mat);
            [sol,RESNORM,RESIDUAL,EXITFLAG,OUTPUT,LAMBDA,JACOBIAN] = lsqnonlin(f,xo,[],[],optimset('MaxIter',500,'MaxFunEvals',5000,'Display','off','TolX',1e-2));
            bmax(kx,1) = -sol(2)/sol(1);
            kd (kx,1) = -1/sol(1);
            sigmaq = RESNORM/(nt-m_debut+1-2);

            try
                cov = full(sigmaq*inv(JACOBIAN'*JACOBIAN));
                dbmax1 = sol(2)/(sol(1)^2);
                dbmax2 = -1/sol(1);
                dkd1   = 1/(sol(1)^2);
                sdbmax = sqrt(dbmax1^2*cov(1,1) + dbmax2^2*cov(2,2) + 2*dbmax1*dbmax2*cov(1,2));
                sdkd   = sqrt(dkd1^2*cov(1,1));
                CVbmax(kx,1) = sdbmax*100/abs(bmax(kx,1));
                CVkd(kx,1) = sdkd*100/abs(kd(kx,1));
            catch
                CVbmax(kx,1) = 0;
                CVkd(kx,1)   = 0;
            end
            
        else
            bmax(kx,1)   = 0;
            kd(kx,1)     = 0;
            CVbmax(kx,1) = 0;
            CVkd(kx,1)   = 0;
        end

    end

    bmax(bmax < 0) = 0;
    kd(kd < 0) = 0;

    fbmax = zeros(size(brainmask));
    fbmax(brainmask) = bmax;
    fkd = zeros(size(brainmask));
    fkd(brainmask) = kd;
    fCVbmax = zeros(size(brainmask));
    fCVbmax(brainmask) = CVbmax;
    fCVkd = zeros(size(brainmask));
    fCVkd(brainmask) = CVkd;
    
    %% Output volumes
    pethd_fin = pethd(1);
    
    writeNIFTI(bmaxf,fbmax,pethd_fin);
    writeNIFTI(kdf,fkd,pethd_fin);
    writeNIFTI(CVbmaxf,fCVbmax,pethd_fin);
    writeNIFTI(CVkdf,fCVkd,pethd_fin);

    tSec = toc;
    tMin = tSec/60;
    disp('');
    disp(' -------------------- ');
    fprintf('Quantification done for %s in %u min \n',sujid,round(tMin));
    disp(' -------------------- ');
    disp('');
end

