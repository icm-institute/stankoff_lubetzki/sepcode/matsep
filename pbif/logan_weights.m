function [Vt, SD_Vt,CV_Vt]=logan_weights(tac_time,tac,tac_nsd,aiftime,aif,n_points)

% Pre-processing
if time(1)~=0; 
    tt_time=[0;tac_time];
    tt_Cref=[0;tac];
    modified=true;
else
    tt_time=tac_time;
    tt_Cref=tac;
    modified=false;
end

% integral of time
integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end
