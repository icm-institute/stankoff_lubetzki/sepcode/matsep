function [newnorm, newnlm]=regionNormalization(reg,ima,regvalue,regint)
%% Slice normalization based on a region
%% regvalue= intensity of the region to use to normalize
%% regint  = output intensity to set the reg region

if(nargin < 5) || isempty(regin)
    regint=100;
end
if(nargin < 4) || isempty(regvalue)
    regvalue=3;
end

% create output images
sizes=size(ima);
newnorm =zeros(sizes); % output image

% for each slice
for s=1:sizes(3)

    % mean(median) of region==3 (control)
    fprintf(' -- Doing slice %d',s);
    slice   =ima(:,:,s);
    sreg =reg(:,:,s);

    if sum(sreg==regvalue) < 10
        fprintf(' -> Skipping');
        % Remove points with not enough normalization points
        slope=0;
    else
        fprintf(' -> Processing');
        % Compute value in the region of reference 
        mea =mean( slice(sreg==regvalue) )

        % Compute slope
        slope= regint/mea;
    end

    % We assume the intensection point is the 0,0
    newnorm(:,:,s)=slope.*(slice(:,:));

end % for each slice
