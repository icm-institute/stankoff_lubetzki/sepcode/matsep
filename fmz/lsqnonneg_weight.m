function x = lsqnonneg_weight(A,y,w,robust)
% Solve linear least squares y = A*x, with x>=0, possibly with weights w,
% where w = nsd (noise standard deviation) and with the option of robust
% fit (iterative reweighting as implemented in matlab for standard least
% square: Bisquare weights)

% to do: double lines prevent detecting convergence 

n = size(A,2);
if ~isempty(w)
    sqrtw = w(:);
    J = repmat(sqrtw,1,n).*A;
    Dy = sqrtw.*y;
else
    J = A;
    Dy = y;
end

x = lsqnonneg(J,Dy);

if  robust
    
    [Q,~]     = qr(J,0);
    leverage  = sum(Q.*Q,2);
    h         = min(.9999, leverage);
    adjfactor = 1 ./ sqrt(1-h);
    
    % If we get a perfect or near perfect fit, the whole idea of finding
    % outliers by comparing them to the residual standard deviation becomes
    % difficult.  We'll deal with that by never allowing our estimate of the
    % standard deviation of the error term to get below a value that is a small
    % fraction of the standard deviation of the raw response values.
    tiny_s = 1e-6 * std(y);
    if tiny_s==0
        tiny_s = 1;
    end
    
    res = Dy - J*x;
    
    % Perform iteratively re-weighted least squares to get coefficient estimates
    D = 1e-6;
    iter = 0;
    iterlim = 50;
    
    % While not converged, estimate weights from residuals and fit again.
    while true % DO-WHILE loop. Exit condition is at the end.
        
        x0 = x;
        
        iter = iter+1;
        
%         subplot(211)
%         plot(y,'o')
%         hold on
%         plot(A*x,'-r')
%         title(num2str(iter))
%         hold off
%         subplot(212)
%         stem(x)
%         pause
        
        % Adjust residuals from previous fit, then compute scale estimate
        radj  = res .* adjfactor;
        P     = length(find(x));
        rs    = sort(abs(radj));
        sigma = median(rs(P+1:end)) / 0.6745;

        % Compute new weights from these residuals, then re-fit
        tune = 4.685;
        r    = radj/(max(tiny_s,sigma)*tune);
        bw   = (abs(r)<1) .* (1 - r.^2).^2;
        if all(bw(:)==0)
            bw(:) = 1;
        end
        
        J  = repmat(bw,1,n).*J;
        Dy = bw.*Dy;
        
        x = lsqnonneg(J,Dy);
               
        res = Dy - J*x;
        
        % DO-WHILE loop -- Exit conditions
        %
        % Check for convergence
        % (when there are double lines, the estimate seems not to converge, 
        % but in practice it is. Need to think how to deal with it ...  
        if all( abs( x-x0 ) <= D*max( abs( x ), abs( x0 ) ) ) 
            exitflag = 1;
            break
        end
        % Check if the iteration limit has been exceeded
        if (iter > iterlim)
            exitflag = 0;
            break
        end

    end % DO-WHILE loop
    
    
end
