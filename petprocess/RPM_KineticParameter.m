function [param,SD_param,CV_param] = RPM_KineticParameter(ALFA,SD_ALFA,CV_ALFA,THETA3)
% Initialization
param = zeros(size(ALFA));
SD_param = zeros(size(ALFA));
CV_param = zeros(size(ALFA));

% Ri
param(1) = ALFA(1);
SD_param(1) = SD_ALFA(1);
CV_param(1) = CV_ALFA(1);

%k2
param(2) = ALFA(2)+ALFA(1)*THETA3;
SD_param(2) = sqrt(SD_ALFA(1).^2+SD_ALFA(2)^2);
CV_param(2) = SD_param(2)./param(2)*100;

%DVR
param(3)=param(2)/THETA3;
SD_param(3)=SD_param(2);
CV_param(3)=CV_param(2);



