
function ret=doprinlm(infile,outfile,level,blocksize,beta,verbose,incmd)

try
    if ~exist('blocksize','var') || blocksize < 1
        blocksize=5;
    end
    
    if ~exist('beta','var')
        beta=1;
    end

    doverbose=1;
    if ~exist('verbose','var') || verbose==0
        doverbose=0;
    end
    
    doexit=0;
    if ~exist('verbose','var') || incmd==1
        doexit=1;
    end


    if doverbose
        fprintf(1,' -- Reading image %s\n',infile);
    end
    
    [ima,V1]=readNIFTI(infile); % using our function to deal with compression
    %V1=spm_vol(infile);
    %ima=spm_read_vols(V1);


    if nargin<3 || level <=0
        if doverbose
            fprintf(1,' -- Computing Rician STD\n');
        end
        level=RicianSTD(ima);
        if doverbose
            fprintf(1,' --  Rician STD is %1.2f \n',level);
        end
        level=beta*level;
    end
    
    
    if doverbose
        fprintf(1,' -- Using noise level: %f\n',level);
        fprintf(1,' -- Using blocksize: %d\n',blocksize);
    end


    den=prinlm(ima,level,blocksize);

    if doverbose
        fprintf(1,' -- Writing ouptut image %s\n',outfile);
    end
    
    
    %Vout=V1;
    %Vout.dt=[16 0];
    %Vout.fname=outfile;
    %spm_write_vol(Vout,den);
    
    writeNIFTI(outfile,den,V1,1);
    
catch exception
    disp(' ***Error in doprinlm')
    disp(exception.identifier)
    if doexit
        exit;
    else
        ret=-1;
    end
end
ret =0;
