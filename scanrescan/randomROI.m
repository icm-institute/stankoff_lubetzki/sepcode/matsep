

function idxs=randomROI(idx0,mask,s)
%% Creates a random ROI from a binary mask
%
% idx0: is the initial index of the ROI
% mask: binary mask where all points are chosen
% s : size of the region
%
%
% Output: list of idxs of the roi
%
% If impossible to create the ROI, mask too small
% Function returns an empty matrix


dims=size(mask);
idxs=[idx0];

% get posible neighbors
border=getNeighborIdxs(idx0,mask);
toosmall=0;
for i=1:(s-1)
    if numel(border)==0
        toosmall=1;
        break
    end 
    % Choose from the border a new voxel
    r=random('unid',size(border,1));

    newidx=border(r);
    idxs=[idxs;newidx];
    
    % remove the random index from the border
    border(r)=[];
    
    % Get the borders of this new voxel
    border=[border;  getNeighborIdxs(newidx,mask)];
    
    % option: Remove repeated indexes? 
    %(depends on the stats we want to do...
    border=unique(border);

    
    % remove points already in idxs
    for kk=idxs'

        rind=find(border==kk);
        border(rind)=[];
    end
end
if toosmall==1
    idxs=[];
end

