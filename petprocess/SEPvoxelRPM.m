function exception=SEPvoxelRPM(petname,listname,projectdir,n_points)
exception='';

if nargin<2 || isempty(listname)
    listname = spm_select(1,'any','Select subjects list');
    subjects=readList(listname);
elseif ~exist(listname,'file')
    fprintf(' -- Considering input as ID');
    names=regexp(listname,'/','split')
    subjects{1}=names;
else
    subjects=readList(listname);
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

if nargin<4
    n_points=10;
end

%% Read the list



%% DOING THE PROCESSING

for n=1:size(subjects,2)
    
    inpetfolder =[petname 'preproc'];
    superfolder =[petname 'super'];
    outputfolder=[petname 'RPM'];
    
    fprintf(' Subject %s/%s\n',subjects{n}{1},subjects{n}{2});
    
    % Find data acquisition sif information
    rawpetdir=get_subdir_regex(projectdir,'raw',subjects{n}{1},subjects{n}{2},['^' petname]);
    if isempty(rawpetdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    siffile =get_subdir_regex_files(rawpetdir,'sif$');
    [time,~,~,delta]=readSIF(siffile);
    
    % Find pet image, corrected for movement
    petdir  =fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},inpetfolder,'');
    pet=get_subdir_regex_files(petdir,'movcorr.nii');
    
    if isempty(pet)
        fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    pet=char(pet);

    %% Open cluster info
    superdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},superfolder,'');
    tacfile=get_subdir_regex_files(superdir,[petname '_refregion_tac.mat']);
    if isempty(tacfile)
        fprintf('-- Subject %s/%s refregion tac %s missing\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    tacfile=char(tacfile);
    
    
    % Find brain mask to reduce processing
    roidir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},[petname 'ROI'],'');
    maskfile =get_subdir_regex_files(roidir,'brainmask.*.nii');
    if isempty(maskfile) 
        fprintf(' -- ERROR brainmask not found! %s - %s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end

    % find basal to read NSD
    basalfile =get_subdir_regex_files(roidir,'BasalG.*.nii');
    if numel(basalfile) ==0
        fprintf(' -- ERROR basal mask not found! %s\%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end
   
    % get output dir
    outdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},outputfolder,'');
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
   
    % test output exists
    vtfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_logan_vt.nii']);
    rifilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_logan_ri.nii']);
    k2filename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_logan_k2.nii']);
    sdfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_logan_sd.nii']);
    cvfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_logan_cv.nii']);
    if exist(vtfilename,'file') 
        fprintf(' -- SKIPPING: results already exists! %s-%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end

    % load reftac
    tacs=load(tacfile);
    
    % read 4D pet
    fprintf(' -- Reading dynPET\n');
    dynPET=readNIFTI(pet);
    
    % open brainmask 
    fprintf(' -- Reading brainmask\n');
    [mask,hdr]=readNIFTI(maskfile);
    % open basal
    fprintf(' -- Reading basal for NSD\n');
    basal=readNIFTI(basalfile);
    
    % extract NSD from basal
    nsd=sqrt(tacExtraction(dynPET,basal)./(1000.*delta));
    clear basal;
    
    % run logan for the image
    [Vt,Ri,k2,SD_Vt,CV_Vt]=rpm_refregion_image(time,dynPET./1000.0,tacs.tacs(1).tac./1000.0,nsd,mask);
    
    %clear tacs;
    %clear dynPET;
    
    %% write output images
    writeNIFTI(vtfilename,Vt   ,hdr,1);
    writeNIFTI(rifilename,Ri   ,hdr,1);
    writeNIFTI(k2filename,k2   ,hdr,1);
    writeNIFTI(sdfilename,SD_Vt,hdr,1,1);
    writeNIFTI(cvfilename,CV_Vt,hdr,1,1);
    
end
