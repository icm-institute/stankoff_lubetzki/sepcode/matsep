function r=tacs_srtm_regregion(tacs,reftac)

if ~exist('name','var')
    name='';
    disp=0;
end

if ~exist('disp','var')
    disp=0;
end

time=tacs(1).time;

if time(end)>300
    time=time./60; % conversion to minutes
end

if max(reftac) > 1000
    reftac=reftac./1000;
end


%% COMPUTING SOMETHING FOR THE SRTM
Cref=reftac;
time_ref=time;
            
endOfExperiment=floor(time(end));
tv=(0:0.5:endOfExperiment)';

n=length(time);
if endOfExperiment>time_ref(end)
    Cref(n+1)=Cref(n);
    time_ref(n+1)=time_ref(n);
end

if time_ref(1)>0.001;
    Cref=[0;Cref];
    time_ref=[0;time_ref];
end
Cref_virtual=interp1(time_ref,Cref,tv);

par0=[0.8; 0.05; 1.5];
for n=1:numel(tacs)
    tac=tacs(n).tac;
    
    if max(tac) > 1000
        tac=tac./1000;
    end

    weights=setweights(tacs(n).NSD(tacs(n).NSD>0),tac);

    
    [param,fit,r(n).SD_param,r(n).CV_param]=execute_SRTM(tv,Cref_virtual,time,tac,par0,weights,1,false); %PUT false to avoid graphic
    r(n).Ri=param(1);
    r(n).k2=param(2);
    r(n).DVR=param(3);
    r(n).fit=fit;
    
    r(n).name=tacs(n).name;
end
