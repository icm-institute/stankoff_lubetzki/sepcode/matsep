function [REFERENCE,GRAYRATIO,BINDINGRATIO,WHITERATIO,BLOODRATIO]=superdpa_perm(permPET,mask,time,profiletxt,thrs_ref,brainmask)
% Code for extraction of reference region
% permPET = 4D matrix for the reference region [time, x, y z]
% mas    = 3D matrix with a binary mask
% time   = vector with the time for each frame (in seconds!)
% profile= txt file path with the profile for each class
%              REF BLOOD WHITE BINDING

if nargin<5
    thrs_ref=0.9;
end
if nargin<6
    brainmask=mask;
end

DIM=size(permPET);   %PET dimension
if time(end) <150
    time=time*60; % convert to seconds
end

permPET(isnan(permPET))=0;
fprintf('\nNORMALIZATION....');
[normDYN]=permPET_normalization(permPET,brainmask);
fprintf('COMPLETED\n');

%% Phase 3 - Loading class
a	= importdata(profiletxt);
[m,n]	= size(a);
tref	= a(:,1);
class	= a(:,2:n);


% Maximum frame usable (in theory all)
for maxk=length(time):-1:1,
   if time(maxk)<=tref(m),
       break;
   end
end

classI	= zeros(maxk,n-1);
for k=1:n-1,
    classI(:,k) = interp1(tref,class(:,k),time(1:maxk));
end


%% Phase 5 - Reference Extraction
fprintf(1,' \n Classes Extraction: %d Planes\n',DIM(3));

%Class definition
GRAYRATIO       = zeros(DIM(2:4));
BLOODRATIO      = zeros(DIM(2:4));
WHITERATIO      = zeros(DIM(2:4));
BINDINGRATIO	= zeros(DIM(2:4));
%Subset of voxel for the research of Reference 
v		= zeros(maxk,1);
%Clustering
failed=0;
total =1;
idxs=find(mask);
for i=idxs'
    v(:)=normDYN(:,i);
    total=total+1;
        
    [bv,rnorm,~,eflag]  = lsqnonneg(classI,v);
    
    if (sum(bv)>0)
        GRAYRATIO(i)   = bv(1)/sum(bv);
        BLOODRATIO(i)  = bv(2)/sum(bv);
        WHITERATIO(i)  = bv(3)/sum(bv);
        BINDINGRATIO(i)= bv(4)/sum(bv);
    else
        failed=failed+1;
    end    
end

fprintf(' Total errors: %1.2f\n',100.*failed./total)
REFERENCE  = GRAYRATIO>thrs_ref;
    