 function matsep_setup()
% This function should be run before any function can be called
% It will include the necessary paths to run all the matsep code
% 
% the path towards our code
matsep='/shfj/dpahc/code/matsep/';

% The path towards spm and cenir code 
matvolpath='/shfj/dpahc/code/matvol/';
spmpath='/shfj/local/spm12-7771/';

fprintf(' -- Including matsep files in %s\n',matsep);
pp=genpath(matsep);

addpath(pp,matvolpath,spmpath);

%go;% necessary command for initializing some stuff

