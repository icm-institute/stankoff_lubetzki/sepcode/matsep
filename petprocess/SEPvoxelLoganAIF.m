function exception=SEPvoxelLoganAIF(petname,listname,projectdir,t_star)
exception='';

if nargin<2 || isempty(listname)
    listname = spm_select(1,'any','Select subjects list');
    subjects=readList(listname);
elseif exist(listname,'file')
    subjects=readList(listname);    
elseif ~exist(listname,'file')
    fprintf(' -- Considering input as ID');
    names=regexp(listname,'/','split')
    subjects{1}=names;
else
    fprintf(' -- ERROR: No subject given!!\n\n')
    return
end


if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
    fprintf('       .... using %s\n',projectdir)
end

if nargin<4
    t_star=10;
end


%% DOING THE PROCESSING

for n=1:size(subjects,2)
 
    rdir=fullfile(projectdir,'raw',subjects{n}{1},subjects{n}{2},'');
    pdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},'');
    
    inpetfolder =[petname 'preproc'];
    outputfolder=[petname 'loganAIF'];
      
    % TO IMPROVE: Get aif file
    aiffile=sprintf('%s/%s_if/%s_%s_%s_cor.aif',rdir,petname,subjects{n}{1},subjects{n}{2},petname);
    
    if ~exist(aiffile,'file')
        fprintf(' ** Input Aif function not found! %s\n',aiffile)
        continue
    end
  
    
    % Find data acquisition sif information
    rawpetdir=get_subdir_regex(rdir,['^' petname '$']);
    if isempty(rawpetdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    siffile =get_subdir_regex_files(rawpetdir,'sif$');
    if isempty(siffile)
        fprintf(' -- Missing siffile\n');
        continue
    end
    [time,~,~,~]=readSIF(siffile);
    
    % Find pet image, corrected for movement
    petdir  =fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},inpetfolder,'');
    pet=get_subdir_regex_files(petdir,'movcorr.nii');
    
    if isempty(pet)
        fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    pet=char(pet);

    % Find brain mask to reduce processing
    roidir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},[petname 'ROI'],'');
    maskfile =get_subdir_regex_files(roidir,'brainmask.*.nii');
    if isempty(maskfile) 
        fprintf(' -- ERROR brainmask not found! %s - %s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end

    
    % get output dir
    outdir=fullfile(pdir,outputfolder,'');
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
   
    % test output exists
    vtfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_loganAIF_vt.nii']);
    sdfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_loganAIF_sd.nii']);
    cvfilename=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_loganAIF_cv.nii']);
    if exist(vtfilename,'file') 
        fprintf(' -- SKIPPING: results already exists! %s-%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end

    
    % read 4D pet
    fprintf(' -- Reading dynPET\n');
    dynPET=readNIFTI(pet)./1000.0;
    
    % open brainmask 
    fprintf(' -- Reading brainmask\n');
    [mask,hdr]=readNIFTI(maskfile);

    
    %% Find AIF if available
    [time_aif,aif]        =readAIF(aiffile);
    

    tic
    % run logan for the image
    [Vt,SD_Vt,CV_Vt]=logan_aif_image(time,dynPET,mask,time_aif,aif,t_star);
    timed=toc;
    fprintf( '-- Elapsed time: %1.1f\n',timed)
    clear tacs;
    clear dynPET;

    %% write output images
    writeNIFTI(vtfilename,Vt   ,hdr,1);
    writeNIFTI(sdfilename,SD_Vt,hdr,1,1);
    writeNIFTI(cvfilename,CV_Vt,hdr,1,1);

end
