function [distance idx] = findClosest(matrix,point)

distance=0;
for d=1:3
    distance=distance+bsxfun(@minus,point(d),matrix(:,d)').^2;
end
[distance idx]=min(distance);
