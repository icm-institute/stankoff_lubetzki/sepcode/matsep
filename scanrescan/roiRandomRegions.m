function out=roiRandomRegions(ima,regions,lut,regsizes,iters)
% Compute random rois for each region of the regions label image
%
%
% IMA is the image where regions are evaluated
% ROIS is the label image, associated with the lut struct
% LUT is a struct given by importdata() with the names and numbers of
% regions
% REGSIZES is a vector with the sizes in voxels of the rois
% ITERS is the number of random rois of the analysis
%
%
% Output: 
% Struct with the average of regions



nblabels=numel(lut.textdata);
nbsizes =numel(regsizes);

% create output structure
out.regsizes=regsizes;
out.iterations=iters;
out.labels =lut.textdata;
out.roimeans=zeros(nbsizes,numel(regsizes),iters);


% compute for each label
for y=1:size(lut.data,1)
    
    
    val  =lut.data(y);
    fprintf(' -- Doing Region %s => %d\n',char(lut.textdata(y)),val);
    themask=(regions==val & ima>0);


    if sum(themask(:))==0
        fprintf(' xx Region not found with value %d\n',val);
        continue
    end
    % compute statistis for each region size
    for r=1:numel(regsizes)
        % get the random averages
        p1=getAveragePoints(ima,themask,regsizes(r),iters,themask);
        
        
        
        %% compute the final statistics
        out.roimeans(y,r,:)= p1(:);

    end
end
