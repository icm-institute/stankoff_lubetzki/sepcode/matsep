function ExtractValuesFrom2Images(image1,distance,ofile,id,suffixe)
    
    [img1,hdr1] = readNIFTI(image1,'0');
    [img2,hdr2] = readNIFTI(distance,'0');
    
    fid = fopen(char(ofile),'a');
    for i = 1:size(img1,1)
        for j = 1:size(img1,2)
            for k = 1:size(img1,3)
                if img1(i,j,k)~=0
                    v1 = img1(i,j,k);
                    v2 = img2(i,j,k);
                    
                    fprintf(fid,'%s %s %f %f\n',id,suffixe,v1,v2);
                    %dlmwrite(char(ofile),[id v1 v2],'delimiter',' ','-append');
                end
            end
        end
    end
    fclose(fid);
    
end
