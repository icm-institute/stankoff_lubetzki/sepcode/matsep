function doQuantileNorm(imafile,normfile,nlmfile)

% open image
[ima,Vima]=readNIFTI(imafile);

norm=quantileNormalization(ima);

%
writeNIFTI(normfile,norm,Vima,1)
exit