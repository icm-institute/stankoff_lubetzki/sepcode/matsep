function [suffix] = SEPdwiCreateImages(inputdir,outputdir,space,im)

images = str2num(im);

switch space
    case 'dwi'
        h = dir(fullfile(inputdir,'*_native.mat'));
    case 't1'
        h = dir(fullfile(inputdir,'*_trafo.mat'));
        
    otherwise
        disp('Error: space not recognized')
        return
end

matfile = fullfile(inputdir,h(1).name);

img{1 ,1} = {'Fractional anisotropy (''_FA.nii'')'};
img{2 ,1} = {'Mean diffusivity (''_MD.nii'')'};
img{3 ,1} = {'Largest eigenvalue (''_L1.nii'')'};
img{4 ,1} = {'Middle eigenvalue (''_L2.nii'')'};
img{5 ,1} = {'Smallest eigenvalue (''_L3.nii'')'};
img{6 ,1} = {'Radial/transverse diffusivity (''_RD.nii'')'};
img{7 ,1} = {'Relative Anisotropy (''_RA.nii'')'};
img{8 ,1} = {'Estimated B0 image (''_B0.nii'')'};
img{9 ,1} = {'Diffusion tensor (''_DT.nii'')'};
img{10,1} = {'Westin-measure CL (''_CL.nii'')'};
img{11,1} = {'Westin-measure CP (''_CP.nii'')'};
img{12,1} = {'Westin-measure CS (''_CS.nii'')'};
img{13,1} = {'First eigenvector (''_FE.nii'')'};
img{14,1} = {'Second eigenvector (''_SE.nii'')'};
img{15,1} = {'Third eigenvector (''_TE.nii'')'};
img{16,1} = {'|first eigenvector| (''_abs_FE.nii'')'};
img{17,1} = {'|second eigenvector| (''_abs_SE.nii'')'};
img{18,1} = {'|third eigenvector| (''_abs_TE.nii'')'};
img{19,1} = {'FA x |first eigenvector| (''_FA_abs_FE.nii'')'};
img{20,1} = {'CL x |first eigenvector| (''_CL_abs_FE.nii'')'};
img{21,1} = {'CP x |third eigenvector| (''_CP_abs_TE.nii'')'};
img{22,1} = {'Mean of DWI residuals from DT (''_mean_res_DWI_DT.nii'')'};
img{23,1} = {'Max of DWI residuals from DT (''_max_res_DWI_DT.nii'')'};
img{24,1} = {'Mean of B0 residuals from DT (''_mean_res_B0_DT.nii'')'};
img{25,1} = {'Max of B0 residuals from DT (''_max_res_B0_DT.nii'')'};
img{26,1} = {'Standard deviation DWIs (''_std_DWI.nii'')'};
img{27,1} = {'Standard deviation B0s (''_std_B0.nii'')'};
img{28,1} = {'Skew eigenvalues (''_skew_L.nii'')'};
img{29,1} = {'DWI>B0 values on FA (''_PNPIV.nii'')'};
img{30,1} = {'DWIs with B0(s) (''_DWIs.nii'')'};
img{31,1} = {'Local dyadic coherence (''_Kappa.nii'')'};
img{32,1} = {'Inter-voxel diffusion coherence (''_IVDC.nii'')'};
img{33,1} = {'B-matrix (''_B_matrix.txt'')'};
img{34,1} = {'Gradient directions (''_grads.txt'')'};
img{35,1} = {'Apparent diffusion coefficients (''_ADCs.nii'')'};
img{36,1} = {'ADCs - reconstructed with DT model (''_ADCs_DT.nii'')'};
img{37,1} = {'DWIs - reconstructed with DT model (''_DWIs_DT.nii'')'};
img{38,1} = {'DWI residuals from DT (''_res_DWI_DT.nii'')'};
img{39,1} = {'ADC residuals from DT (''_res_ADC_DT.nii'')'};
img{40,1} = {'Mask (''_mask.nii'')'};
img{41,1} = {'Lattice Index (''_LI.nii'')'};
img{42,1} = {'Mean of DWIs (''_mean_DWIs.nii'')'};
img{43,1} = {'Mean of B0s (''_mean_B0s.nii'')'};
img{44,1} = {'HARDI (see settings) (''_HARDI_Lmax_8_CSD_RF_NSim_0.8_8.nii'')'};
img{45,1} = {'Diffusion kurtosis tensor (''_KT.nii'')'};
img{46,1} = {'Mean kurtosis (''_MK.nii'')'};
img{47,1} = {'Axial kurtosis (''_AK.nii'')'};
img{48,1} = {'Radial kurtosis (''_RK.nii'')'};
img{49,1} = {'Kurtosis anisotropy (''_KA.nii'')'};
img{50,1} = {'Mode of anisotropy (''_AM.nii'')'};
img{51,1} = {'Diffusion tensor norm (''_DTN.nii'')'};
img{52,1} = {'Skeletonized FA (''_FA_Skeleton.nii'')'};
img{53,1} = {'Axonal Water Fraction (from DKI) (''_AWF.nii'')'};
img{54,1} = {'Tortuosity (from DKI) (''_TORT.nii'')'};
img{55,1} = {'Axial extra-axonal diffusivity (from DKI) (''_AxEAD.nii'')'};
img{56,1} = {'Radial extra-axonal diffusivity (from DKI) (''_RadEAD.nii'')'};


disp('ExploreDTI needs to be running to perform the image extraction')
MainExploreDTI

for i = 1 : length(images)
    image = img{images(i)};
    
    % suffix
    C = strsplit(char(image));
    S = strsplit(char(C(end)),char(39));    
    suffix{i} = char(S(2));
    
    % conversion
    E_DTI_Convert_mat_2_nii(matfile,outputdir,image);
    
    % get output
    h = dir(fullfile(outputdir,strcat('*',suffix{i})));
    outfile = fullfile(outputdir,h(1).name);
    
    % flip
    disp(['Flipping and compressing: ',outfile])
    flip_lr(outfile,outfile)
    
    % compress
    gzip(outfile)
    
    if exist(outfile)
        delete(outfile)
    end
    
end

close all force
