function plotCluster(cluster)
% Plots the tacs of a cluster structure
%
% figure should be created before
%
ROI=[cluster.tac];
names=cell([1 length(cluster)]);

for x=1:length(cluster)
    names{x}=cluster(x).name;
end


plot(cluster(1).time,ROI)
legend(names)


