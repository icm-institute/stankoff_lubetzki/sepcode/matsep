function [tracks_interp,trk_mean_length] = trk_geointerp(tracks,nPoints_new)
%TRK_INTERP - Interpolate tracks with cubic B-splines
%Streamlines will be resampled to have a new number of vertices. May be useful
%to groom your tracks first. Can be multithreaded if Parallel Computing Toolbox
%is installed.
%
% Syntax: [tracks_interp,trk_mean_length] = trk_interp(tracks,nPoints_new,spacing,tie_at_center)
%
% Inputs:
%    tracks      - Struc array output of TRK_READ [1 x nTracks]
%    nPoints_new - Constant # mode: Number of vertices for each streamline
%                  (spacing between vertices will vary between streamlines)
%                  (Default: 100)
%    spacing     - Constant spacing mode: Spacing between each vertex (# of
%                  vertices will vary between streamlines). Note: Only supply
%                  nPoints_new *OR* spacing!
%    tie_at_center - (Optional) Use with nPoints_new to add an additional
%                  "tie-down" point at the midpoint of the tract. Recommended.
%                  http://github.com/johncolby/along-tract-stats/wiki/tie-at-center
%
% Outputs:
%    tracks_interp   - Interpolated tracks in matrix format.
%                      [nPoints_new x 3 x nTracks]
%    trk_mean_length - The length of the mean tract geometry if using the
%                      spacing parameter, above. Useful to then normalize track
%                      lengths between subjects.
%
% Example:
%    exDir                   = '/path/to/along-tract-stats/example';
%    subDir                  = fullfile(exDir, 'subject1');
%    trkPath                 = fullfile(subDir, 'CST_L.trk');
%    volPath                 = fullfile(subDir, 'dti_fa.nii.gz');
%    volume                  = read_avw(volPath);
%    [header tracks]         = trk_read(trkPath);
%    tracks_interp           = trk_interp(tracks, 100);
%    tracks_interp           = trk_flip(header, tracks_interp, [97 110 4]);
%    tracks_interp_str       = trk_restruc(tracks_interp);
%    [header_sc tracks_sc]   = trk_add_sc(header, tracks_interp_str, volume, 'FA');
%    [scalar_mean scalar_sd] = trk_mean_sc(header_sc, tracks_sc);
%
% Other m-files required: Curve Fitting Toolbox (aka Spline Toolbox)
% Subfunctions: none
% MAT-files required: none
%
% See also: TRK_READ, SPLINE, PARFOR

% Author: John Colby (johncolby@ucla.edu)
% UCLA Developmental Cognitive Neuroimaging Group (Sowell Lab)
% Apr 2010


tracks_interp   = zeros(nPoints_new, 3, length(tracks));
trk_mean_length = [];
pp = repmat({[]},length(tracks));


%% 1) Find the shortest track
iShort     =1;
lengthShort=10000;
for iTrk=1:length(tracks)
    tracks_tmp = tracks(iTrk);
    
    % Determine streamline segment lengths
    segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
    len  = sum(segs(:));
    
    if len < lengthShort
        lengthShort=len;
        iShort=iTrk;
    end
end

%% 2) Sample shortest path / Choose the nPoints
% This track is resample as before
tracks_tmp = tracks(iShort);

% Determine streamline segment lengths
segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
dist = [0; cumsum(segs)];

% Fit spline
pp{iShort} = spline(dist, tracks_tmp.matrix');
    
% Resample streamline along the spline
tracks_short= ppval(pp{iShort}, linspace(0, max(dist), nPoints_new))';



%% 3) High sample & choose closest points
% First interpolate with many points
% for each point in the shortest path, find the closest in the track and
% keep it as the reduced version of the sample.

parfor iTrk=1:length(tracks)
    tracks_tmp = tracks(iTrk);
    
    % Determine streamline segment lengths
    segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
    dist = [0; cumsum(segs)];
    
    % Fit spline
    pp{iTrk} = spline(dist, tracks_tmp.matrix');
    
    % Resample streamline along the spline
    tracks_interp_tmp = ppval(pp{iTrk}, linspace(0, max(dist), nPoints_new*20))';
    
    new_track=zeros(nPoints_new,3);
    for x=1:nPoints_new
        [mdist mitem]=min(sum((tracks_interp_tmp-squeeze(repmat(tracks_short(x,:),[size(tracks_interp_tmp,1) 1]))).^2,2));
        new_track(x,:)=tracks_interp_tmp(mitem,:);
    end
    tracks_interp(:,:,iTrk)=new_track; 
    
end
tracks_interp(:,:,iShort) =tracks_short;
