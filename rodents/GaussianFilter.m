function new_img = GaussianFilter(image,variance)
% fonction permettant d'appliquer un filtre gaussien � l'image d'entree
% image: image d'entree
% variance: variance de la gaussienne appliquee avec le filtre
% new_img: image filtree
    
    masque = (-3*variance):(3*variance);
    gaussien = zeros(length(masque),length(masque));
    for i = 1:length(masque)
        for j = 1:length(masque)
            gaussien(i,j) = exp(-((masque(i)^2+masque(j)^2)/(2*variance^2)))/(2*pi*variance^2);
        end
    end
    %figure();
    %surf(gaussien);
    new_img = conv2(image,gaussien,'same');

end
