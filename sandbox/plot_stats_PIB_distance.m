

clear all;

indir = '/servernas/home/poirion/data_cluster/SHADOWTEP/data/process/';
sujdir = get_subdir_regex(indir,'P.*','V01');

distdir = get_subdir_regex(sujdir,'pibDistance');
distance = get_subdir_regex_files(distdir,'.*les_distancemap_2pib.nii.gz');

pibdir = get_subdir_regex(sujdir,'piblogan');
pib = get_subdir_regex_files(pibdir,'.*vt.nii');
pib(5) = [];

id = {'P_ABIAB_V01','P_BARLA_V01','P_BELNO_V01','P_BENMO_V01','P_BOUJE_V01','P_BOUNA_V01','P_DILAN_V01','P_ELONA_V01','P_ISSLO_V01', 'P_LAANA_V01', 'P_LABDU_V01', 'P_LIOKE_V01','P_MARAU_V01','P_NICVA_V01','P_SIFIR_V01','P_TAZKA_V01','P_TROAR_V01','P_VIDVI_V01','P_ZEMSA_V01'};

ofile = '/servernas/home/poirion/data_cluster/SHADOWTEP/stats/Benedetta/20140904_PIB_Distance_Lesion_data_clear.txt';
result_tot = [];

for i = 1:19
   [Rsquared,resultat_mean_clean] = ComputeStat(distance(i),pib(i),ofile,id(i));
   result_tot = [result_tot;resultat_mean_clean];   
end

% result_tot(:,1) = result_tot(:,1)-1;
% result_end(:,:) = result_tot(result_tot(:,1)>0,:);
% 
% test_inf = result_end(:,1)>0 & result_end(:,1)=3;
% inf = result_end(test_inf,:);
% res_inf = polyfit(inf(:,1),inf(:,2),1);
% a_inf = res_inf(1);
% b_inf = res_inf(2);
% x_inf = 0:0.01:2;
% y_inf = a_inf*x_inf'+b_inf;
% 
% test_sup = result_end(:,1)>3;
% sup = result_end(test_sup,:);
res = polyfit(result_tot(:,1),result_tot(:,2),1);
a =res(1);
b =res(2);
x = 0:0.01:max(result_tot(:,1));
y = a*x+b;

figure();
hold on;
plot(-result_end(:,1)+max(result_end(:,1)),result_end(:,2),'+k');
% plot(max(result_end(:,1))-3,0:0.005:max(result_tot(:,2)),'-k');
% line(-x_inf'+max(x_inf),y_inf');
% line(-x'+max(x),y');
hold off;