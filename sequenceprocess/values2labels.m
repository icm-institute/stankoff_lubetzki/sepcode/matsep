function exception=values2labels(image,labels,output)
% values2labels(image,labels,output)
%
% Create an image with the mean of the image values for each labels
%

exception='';

[ima,hdr]=readNIFTI(image);
lab=readNIFTI(labels);

outima=zeros(size(ima));

nb=max(lab(:))

for n=1:nb
    
    lmean=mean(ima(lab==n));
    tmp=lab==n;
    lsum =sum(tmp(:));
    fprintf(' -- lesion %d: size %d mean %1.2f\n',n,lsum,lmean);
    
    
    outima(lab==n)=lmean;
end

writeNIFTI(output,outima,hdr);
