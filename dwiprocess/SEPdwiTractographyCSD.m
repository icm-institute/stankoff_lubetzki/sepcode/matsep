function SEPdwiTractographyCSD(inputdir,outputdir,space)

switch space
    case 'dwi'
        h = dir(fullfile(inputdir,'*_native.mat'));
    case 't1'
        h = dir(fullfile(inputdir,'*_trafo.mat'));
        
    otherwise
        disp('Error: space not recognized')
        return
end

matfile   = fullfile(inputdir,h(1).name);
%% Add sufix to filename
splitting = split(h(1).name,'.'); % {1} name {2} ext
newBaseName = strcat(splitting{1},'_Tracts_CSD','.',splitting{2});
matoutput = fullfile(outputdir,newBaseName);

%%
p.f_in  = matfile;
p.f_out = matoutput;

p.SeedPointRes     = [2 2 2];   % Seed point resolution in mm.
p.StepSize         = 1;
p.AngleThresh      = 30;
p.lmax             = 8;         % for number of directions below 45 (and above 28), set to "6".
p.FiberLengthRange = [50 500];
p.blob_T           = 0.1;       % this is the FOD threshold (is somewhat equivalent to the FA threshold with DTI)
p.randp            = 1;
p.t                = 0.01;      % Equal to "PR" variable in http://www.ncbi.nlm.nih.gov/pubmed/23927905.
p.it               = 10;        % Number of iterations.
p.suf              = 3;         % Speed up factor (integer value > 0): 1 = slowest, 2 = faster, etc.(larger than 5, reliability may go down).
p.FA_t             = 0.01;      % The initial "fat" response function needs a slightly anisotropic shape (realistic range: [0.01 0.1])

WholeBrainTrackingCSD_fast_exe_CL(p);

E_DTI_convert_tracts_mat_2_trk(matoutput)

