function wmmask=wmNeighborhood(mask,classif,percent)

if ~exist('percent','var')
    percent=0.05;
end

%% Take wm
wm = (classif == 3);


% Label lesions
CC   = bwconncomp(mask,6);
label= labelmatrix(CC);

% Dilate lesions to get neighbouring wm
se=zeros(3,3,3);
se([5 11 13 14 15 17 23])=1;
big= imdilate(label,se);

% Getting wm only
wmtouch=zeros(size(big));
wmtouch(wm)=big(wm)-label(wm);

% Find lesions with and without white matter
bins=0.5+ (0:1:CC.NumObjects);
hlab=histc(big(:)-label(:),bins);
hwm =histc(wmtouch(:)     ,bins);

wmpercent=hwm./hlab;
%figure,bar([hlab hwm])

wmmask=mask;
for n=1:CC.NumObjects
    if  wmpercent(n) > percent
        continue
    end
    wmmask(CC.PixelIdxList{n})=0;
end

end