function [Vt,SD_Vt,CV_Vt]=spatialboostrap_logan_refregion_image( time, dynPET, mask, reftac, nsd,n_points, proba_real, iters, block,visualize)
% Experimental function to reduce noise in logan images
%
%
% [Vt,SD_Vt,CV_Vt]=spatialboostrap_logan_refregion_image( time, dynPET, mask, reftac, nsd,n_points, proba_real, iters, block,visualize)
%


if ~exist('visualize','var')  
  visualize=0;
end
if ~exist('iters','var')  
  iters=200;
end
if ~exist('block','var')
  block=2;
end




% Pre-processing: create integral of reference image
if time(1)~=0;
    tt_time=[0;time];
    tt_Cref=[0;reftac];
    modified=true;
else
    tt_time=time;
    tt_Cref=reftac;
    modified=false;
end

integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end

%DIM=size(dynPET);

Vt   =zeros(size(mask));
SD_Vt=Vt;
CV_Vt=Vt;

items=size(dynPET,4);

for z=1:size(mask,3)
    if visualize
        fprintf(1,'\n       ___Plane no. %d',z);
    end
    for y=1:size(mask,2)
        for x=1:size(mask,1)
            if mask(x,y,z) <= 0
                continue
            end

            
            %% take original tac
            tac=squeeze(dynPET(x,y,z,:));
            
            %% prepare random samples
            % prepare neighborhood
            zvec=z+(-block:block);
            zvec=zvec(zvec>0 & zvec <= size(dynPET,3));
                        
            yvec=y+(-block:block);
            yvec=yvec(yvec>0 & yvec <= size(dynPET,2));
                        
            xvec=x+(-block:block);
            xvec=xvec(xvec>0 & xvec <= size(dynPET,1));
            
            % store samples in a vector without the center
            samples=zeros(numel(zvec)*numel(yvec)*numel(xvec)-1,items);
            
            % remove the tac point and store in vector
            for j=1:items
                theblock=dynPET(xvec,yvec,zvec,j);
                theblock=theblock(:);
                idx=find(theblock==tac(j),1);
                theblock(idx)=[];
                samples(:,j)=theblock(:);
            end
            
            %% figure, plot(time,samples)
            
            rnd=rand([iters items]); % choosing point
            vt_iters=zeros(iters,1);
            sd_iters=zeros(iters,1);
            
            %% DOING BOOTSTRAP
            btacs={};
            parfor i=1:iters
                prnd=rnd(i,:);
                % choos point for each item
                btac=tac;
                for j=1:items            
                    if prnd(j) > proba_real
                        %fprintf(' rand (%d,%d) = %1.2f\n',i,j,rnd(i,j))
                        btac(j)=samples(randi(size(samples,1),1),j);
                    end
                end
                btacs{i}=btac;
                [vt_iters(i),sd_iters(i)]=execute_Logan_REF_weights(time,btac,integral_Cref,n_points,nsd,'none',0);
            end
            
            %% weighted result
            boottacs=[btacs{:}];
            cv_iters=100.*sd_iters./vt_iters;
            sigma=quantile(cv_iters,0.05);
            wtac=wmean(sd_iters,boottacs,sigma);
            [Vt(x,y,z),SD_Vt(x,y,z),CV_Vt(x,y,z)]=execute_Logan_REF_weights(time,wtac,integral_Cref,n_points,nsd,'none',0);
            
            if visualize
                
                
                [vt_tac,sd_tac]=execute_Logan_REF_weights(time,tac,integral_Cref,n_points,nsd,'none',0);
                
                %tacsss=[btacs{:}];
                
               
                
               
                

                fprintf('True vt %1.2f   - Wvt  %1.2f\n',vt_tac,vt_wtac); 
                figure,
                subplot(1,2,1),scatter(vt_iters,100.*sd_iters./vt_iters)
                hold on
                scatter(vt_tac,100.*sd_tac./vt_tac,'r')
                scatter(vt_wtac,100.*sd_wtac./vt_wtac,'g')
                hold off
                
                [~,minidx]=min(cv_iters);
                
                
                
                subplot(1,2,2),plot(time,tac,time,btacs{minidx},time,mean(boottacs,2),time,wtac)
                legend('Voxel','MinCV','Mean','Wmean')

                pause
                close all
            end
        end
    end
end

function mval=wmean(weights,values,sigma)

    weights=exp(-weights./sigma);
    mval=(values*weights)./sum(weights(:));
    
