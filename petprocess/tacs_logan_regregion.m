function r=tacs_logan_regregion(tacs,reftac,n_points)

if ~exist('name','var')
    name='';
    disp=0;
end

if ~exist('disp','var')
    disp=0;
end

time=tacs(1).time;

if time(end)>300
    time=time./60; % conversion to minutes
end

if max(reftac) > 1000
    reftac=reftac./1000;
end


% Pre-processing
if time(1)~=0;
    tt_time=[0;time(:)];
    tt_Cref=[0;reftac(:)];
    modified=true;
else
    tt_time=time(:);
    tt_Cref=reftac(:);
    modified=false;
end

integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end

for n=1:numel(tacs)
    tac=tacs(n).tac;
    if max(tac) > 1000
        tac=tac./1000;
    end
    [r(n).Vt,r(n).SD_Vt,r(n).CV_Vt]= execute_Logan_REF_weights(time,tac,integral_Cref,n_points,tacs(n).NSD,name,disp);
    r(n).name=tacs(n).name;
end
