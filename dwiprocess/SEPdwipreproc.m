function SEPdwipreproc(sid,visit,rawdir,procdir,mri,options)

if nargin < 6
    options = struct();
end

try options.SignalDriftCorr; catch, options.SignalDriftCorr = true;     end
try options.GibbsCorr;       catch, options.GibbsCorr       = true;     end
try options.KeepTmp;         catch, options.KeepTmp         = false;    end

% Number of B0, perm and flip, bval for signal drift correction
switch mri
    case 'prisma'
        NrB0 = 6;
        % Warning: there seems to be a coordinate systems mismatch!
        % Use the following conversion settings:
        % Permute gradient components: y x z
        % Flip sign of gradient components: -x y z
        perm = 2; % having values 1, 2, ... or 6
        flip = 2; % having values 1, 2, ... or 4 (order is the same as in the pop-up window when converting *.nii/txt to *.mat)
        
        bvalC = 0; % bvalue to perform the frift correction on
        
    case 'trio'
        NrB0 = 1;
        % Warning: there seems to be a coordinate systems mismatch!
        % Use the following conversion settings:
        % Permute gradient components: y x z
        % Flip sign of gradient components: x y -z
        perm = 2; % having values 1, 2, ... or 6
        flip = 4; % having values 1, 2, ... or 4 (order is the same as in the pop-up window when converting *.nii/txt to *.mat)
        
        bvalC = 1000; % bvalue to perform the frift correction on
    otherwise
        disp('Error: mri nor recognized')
        return
end

rawsubjdir  = fullfile(rawdir,sid,visit);
procsubjdir = fullfile(procdir,sid,visit);
outputdir   = fullfile(procsubjdir,'dwipreproc');

% counter of temporary files
tmpcount = 0;

disp([' +++ Processing subject ',sid,' ',visit])

% mkdir(outputdir);

%% Bval, bvec to txt
disp(' -- Bval & bvec to .txt creation')
bvalfile    = fullfile(rawsubjdir,'dwi',strcat(sid,'_',visit,'_dwi.bvals'));
bvecfile    = fullfile(rawsubjdir,'dwi',strcat(sid,'_',visit,'_dwi.bvecs'));
currenttxt  = fullfile(outputdir,strcat(sid,'_',visit,'_dwi.txt'));

bval = textread(bvalfile)';
bvec = textread(bvecfile)';

% remove extraline
bval = bval(1:end-1);
bvec = bvec(1:end-1,:);

% creation of matrix
B = bval(:,ones(1,6)).*[bvec(:,1).^2 2*bvec(:,1).*bvec(:,2) 2*bvec(:,1).*bvec(:,3) bvec(:,2).^2 2*bvec(:,2).*bvec(:,3) bvec(:,3).^2];
fid = fopen(currenttxt,'w');
for i = 1 : size(B,1)    
    fprintf(fid, '%12.8f\t%12.8f\t%12.8f\t%12.8f\t%12.8f\t%12.8f\n',B(i,:));
end
fclose(fid);
disp(' ---- Done')

%% Unpack DWI
disp(' -- Unpacking DWI')
currentdwi = fullfile(rawsubjdir,'dwi',strcat(sid,'_',visit,'_dwi.nii.gz'));
gunzip(currentdwi,outputdir)
currentdwi = fullfile(outputdir,strcat(sid,'_',visit,'_dwi.nii'));
disp(' ---- Done')

%% Drift correction
if options.SignalDriftCorr
    disp(' -- Signal drift correction')
    
    % input and output
    par.f_in_nii  = currentdwi;
    par.f_in_txt  = currenttxt;
    
    suffix = '_sdc';
    outdwi = addsufixtofilenames(currentdwi,suffix);
    outtxt = addsufixtofilenames(currenttxt,suffix);
    
    par.f_out_nii = outdwi;
    par.f_out_txt = outtxt;
    
    % algorithm parameters
    par.method         = 2;     % 1 (Linear), 2 (Quadratic), 3 (Cubic)
    par.masking.do_it  = 0;     % 0,1 (enable disable masking)
    par.masking.p1     = 5;     % erosion factor 1
    par.masking.p2     = 1;     % erosion factor 2 (puoi copiare questi valori da settings, signal drift correction)
    par.bvalC          = bvalC; % bvalue to perform the correction on
    par.bv_thresh      = 10;    % tolerance around bvalC
    par.show_summ_plot = 1;     % 0,1 (save the png of the correction)
    
    % drift correction
    E_DTI_signal_drift_correction(par)
    
    % temporary files to delete
    tmpcount = tmpcount + 1;
    tmplist{1,tmpcount} = currentdwi;
    tmpcount = tmpcount + 1;
    tmplist{1,tmpcount} = currenttxt;
    
    % updated files
    currentdwi = outdwi;
    currenttxt = outtxt;
    
    disp(' ---- Done')
end


%% B0 at the beginning
if NrB0 > 1
    disp(' -- Sort DWIs with respect to B-val')
    suffix = '_SB';
    outdwi = addsufixtofilenames(currentdwi,suffix);
    
    E_DTI_sort_DWIs_wrt_b_val_ex(currentdwi,'',outdwi)
    
    tmpcount = tmpcount + 1;
    tmplist{1,tmpcount} = currentdwi;
    tmpcount = tmpcount + 1;
    tmplist{1,tmpcount} = currenttxt;
    
    currenttxt = addsufixtofilenames(currenttxt,suffix);
    currentdwi = outdwi;
    disp(' ---- Done')
end

%% Gibbs correction
if options.GibbsCorr
    disp(' -- Gibbs ringing correction')
    suffix = '_TV';
    outdwi = addsufixtofilenames(currentdwi,suffix);
    
    p.NrB0   = NrB0;    % The GR tool corrects only for the first "p.NrB0" volumes. Typically, the b=0 s/mm^2 images are affected most (so it was initially designed to correct only the "p.NrB0" non-DWIs in the beginning of the *.nii file), but you could also apply it to all DWI volumes (set "p.NrB0" then as the total number of DWIs). For the following parameter settings, see the paper: http://www.ncbi.nlm.nih.gov/pubmed/26142273
    p.lambda = 100;     % rough range: [10 150] depending on voxel size / SNR etc.
    p.iter   = 100;     % Idem... [10 200]
    p.ss     = 0.01;    % Idem... [0.005 0.05]
    p.ip     = 3;       % Has value "1" (coronal), "2" (sagittal) or "3" (axial) for the acquisition plane.
    
    E_DTI_Gibbs_Ringing_removal_with_TV_exe(currentdwi,outdwi,p);
    
    tmpcount = tmpcount + 1;
    tmplist{1,tmpcount} = currentdwi;
    
    currentdwi = outdwi;
    disp(' ---- Done')
end


%% mat file creation
disp(' -- Quick and dirty nifti and b-matrix to .mat file')
currentmat = change_file_extension(currentdwi,'.mat');

% masking parameters... tune them at will:
Mask_par.tune_NDWI = 0.7;   % (rough range: [0.3 1.5])
Mask_par.tune_DWI = 0.7;    % (rough range: [0.3 1.5])
Mask_par.mfs = 5;           % (uneven integer)

E_DTI_quick_and_dirty_DTI_convert_from_nii_txt_to_mat(currentdwi, currenttxt, currentmat, Mask_par, NrB0, perm, flip)

% somewhere, somehow, somebody is flipping the data, so we flip it back
% E_DTI_Flip_left_right_mat(currentmat,currentmat)

tmpcount = tmpcount + 1;
tmplist{1,tmpcount} = currentdwi;
tmpcount = tmpcount + 1;
tmplist{1,tmpcount} = currenttxt;

disp(' ---- Done')

%% SM/EC/EPI correction
disp(' -- SM/EC/EPI correction')

% Unzip T1 & mask and put appropiate name
% currentt1 = fullfile(procsubjdir,'t1preproc',strcat(sid,'_',visit,'_t1_N4.nii.gz'));
% gunzip(currentt1,outputdir)
% currentmask = fullfile(procsubjdir,'t1preproc',strcat(sid,'_',visit,'_t1_mask.nii.gz'));
% gunzip(currentmask,outputdir)

%currentt1 = fullfile(outputdir,strcat(sid,'_',visit,'_t1_N4_111.nii'));
currentt1 = cell2mat(get_subdir_regex_files(outputdir,'(.*t1_pre_111.nii.*)|(.*t1_N4_111.nii.*)'));
outt1 = addsufixtofilenames(currentdwi,'_t1_111');
movefile(currentt1,outt1)
% currentt1 = outt1;

currentmask = fullfile(outputdir,strcat(sid,'_',visit,'_t1_mask_111.nii'));
outmask = addsufixtofilenames(currentdwi,'_t1_mask_111');
movefile(currentmask,outmask)
% currentmask = outmask;

% tmpcount = tmpcount + 1;
% tmplist{1,tmpcount} = currentt1;
% tmpcount = tmpcount + 1;
% tmplist{1,tmpcount} = currentmask;

% create Parameters_SM_EC_EPI.txt
createParameters_SM_EC_EPI(currentmat,'_t1_111.nii','_t1_mask_111.nii',outputdir)
parameter_filename = fullfile(outputdir,'Parameters_SM_EC_EPI.txt');


% why linux always has problems????
% if isunix
%     EDdir = fileparts(which('MainExploreDTI'));
%     setenv('LD_LIBRARY_PATH',[fullfile(EDdir,'Source','MD_cor_E','linux64'),':',getenv('LD_LIBRARY_PATH')]); 
%     addpath(fullfile(EDdir,'Source','MD_cor_E','linux64'));
% end

% actual corrections
E_DTI_SMECEPI_Main(parameter_filename);

tmpcount = tmpcount + 1;
tmplist{1,tmpcount} = currentmat;

disp(' ---- Done')

%% Clean Temporary files
if ~options.KeepTmp
    native = dir(fullfile(outputdir,'*_native.mat'));
    trafo  = dir(fullfile(outputdir,'*_trafo.mat'));
    
    if length(native)+length(trafo) < 2   
        disp(' -- Error!! Output missing')
    else
        disp(' -- Cleaning Temporary files')
        for i = 1 : tmpcount
            delete(tmplist{i})
        end
        disp(' ---- Done')
    end
end


