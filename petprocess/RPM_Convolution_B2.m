function B2 = RPM_Convolution_B2(Cref,tv,theta3)
%% NOTE that the virtual grid tv has to be uniform 
% check for tv as a column vector
[r,c]= size(tv);
if c>r
    tv=tv';
end

%Virtual convolution
dt=tv(2)-tv(1);
B2=zeros(length(tv),length(theta3));

for i=1:length(theta3);
    v=exp(-tv.*theta3(i));
    B2(:,i)=dt*filter(v,1,Cref);
end