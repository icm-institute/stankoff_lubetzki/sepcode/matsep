function [tritime,triplasma,params,resnorm]=fitTriexp(rawtime,rawplasma,time,visualize,thetitle)
%% Interpolate arterial input function time using a linear + tri-exponential function
%
% Inputs
%  - rawtime  : input time in seconds
%  - rawplasma: input plasma activity in Kbs
%  - time     : final time division in seconds
%  - visualize : binary input to plot the solution (default: 0 no display)
%  - thetitle : Title of the plot if visualize=1
%
%
% Outputs
%  - tritime: final timedivision (exactly the same as time)
%  - triplasma: activity after interpolation
%  - params: optimization parameters
%  - resnorm: residual given by the fitting
% date: 09/02/2015
% author: D.Garcia-Lorenzo

if nargin<4
    visualize=0;
end

tritime=time;
triplasma =zeros(size(tritime));

% find max
[amx,i]=max(rawplasma);
tmx=rawtime(i);

%% finding time in the new division
tmptime=find(tritime<tmx);
triind=tmptime(end);

%% 1) Interpolate Linear up to the max
time1st=tritime(1:triind);
triplasma(1:triind) = interp1(rawtime(1:i),rawplasma(1:i),time1st);
triplasma(1:triind);


%% 2) Tri exponential after the max
F=@(x,xdata)x(1).*exp(-x(2)*xdata)+x(3).*exp(-x(4)*xdata)+x(5).*exp(-x(6)*xdata);

% Initialize parameters (based on DPA data)
init=[amx/2   1.5  amx/4  0.5 amx/4    0.05 ];
lb  =[    0     1      0   0   0       0    ];
hb  =[amx.*0.9  4 amx.*0.8 1  amx.*0.8 1    ];

% Shift time origin to the time maximum for the fitting
% using minutes to have larger fitting constants
to=(rawtime(i:end)-rawtime(i))/60;
opts=optimset('Display','off');

[params,resnorm]=lsqcurvefit(F,init,to,rawplasma(i:end),lb,hb,opts);

% interpolate points
triplasma(triind:end)=F(params,(tritime(triind:end)-rawtime(i))./60);


if visualize
    figure
    subplot(1,2,1)
    plot(rawtime,rawplasma,'ro',tritime,triplasma)
    if nargin==5
        title(thetitle)
        fprintf('  -- %s params:',thetitle);
    end
    subplot(1,2,2)
    plot(rawtime(rawtime>1800),rawplasma(rawtime>1800),'ro',tritime(tritime>1800),triplasma(tritime>1800))
    
    fprintf('f(x) = %1.2f x exp(- %1.2f x t ) + %1.2f x exp(- %1.2f x t ) +  %1.2f x exp(- %1.2f x t )\n', ...
               params(1),params(2),params(3),params(4),params(5),params(6));
end

