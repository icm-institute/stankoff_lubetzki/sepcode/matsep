function dist = funps_residual(xxx,mat)
    % minimisation de (y - ax -b)^2 + (x -y/a +b/a)^2
    [n,~] = size(mat);
    
    y = mat(:,2);
    x = mat(:,1);
    a = xxx(1);
    b = xxx(2);
    
    dist1 = y-a.*x-b;
    dist2 = x-y./a+b/a;
    
    w = [ 1 2 2 2 1.5 ones(1,n-5) ]';
    dist = [dist1*5 ; dist2*sqrt(0.5)].*sqrt(repmat(w,2,1));
    
    
    
    
%     [n,~] = size(mat);
%     w = [ 1 2 2 2 1.5 ones(1,n-5) ];
%     dist = 0;
%     for i = 1:n
%         dist = dist + (((mat(i,2)-xxx(1)*mat(i,1)-xxx(2))*5)^2 + 0.5*(mat(i,1)-mat(i,2)/xxx(1)+xxx(2)/xxx(1))^2)*w(i);
%     end;