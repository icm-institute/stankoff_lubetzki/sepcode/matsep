function gmclass=kmeansTLE(data,k,h,maxiter,emiters)
%
%    gmclass=kmeansTLE(data,k,h,maxiter,emiters)
%
% TrimmedLikelihoodEstimator initialized by a kmeans
%
% see TrimmedLikelihoodEstimator for the parameters
%


if ~exist('maxiter','var')
    maxiter=200;
end
if ~exist('emiters','var')
    emiters =10;
end

%% Use k means as initialization
idx=kmeans(data,k,'replicates',20);

dim=size(data,2);
N  =size(data,1);


mu0=zeros(k,dim);
sigma0=zeros(dim,dim,k);
alpha0=ones(k,1)./k;

for x=1:k
    alpha0(x)=sum(idx==x)./N;
    mu0(x,:) =mean(data(idx==x,:));
    for d=1:dim
        sigma0(d,d,x)=var(data(idx==x,d))./k;
    end
end

% create mixture class
initia = gmdistribution(mu0,sigma0,alpha0);

% run EM
gmclass=TrimmedLikelihoodEstimator(data,k,h,initia,maxiter,emiters);


end