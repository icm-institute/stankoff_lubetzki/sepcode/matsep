function [denoised,nweights]=tnlm_subsample(permPET, mask, beta,searchsize,tzero,subsample,mpet)

if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('subsample','var')
    subsample=2;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end

searchsize=subsample*round(searchsize/subsample);

if ~exist('mpet','var')
    %% Create average image
    fsize=5;
    mpet = convn(permute(permPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
    mpet = single(permute(mpet,[4 1 2 3]));
end

%% 
dims   =size(permPET);

% output image
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 
nweights=zeros(dims(1:3));


%% Start processing voxels
for k=1:dims(4) % for each voxel
    %fprintf(' -- Slice %d\n',k)    
for j=1:dims(3)
for i=1:dims(2)
    
    if mask(i,j,k)==0
        continue
    end
    tac =permPET(:,i,j,k);
    mtac=   mpet(:,i,j,k);
    
    
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(2),i+searchsize);
    Jmax=min(dims(3),j+searchsize);
    Kmax=min(dims(4),k+searchsize);
    %% First: estimate min distance
    mindist=10^10;
    zvec=unique([k:-subsample:Kmin k:subsample:Kmax]);
    yvec=unique([j:-subsample:Jmin j:subsample:Jmax]);
    xvec=unique([i:-subsample:Imin i:subsample:Imax]);
    
    block =permPET(:,xvec,yvec,zvec);
    blockm=mpet(:,xvec,yvec,zvec);
    
    dists=zeros(numel(xvec),numel(yvec),numel(zvec));

    for z=1:numel(zvec)
    for y=1:numel(yvec)
    for x=1:numel(xvec)

        %d=sum(sum(sum(sum( sigma2(tzero:end) .* ((tac(tzero:end)-dynPET(x,y,z,tzero:end)-mtac(tzero:end)+mpet(x,y,z,tzero:end)).^2) ) ) ));
        d      = tac - block(:,x,y,z);
        d_diff = (d - mtac + blockm(:,x,y,z)).^2;
        if d(tzero)<1e-6
            continue;
        end
        dists(x,y,z) =    sum(d(tzero:end).^2);
        t=sum(d_diff(tzero:end));
        if mindist > t
            mindist=t;
        end
    end
    end
    end







    %% Apply weights
    dtac=zeros(dims(1),1);

    betamin=beta*mindist;
    weights=exp(-dists./betamin);
    wmax=max(weights(:));
    wmean=sum(weights(:));
    
    for z=1:numel(zvec)
    for y=1:numel(yvec)
    for x=1:numel(xvec)

        %% skipping center of the square
        if dists(x,y,z)==0;
            continue
        end

        dtac =dtac+ weights(x,y,z) .*  block(:,x,y,z);
    end
    end
    end
    
    nweights(i,j,k)=wmean;
    
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) ./(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end

