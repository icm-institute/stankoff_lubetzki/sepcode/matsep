function exception=SEPloganROI(petname,listname,projectdir)
exception='';

if nargin<2
    listname = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

%% Read the list
subjects=readList(listname);


for n=1:size(subjects,2)

    pdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},'');
    ndir=fullfile('../data/','process',subjects{n}{1},subjects{n}{2},'');
    %% setting output
    outpetfolder=[petname 'LoganROI'];    
    outdir=fullfile(ndir,outpetfolder,'');
    outfile=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_loganROI.csv']);
    
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    
    if exist(outfile,'file')
        fprintf(' ** Skipping: logan file exists: %s\n',outfile);
        continue
    end
    
    
    %% Finding cluster file
    clusterfile=sprintf('%s/%s/%s_%s_%s_tacs.mat', ...
            ndir,[petname 'TACs'],subjects{n}{1},subjects{n}{2},petname);
    if ~exist(clusterfile,'file')
        fprintf (' ** ERROR : clusterfile not found: %s\n\n',clusterfile);
        continue
    end
    
    %% Find reference region cluster
    
    reffile=sprintf('%s/%s/%s_%s_%s_refregion_tac.mat', ...
            pdir,[petname 'super'],subjects{n}{1},subjects{n}{2},petname);
        
     if ~exist(reffile,'file')
        fprintf (' ** ERROR : reference file not found: %s\n\n',reffile);
        continue
     end
   
    %% HELLO
    
    
    a=load(clusterfile);
    r=load(reffile);
    
    tacs=a.tacs;
    ref =r.tacs;
    
    loganvals=zeros(numel(tacs),3);
   
    p=fopen(outfile,'w');
    for m=1:numel(tacs)
        [loganvals(m,1),loganvals(m,2),loganvals(m,3)]= ...
            logan_refregion(ref(1).time ./ 60, ...
                            tacs(m).tac./1000,ref(1).tac./1000, ...
                            12,tacs(m).NSD,char(tacs(m).name),0);
                        
                        
                        
        fprintf(' -- %s :: %s  ->  %1.2f    (%1.2f)\n',subjects{n}{1},char(tacs(m).name),...
                    loganvals(m,1),loganvals(m,3));
                
        fprintf(p,'%s,%s,%s,%1.3f,%1.3f,%1.3f\n', subjects{n}{1},subjects{n}{2},char(tacs(m).name), ...
                loganvals(m,1),loganvals(m,2),loganvals(m,3));
    end
    fclose(p);
    
    
    
end