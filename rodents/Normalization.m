function outimg = Normalization(inimg)
    
%     m = max(max(max(inimg)));
%     outimg = zeros(size(inimg));
%     for i = 1:size(inimg,3)
%         outimg(:,:,i) = inimg(:,:,i)*200/m;
%     end
    m = max(max(max(inimg)));
    outimg = zeros(size(inimg));
    for i = 1:size(inimg,3)
        outimg(:,:,i) = inimg(:,:,i)*200/m;
    end
end