
%% INITIALIZATION
clear all;

% Start directory
pwd='/servernas/home/poirion/data/TEST/FLUMATEP2/data/tracula/';

% Subjects directories
suj = get_subdir_regex(pwd,'^[A-Z].*');
%suj = get_subdir_regex(pwd,'P0101GF');

% Path directories
dpath = get_subdir_regex(suj,'dpath');
%track = get_subdir_regex(dpath,'.*bbr');
%track = get_subdir_regex(dpath,'^lh.*bbr');
%track = get_subdir_regex(dpath,'rh.*bbr');
track = get_subdir_regex(dpath,'lh.atr.*bbr');

% Stat directories
filename = get_subdir_regex_files(track,'.*byvoxel');
stat = char(filename);

figure();
for i=1:length(stat(:,1))
    tab_stat = importdata(stat(i,:));
    stat_tab=zeros(size(tab_stat.data));
    stat_tab(1:length(tab_stat.data),1:length(tab_stat.data(1,:)))=tab_stat.data;

    k=[10,9,8,6,5,4];
    for j=k
        stat_tab(:,j)=[];
    end
    subplot(ceil(length(stat(:,1))/2),2,i)
    hold on;
    plot(1:length(stat_tab(:,4)),stat_tab(:,4));
    plot(1:length(stat_tab(:,4)),stat_tab(:,5),'r');
    legend('FA','FA__Avg');
    title('Variation de la FA');
    hold off;

end

%% Does not work - pb with importdata
% clear all
% 
% pwd='/servernas/home/poirion/data/TEST/FLUMATEP2/data/tracula/';
% statdir = get_subdir_regex(pwd,'stats');
% filename = get_subdir_regex_files(statdir,'.*bbr.FA.txt');
% track = char(filename);
% 
% 
% figure();
% for i=1:length(track(:,1))
%     tab_stat = importdata(track(i,:));
%     stat_tab=zeros(size(tab_stat.data));
%     stat_tab(1:length(tab_stat.data),1:length(tab_stat.data(1,:)))=tab_stat.data;
% 
%     k=[10,9,8,6,5,4];
%     for j=k
%         stat_tab(:,j)=[];
%     end
%     subplot(8,2,i)
%     hold on;
%     plot(1:length(stat_tab(:,4)),stat_tab(:,4));
%     title('Variation de la FA');
%     hold off;
% 
% end





