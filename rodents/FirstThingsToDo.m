function FirstThingsToDo(img,out)
    %change_hdr_orientation(img,[0 0 0 pi/2 0 pi 1 1 1]);
    %mult_vox_size(img,17);
    Modif_Header(img)
    if exist('out','var')
        [inimg,hdr] = readNIFTI(img);
        outimg =200*inimg./quantile(inimg(:),.98);
        writeNIFTI(out,outimg,hdr);
    end
end
