%% visual
% file=dir('*.nii');
% for i=1:length(file)
%     nii=load_nii(file(i).name);
%     img=nii.img;
%     showDS(double(img(:,:,30,:)),[],[0 50],[6 4])
%     sifdata = vjc_SIFread([file(i).name(1:end-8),'.sif']);
%     pause
%     close all
% end
% return
%% realign
% uso: piw_reslice_batch(filename,referenceframe,initialframe,finalframe)
% file=dir('*.nii');
% for i=1:length(file)
%     disp(num2str(i))
%     piw_reslice_batch(file(i).name,12,6,20)
% end
% return
%% verifica
% 
% filerl=dir('*_rl.nii');
% file=dir('*_Cal.nii');
% for i=1:length(file)
%     nii=load_nii(file(i).name);
%     img=nii.img;
%     niirl=load_nii(filerl(i).name);
%     imgrl=niirl.img;
%     showDS(double(img(:,:,30,:)),[],[0 50],[6 4])
%     showDS(double(imgrl(:,:,30,:)),[],[0 50],[6 4])
%     pause
%     close all
% end
% return
% 
%% flip
% filerl=dir('*_rl.nii');
% for i=1:length(file)
%     flip_lr(file(i).name, file(i).name)
% end
% return
%% compress
file=dir('*.nii');

for i=1:length(file)
    nii=load_nii(file(i).name);
    save_nii(nii,[file(i).name,'.gz'])
end
