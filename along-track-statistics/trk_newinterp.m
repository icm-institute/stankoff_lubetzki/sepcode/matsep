
function [tracks_interp] = trk_newinterp(tracks,nPoints_new)

if nargin<2 || isempty(nPoints_new), nPoints_new = 100; end



nbTrk=length(tracks);

%% Find Center in a random fiber

cTrk=randi(nbTrk,1);
tracks_tmp=tracks(cTrk);
middle=tracks_tmp.matrix(floor(size(tracks_tmp.matrix,1)/2),:);

%% Find closest point in all fibers

mindist=zeros(1,nbTrk);
minIdx =zeros(1,nbTrk);
beforeDist=zeros(1,nbTrk);
afterDist =zeros(1,nbTrk);

for iTrk=1:length(tracks)
    % take tracks
    tracks_tmp = tracks(iTrk);
    % find closes point
    [mindist(iTrk) minIdx(iTrk)]=findClosest(tracks_tmp.matrix,middle);
    
    % Determine streamline segment lengths
    segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
    segments = [0; cumsum(segs)];
    
    beforeDist(iTrk)=segments(minIdx(iTrk));
    afterDist (iTrk)=segments(end)-segments(minIdx(iTrk));
end

%% The shortest path before and after
[~, minBidx]=min(beforeDist);
tracks_tmp=tracks(minBidx);
begin=tracks_tmp.matrix(1,:);

[~, minAidx]=min(afterDist);
tracks_tmp=tracks(minAidx);
final=tracks_tmp.matrix(end,:);

%% Find the begin and final point for each track
beginPoint=zeros(1,nbTrk);
finalPoint=zeros(1,nbTrk);
mindistB  =zeros(1,nbTrk);
mindistA  =zeros(1,nbTrk);
for iTrk=1:length(tracks)
    % take tracks
    tracks_tmp = tracks(iTrk);
    % find closes point
    [mindistB(iTrk) beginPoint(iTrk)]=findClosest(tracks_tmp.matrix,begin);
    [mindistA(iTrk) finalPoint(iTrk)]=findClosest(tracks_tmp.matrix,final);
    
    % Again=Determine streamline segment lengths
    segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
    segments = [0; cumsum(segs)];
    
    beforeDist(iTrk)=segments(    minIdx(iTrk))-segments(beginPoint(iTrk));
    afterDist (iTrk)=segments(finalPoint(iTrk))-segments(    minIdx(iTrk));
    
end

fprintf(' -- Estimation: before %f1.2 / after %f1.2\n',mean(beforeDist),mean(afterDist));

%% Interpolate tracks only between begin and final
% Using the former code
tracks_interp   = zeros(nPoints_new, 3, length(tracks));

% Interpolate streamlines so that each has the same number of vertices, spread
% evenly along its length (i.e. vertex spacing will vary between streamlines)
parfor iTrk=1:length(tracks)
    tracks_tmp = tracks(iTrk);
    %fprintf(' -- %d Begin/end: %d / %d\n',iTrk,beginPoint(iTrk),finalPoint(iTrk));
    reducedMatrix=tracks_tmp.matrix(beginPoint(iTrk):finalPoint(iTrk),:);
    
    
    % Determine streamline segment lengths
    segs = sqrt(sum((reducedMatrix(2:end,1:3) - reducedMatrix(1:(end-1),1:3)).^2, 2));
    dist = [0; cumsum(segs)];
    
    % Fit spline
    pp = spline(dist, reducedMatrix');
    
    % Resample streamline along the spline
    tracks_interp(:,:,iTrk) = ppval(pp, linspace(0, max(dist), nPoints_new))';
end

