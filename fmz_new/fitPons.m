function [ypons,dpons] = fitPons(time,pons)   

betamin     = 0.0001;
betamax     = 0.5;
ncomp       = 1000;
betagrid    = logspace(log10(betamin),log10(betamax),ncomp)';
betagrid(1) = 0;

% matrix construction
G = zeros(length(time),ncomp);
for j = 1 : ncomp
    G(:,j) = exp(-betagrid(j)*time);
end
% weights
w     = sqrt(abs(1./(pons.*exp(0.034*time))));
w(isinf(w)) = 0;
GW    = G .* repmat(w,1,size(G,2));
ponsW = pons.*w;

% non-negative estimation
p = lsqnonneg(GW,ponsW);

% fitted pons
ypons = G*p;

if nargout > 1
    dpons = -(repmat(betagrid',size(G,1),1).*G)*p;
end