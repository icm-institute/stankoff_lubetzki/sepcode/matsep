function gmclass=rndTLE(data,k,h,maxiter,emiters,inits)
% gmclass=rndTLE(data,k,h,maxiter,emiters,inits)
%
% Basic random initialization for TrimmedLikelihoodEstimator
%
%


if ~exist('maxiter','var')
    maxiter=200;
end
if ~exist('emiters','var')
    emiters =10;
end
if ~exist('inits','var')
    inits=100;
end

dim=size(data,2);
N  =size(data,1);
res=struct('initia',{},'solution',{},'likelihood',{});
res(inits).initia=0;
parfor n=1:inits % Random initizalizations
    %% 1) Initialize
    % random means
    mu0   =data(randsample(N,k),:);
    % equal variance
    covar=eye(dim);
    for d=1:dim
        covar(d,d)=var(data(:,d))./k;
    end  
    sigma0= repmat(covar,[1 1 k]);
    % equal mixing 
    alpha0=ones(k,1)./k;
    % create mixture class
    initia = gmdistribution(mu0,sigma0,alpha0);
    
    res(n).initia=initia;    
    res(n).solution  =TrimmedLikelihoodEstimator(data,k,h,initia,maxiter,emiters);
    res(n).likelihood=res(n).solution.NlogL;
end

%% Select bet likelihood
[~,idx]=min([res.likelihood]);
gmclass=res(idx).solution;


end