function exception=SEPimportIF(petname,files,projectdir,listname)

if nargin < 1
    fprintf(' -- ERROR petname is mandatory')
    return
end

if nargin<2 || isempty(files)
    files = spm_select([1 Inf],'any','Select all xls files to process');
end


if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

for n=1:size(files,1)
    
    doagain=1;
    while(doagain)
        fprintf('Please assign SID to this file:\n%s\n',files(n,:))
        sidstr=input(' Write SID/VISIT ','s');
        sid=regexp(sidstr,'/','split');

        if numel(sid) ~=2
            fprintf(' --Incorrect input, format is SID/VISIT');
        else 
            pdir=fullfile(projectdir,'raw',sid{1},sid{2},[]);
            exist(pdir,'dir')
            if exist(pdir,'dir')
                doagain=0;
            else
                fprintf(' -- SID does not exists,import images first\n');
            end
        end
    end
    
    % read file and create afis
    fdir=fullfile(pdir,[petname '_if'],[]);
    mkdir(fdir)
    prefix=fullfile(pdir,[petname '_if'],[sid{1},'_',sid{2},'_',petname]);
    
    xls2aif(deblank(files(n,:)),prefix) 
end
