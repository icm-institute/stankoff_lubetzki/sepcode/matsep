function do_superdpa(petfile,siffile,maskfile,profiletxt,outputprefix)

[time,~,~,~]=read_SIF(siffile);
    
    
% output images
reffile  =[outputprefix '_refregion.nii'];
grayfile =[outputprefix '_grayratio.nii'];
bindfile =[outputprefix '_bindratio.nii'];
whitefile=[outputprefix '_whiteratio.nii'];
bloodfile=[outputprefix '_bloodratio.nii'];

% read 4D pet
fprintf(' -- Reading dynPET\n');
dynPET =spm_read_vols(spm_vol(petfile));

% open rois
fprintf(' -- Reading ROIs\n');
hdr=spm_vol(maskfile);
brainmask= spm_read_vols(hdr);


% do super
[ref,gray,bind,white,blood]=superdpa(dynPET,brainmask,time,profiletxt);
        
      
%% write output images
write_NIFTI(reffile  ,ref  ,hdr,0);
write_NIFTI(grayfile ,gray ,hdr,1);
write_NIFTI(bindfile ,bind ,hdr,1);
write_NIFTI(whitefile ,white ,hdr,1);
write_NIFTI(bloodfile,blood,hdr,1);

