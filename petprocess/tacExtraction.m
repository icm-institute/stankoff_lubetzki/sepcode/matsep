function [meanTac,TACs]=tacExtraction(dyn,mask)
% Create tac from a 4D pet and a binary mask
% [meanTac,TACs]=tacExtraction(dyn,mask)
%
% Return meanTac and All individual tacs
%
n=size(dyn,4);
ind=find(mask>0);
meanTac=zeros(n,1);

if nargout==2
    TACs=zeros(n,length(ind));
end

for f=1:n
    temp=dyn(:,:,:,f);
    meanTac(f)=sum(temp(ind))./length(ind);
    
    if nargout==2
        TACs(f,:)=temp(ind)';
    end
end


