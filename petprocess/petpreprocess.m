function [rpetfile,rmean,others]=petpreprocess(petfile,fr_fixed,fwhm)

if nargin<2
    fr_fixed=1:7;
end
if nargin<3
    fwhm=2;
end

petfile=deblank(char(petfile));
fprintf(' -- Doing %s \n',petfile);

others={};

[path,name,ext]=fileparts(petfile);

%% check if gunzip is necessary
if numel(ext)==3
    system(sprintf('gunzip %s',petfile))
    [~,name,ext]=fileparts(name);
    petfile=fullfile(path,[name ext]);
end

rmean =fullfile(path,['r' name ext]);
smooth=fullfile(path,[name '_smooth' ext]);
norm  =fullfile(path,[name '_norm' ext]);


others{2}=smooth;
others{3}=regexprep(smooth,'\.nii','.mat');
others{4}=norm;
others{5}=regexprep(norm  ,'\.nii','.mat');
others{6}=fullfile(path,['mean' name '_norm' ext]);


% if exist(fullfile(path,['r' name ext]),'file')
%     sprintf(' -- Skipping image exists!');
%     rpetfile=fullfile(path,['r' name ext]);
%     others={};
%     return
% end

hdr=spm_vol(petfile);
%% Smooth images
if ~ exist(smooth,'file')
    spm_smooth(petfile,smooth,[fwhm fwhm fwhm]);
else
    fprintf(' -- Skipping smooth %s \n',name);
end

%% Normalize
if ~exist(norm,'file')
    fprintf('fslmaths %s -inm 100 %s',smooth,norm);
    system(sprintf('fslmaths %s -inm 100 %s',smooth,norm));
    if exist([norm '.gz'],'file')
        system(sprintf('gunzip %s.gz',norm))
    end
else
    fprintf(' -- Skipping norm %s \n',name);
end

%% Realign

fr_moved =1:size(hdr,1);
fr_moved(fr_fixed)=[];
[rfile,~,rpetfile]=petRealign(norm,fr_fixed,fr_moved,petfile);
others{1}=rfile;
%% Compute partial mean only moving frames?
%meanfile= fullfile(path,['r' name '_mean' ext]);
%if ~exist(meanfile)
%    partialMean(rother,fr_moved,meanfile);
%else
%    fprintf(' -- Skipping mean %s \n',name);
%end

