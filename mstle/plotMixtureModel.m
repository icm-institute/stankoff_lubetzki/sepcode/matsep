function plotMixtureModel(gmclass,data,data_range)


dim=gmclass.NDimensions;


if ~exist('data_range','var') || size(data_range,1) ~= 2 || size(data_range,2) ~= dim
    data_range=zeros(2,dim);
    data_range(1,:)=min(data,[],1);
    data_range(2,:)=max(data,[],1);
end


if dim == 1

    nbins=101;
    step=(data_range(2)-data_range(1))/nbins;

    x=data_range(1):step:data_range(2);
    edges=x-step/2;



    if exist('data','var')
      [n,bins]=histc(data,edges);
      bar(x,n/(sum(n)*step));
      hold on 
    end
    plot(x,gmclass.pdf(x'));
    hold off

elseif dim==2

    h=ezcontour(@(x,y)gmclass.pdf([x y]),data_range(:,1)',data_range(:,2)',500);
    
    
else
        fprintf(' -- ERROR: %d dimension is not supported yet!\n',dim);
end