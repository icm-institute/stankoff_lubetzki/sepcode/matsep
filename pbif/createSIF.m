function [sif_max,time_max,aifs]=createPBIF(aiffiles,pbiffile,norminterval,endtime,alignpeaks,doplot)

%% Creates the average population PBIF from a series of files
%
% [pbif_max,time_max,sif_raw,time_raw,aifs]=createPBIF(aiffiles,pbiffile,norminterval,endtime,doplot)
% 
% aiffiles should be triexponentially fitted first
% norminterval: should be given in seconds... or all seconds that should be used
% alignpeaks: algingpeaks of all aif (it should improve the results)
% endtime: is the duration for all aif files (shifting the maximum should
% shorten this value)
%
%
% date: 09/02/2015
% author: D.Garcia-Lorenzo



if nargin < 5
    doplot=0;
end

% open aiffiles
aiffiles=char(aiffiles);


sif_time=linspace(0,endtime,endtime+1)';

% create figure for plots
if doplot
    figure
end



for a=1:size(aiffiles,1)
    
    % Read name and rawdata
    af=deblank(aiffiles(a,:));

    [~,name]=fileparts(af);    
    names{a}=name;

    [aifs(a).rawtime,aifs(a).rawaif]=readIF(af);
    
    %% Interpolate all input fuctions linearly up to seconds and to the same final distance
    % This is uncessary if triexp fitted before
    aifs(a).time=sif_time; % interpolate by second
    aifs(a).aif =interp1(aifs(a).rawtime,aifs(a).rawaif,aifs(a).time,'linear','extrap');

end


%% Combine all data
ttimes=[aifs.time];
taifs =[aifs.naif];

% No-aligned SIF
sif_raw=mean(taifs,2);

%% CENTERING ON THE MAXIMUM
% find max for all subjects
[ymax, imax]=max(taifs,[],1);

% newi => time index of the SIF peak
newi=round(mean(imax));

%%%%% CHECK THE REST!!!!!

% Find shift at the begining and end
shift_begin=imax-newi;
shift_end  =max(shift_begin)-shift_begin;

newlength=size(taifs,1)-max(shift_begin);
centered=zeros(newlength,size(taifs,2));
ctime  =linspace(0,newlength-1,newlength)';

if doplot
    figure
end
for a=1:size(taifs,2)
    
    % Shift images
    centered(:,a)=taifs ((1+shift_begin(a)):(end-shift_end(a)),a);
    aifs(a).caif =centered(:,a);
    aifs(a).ctime=ctime;
    
    %% plot
    if doplot
        % plotting raw
        subplot(2,2,1)
        plot(aifs(a).time,aifs(a).naif,'x');
        hold on
        subplot(2,2,2)
        plot(aifs(a).time(aifs(a).time<600),  aifs(a).naif(aifs(a).time<600),'x-');
        hold on
        % plotting interpolated normalized
        subplot(2,2,3)
        plot(aifs(a).ctime,aifs(a).caif,'x');
        hold on
        subplot(2,2,4)
        plot(aifs(a).ctime(aifs(a).ctime<600),  aifs(a).caif(aifs(a).ctime<600),'x-');
        hold on

    end

end

pbif_max=mean(centered,2);
time_max=ctime;
time_raw=ttimes(:,1);

if ~isempty(pbiffile)
    csvwrite(pbiffile,[time_max pbif_max]);
end
