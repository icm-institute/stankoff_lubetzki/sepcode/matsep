function createParameters_SM_EC_EPI(DTI_f_in,t1suffix,t1mask,out_folder)

k = 1;

% Parameter file for scripting. Example of usage:
% E_DTI_SMECEPI_Main('C:\Data\Parameters_SM_EC_EPI.txt')
% ------------------------------------------------------
% 
txt{k,1} = {['par.DTI_f_in = ',char(39),DTI_f_in,char(39),';']};
k = k + 1;
% Full path name of uncorrected DTI *.mat file.
% Alternatively, this can be the path to a folder that
% contains DTI *.mat files.
% Example: par.DTI_f_in = 'C:\Data\DTI.mat';
% ------------------------------------------------------
% 
txt{k,1} = {['par.TE.NS = 4;']};
k = k + 1;
% Tensor estimation for 'native' space data:
% OLLS = 1, WLLS = 2, NLLS = 3, ROBUST = 4
% ------------------------------------------------------
% 
txt{k,1} = {['par.TE.TS = 4;']};
k = k + 1;
% Tensor estimation for 'transformed' space data:
% OLLS = 1, WLLS = 2, NLLS = 3, ROBUST = 4
% ------------------------------------------------------
% 
txt{k,1} = {['par.ROBUST_option = 1;']};
k = k + 1;
% 1 = linearized version (fast); 2 = non-linear version (slow)
% ------------------------------------------------------
% 
txt{k,1} = {['par.cust_mask.NS = ',char(39),char(39),';']};
k = k + 1;
% File name suffix of *.nii mask file in 'native' space.
% ------------------------------------------------------
% 
txt{k,1} = {['par.cust_mask.TS = ',char(39),t1mask,char(39),';']};
k = k + 1;
% File name suffix of *.nii mask file in 'transformed' space.
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.NS.mfs = 5;']};
k = k + 1;
% Masking parameter for 'native' space data: morphological
% filter size (uneven integer >= 3)
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.NS.NDWI = 0.7;']};
k = k + 1;
% Masking parameter for 'native' space data: tuning factor
% for b=0 s/mm2 image with typical value range of [0.5 1].
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.NS.DWI = 0.7;']};
k = k + 1;
% Masking parameter for 'native' space data: tuning factor
% for b~=0 s/mm2 image with typical value range of [0.5 1].
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.TS.mfs = 9;']};
k = k + 1;
% Masking parameter for 'transformed' space data: morphological
% filter size (uneven integer >= 3)
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.TS.NDWI = 0.7;']};
k = k + 1;
% Masking parameter for 'transformed' space data: tuning factor
% for b=0 s/mm2 image with typical value range of [0.5 1].
% ------------------------------------------------------
% 
txt{k,1} = {['par.mask_P.TS.DWI = 0.7;']};
k = k + 1;
% Masking parameter for 'transformed' space data: tuning factor
% for b~=0 s/mm2 image with typical value range of [0.5 1].
% ------------------------------------------------------
% 
txt{k,1} = {['par.DOF = 3;']};
k = k + 1;
% Type of transformation: 1 = unity transform, 2 = rigid-body,
% 3 = affine.
% ------------------------------------------------------
% 
txt{k,1} = {['par.R2D.type = 3;']};
k = k + 1;
% 0 = no transformation to other data space, 1 = rigid
% transformation to single data set, 2 = rigid transformation
% to 'associated' (via suffix definition) *.nii files,
% 3 = EPI unwarping (non-rigid).
% ------------------------------------------------------
% 
txt{k,1} = {['par.R2D.FN = ',char(39),t1suffix,char(39),';']};
k = k + 1;
% File name (par.R2D.type=1) or suffix (par.R2D.type=2/3).
% ------------------------------------------------------
% 
txt{k,1} = {['par.R2D.contrast = 1;']};
k = k + 1;
% 1 = non-DWI; 2 = FA; 3 = avg(DWIs)
% ------------------------------------------------------
% 
txt{k,1} = {['par.Num_iter = 1000;']};
k = k + 1;
% Number of iterations during correction for subject motion
% and eddy current induced distortions (integer > 1).
% ------------------------------------------------------
% 
txt{k,1} = {['par.Num_samp = 2000;']};
k = k + 1;
% Number of data samples during correction for subject
% motion and eddy current distortions (integer > 500).
% ------------------------------------------------------
% 
txt{k,1} = {['par.Hist_bin = 64;']};
k = k + 1;
% Number of histogram bins during correction for subject
% motion and eddy current distortions (16, 32, 64, or 128).
% ------------------------------------------------------
% 
txt{k,1} = {['par.Num_Resol = 1;']};
k = k + 1;
% Number of resolutions during correction for subject motion
% and eddy current distortions (integer > 0).
% ------------------------------------------------------
% 
txt{k,1} = {['par.Interpol = 1;']};
k = k + 1;
% Interpolation approach (1 = linear; 2 = cubic spline).
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Num_iter = 1000;']};
k = k + 1;
% Number of iterations during registration to other data
% (integer > 1).
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Num_samp = 20000;']};
k = k + 1;
% Number of data samples during registration to other data
% (integer > 500).
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Hist_bin = 64;']};
k = k + 1;
% Number of histogram bins during registration to other data
% (16, 32, 64, or 128).
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Num_Resol = 4;']};
k = k + 1;
% Number of resolutions during registration to other data
% (integer > 1).
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Grid_Spacing = [30  30  30];']};
k = k + 1;
% Grid spacing in mm.
% ------------------------------------------------------
% 
txt{k,1} = {['par.EPI.Deriv_Scales = [1  1  1];']};
k = k + 1;
% Allow deformation along axes (set to 0 to constrain
% deformation along that axis.
% ------------------------------------------------------
% 
txt{k,1} = {['par.Regul = 0;']};
k = k + 1;
% Regularize data (e.g., for low SNR data) - only for
% registration purposes (0 = no; 1 = yes).
% ------------------------------------------------------
% 
txt{k,1} = {['par.suff.NS = ',char(39),'_MD_C_native.mat',char(39),';']};
k = k + 1;
% File name suffix for corrected data in 'native' space.
% ------------------------------------------------------
% 
txt{k,1} = {['par.suff.TS = ',char(39),'_MD_C_trafo.mat',char(39),';']};
k = k + 1;
% File name suffix for data in 'transformed' space.
% ------------------------------------------------------
% 
txt{k,1} = {['par.out_folder = ',char(39),out_folder,char(39),';']};
k = k + 1;
% Output folder name.
% ------------------------------------------------------
% 
txt{k,1} = {['par.clean_up_PIS = 1;']};
k = k + 1;
% Adjust voxels that have physically implausible signals
% (0 = no, 1 = yes)
% ------------------------------------------------------
% 
txt{k,1} = {['par.temp_folder = ',char(39),out_folder,char(39),';']};
k = k + 1;
% Name of 'temporary' folder.
% ------------------------------------------------------
% 
txt{k,1} = {['par.RE.rel_convergence = 0.001;']};
k = k + 1;
% Convergence criterium: relative change.
% ------------------------------------------------------
% 
txt{k,1} = {['par.RE.max_iter = 20;']};
k = k + 1;
% Convergence criterium: maximum number of iterations.
% ------------------------------------------------------
% 
txt{k,1} = {['par.RE.kappa = 6;']};
k = k + 1;
% Outlier definition: outlier = value > P(75) + kappa*IQR.
% ------------------------------------------------------
% 
txt{k,1} = {['par.DKI_constraints.do_it = 1;']};
k = k + 1;
% For DKI, if using constraints, set to 1 (otherwise, 0).
% ------------------------------------------------------
% 
txt{k,1} = {['par.DKI_constraints.constr1 = [-Inf  Inf];']};
k = k + 1;
% Constraints for the 'positive' KT elements [-inf inf].
% ------------------------------------------------------
% 
txt{k,1} = {['par.DKI_constraints.constr2 = [-Inf  Inf];']};
k = k + 1;
% Constraints for the 'other' KT elements [-inf inf].
% ------------------------------------------------------
% 

EDdir = fileparts(which('MainExploreDTI'));

if isunix
    elastix     = fullfile(EDdir,'Source','MD_cor_E','linux64','elastix_L_64');
    transformix = fullfile(EDdir,'Source','MD_cor_E','linux64','transformix_L_64');
    % Code to run on Linux plaform
elseif ispc
    elastix     = fullfile(EDdir,'Source','MD_cor_E','win64','elastix_64.exe');
    transformix = fullfile(EDdir,'Source','MD_cor_E','win64','transformix_64.exe');
    % Code to run on Windows platform
else
    disp('Platform not supported')
    return
end

txt{k,1} = {['par.E_path = ',char(39),elastix,char(39),';']};
k = k + 1;
% The path to the coregistration executable.
% ------------------------------------------------------
% 
txt{k,1} = {['par.T_path = ',char(39),transformix,char(39),';']};
k = k + 1;
% The path to the transformation executable.
% ------------------------------------------------------
% 
txt{k,1} = {['par.MDC_constr_fac = 6;']};
k = k + 1;
% Tuning factor for scales/skews when correcting
% eddy current distortions [0, 1, 2,... 10].
% ------------------------------------------------------

%% write Parameters_SM_EC_EPI.txt
txtfile = fullfile(out_folder,'Parameters_SM_EC_EPI.txt');
fid = fopen(txtfile,'w');
for i = 1 : k - 1    
    fprintf(fid,'%s ', char(txt{i,:}) );
    fprintf(fid,'\n');
end
fclose(fid);
