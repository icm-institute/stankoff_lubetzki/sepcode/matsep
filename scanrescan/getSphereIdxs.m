function idxs=getSphereIdxs(center, radius, image)
% from a point in an image, get the indexes of a sphere of radius
%
% Warning: distances are set in voxel size
% 

% get dimensions
dims=size(image);

% transform idx to voxel dims if necessary
if numel(center)==1
    %fprintf(' -- Changing idx to ijk: ');
    [center(1),center(2),center(3)]=ind2sub(dims,center);
    %fprintf('%d\t',center);
    %fprintf('\n');
end

% get the box including the sphere
minv=floor(max([1 1 1]   ,center-radius));
maxv=floor(min(dims,center+radius+0.5));

% Diameter
spdims=(maxv-minv)+1;

% Set box as a vector to simplify distance stimation
x=repmat(minv(1):maxv(1)',[1 spdims(2) spdims(3)]);
y=repmat(minv(2):maxv(2),[spdims(1) 1 spdims(3)]);
z=repmat(reshape(minv(3):maxv(3),1,1,spdims(3)),[spdims(1) spdims(2) 1]);
xyz=[x(:) y(:) z(:)];

% Compute all distances
dist=sum(bsxfun(@minus,xyz,center).^2,2)-radius.^2;

% Select idxs inside the sphere
xx=[xyz(dist<=0, 1), xyz(dist<=0, 2), xyz(dist<=0, 3)];

% convert spheres to mask
idxs=sub2ind(dims,xyz(dist<=0,1),xyz(dist<=0,2),xyz(dist<=0,3));





