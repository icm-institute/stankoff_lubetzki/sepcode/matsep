function bg2 = BrainMask3D(inimg,output)
%% Main to extract a brain mask
% inimg: path to the input image
% seuil: 
% taille: height of the brain (estimation) in pixel

    [inimg,hdr] = readNIFTI(inimg);
    [l,c,p] = size(inimg);

    [IDX,C] = kmeans(inimg(:),2,'start','cluster','distance','cityblock','emptyaction','singleton');    
    id = reshape(IDX,size(inimg));
    
    [~,ind] = max(C);
    brain = id == ind;

    bg2 = zeros(l,c,p);
    for j = 1:c
        tmp = squeeze(brain(:,j,:));
        tmp = bwareaopen(tmp,300,4);
        bg2(:,j,:) = reshape(tmp,l,1,p);
    end
    
    for i = 1:l
        tmp = squeeze(bg2(i,:,:));
        tmp = bwareaopen(tmp,300,4);
        bg2(i,:,:) = reshape(tmp,1,c,p);
    end
    
    for k = 1:p
        tmp = squeeze(bg2(:,:,k));
        tmp = bwareaopen(tmp,300,4);
        bg2(:,:,k) = reshape(tmp,l,c,1);
    end
    
    for i = 1:l
        tmp = squeeze(bg2(i,:,:));
        tmp = bwareaopen(tmp,300,4);
        tmp = Remplissage(tmp);
        bg2(i,:,:) = reshape(tmp,1,c,p);
    end
    
    
    writeNIFTI(output,bg2,hdr);