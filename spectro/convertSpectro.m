function convertSpectro(indir,outdir)
%%
% indir : input directory for a subject and a timepoint
% outdir: directory to store the output
% type: tissu studied like TH,WM
    wr = explore_spectro_data(char(indir));
    write_fid_to_nii(wr,outdir)
end

