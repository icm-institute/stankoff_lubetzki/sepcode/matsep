function FindDemy(im_4d_distance_name,im_4d_pib_name,pib1_name,dist2csf_name,thrin,odir,sujid)

[im_4d_pib,hdr_4d_pib]  = readNIFTI(im_4d_pib_name,1);
[im_4d_dist,hdr_4d_dist] = readNIFTI(im_4d_distance_name,1);
thr            = str2num(thrin);

[dx,dy,dz,~]   = size(im_4d_pib);

vett           = 0:0.2:20;
[~,di]         = size(vett);
mask_gm4d      = im_4d_dist > 0;
im_index       = mask_gm4d.*(floor(im_4d_dist/0.2));

vett_threshold = zeros(di,2);
for ii = 1:1:di
    if ii < 101
        vett_threshold(ii,1) = mean(im_4d_pib(im_index==ii));
        vett_threshold(ii,2) = std(im_4d_pib(im_index==ii));
    else
        vett_threshold(di,1) = mean(im_4d_pib(im_index>=di));
        vett_threshold(di,2) = std(im_4d_pib(im_index>=di));
    end
end

mask_nan   = isnan(vett_threshold);
[a,b]      = bwlabel(mask_nan(:,1));
val_max    = di-sum(a==max(b));
vett_threshold(mask_nan>0) = 0;

thresholds = zeros(di,1);

for ii = 1:val_max
    if ii > 1
    if vett_threshold(ii,1) == 0
	thresholds(ii,1) = abs((vett_threshold(ii+1,1)*thr)+(vett_threshold(ii-1,1)*thr))/2;
    else
        thresholds(ii,1) = vett_threshold(ii,1)*thr;
    end
    end
end

thresholds(val_max+1:end,1) = mean(thresholds(val_max-20:val_max,1));

obj_fit    = spline(vett',thresholds);
thresholds = ppval(obj_fit,vett');
thresholds(thresholds<0)=0;

[pib1,hdr3] = readNIFTI(pib1_name,1);
[pib_dist,hdr_dist] = readNIFTI(dist2csf_name,1);

mask_gm         = pib_dist>0;
index_pib_im    = floor(pib_dist/0.2)+1;
index_pib_im(index_pib_im>di)=di;

demy_tp1 = zeros(dx,dy,dz);

for zz=1:dz
    for yy=1:dy
        for xx=1:dx
            if mask_gm(xx,yy,zz)>0
                threshold_vox=thresholds(index_pib_im(xx,yy,zz),1);
                if pib1(xx,yy,zz)<threshold_vox
                    demy_tp1(xx,yy,zz)=1;
                end
            end
        end
    end
end

writeNIFTI(fullfile(odir,[sujid '_demyBase_' num2str(thr,2) '.nii.gz']),demy_tp1,hdr3,1,1);

vett_val=[sum(sum(sum(mask_gm))), sum(sum(sum(demy_tp1)))];
dlmwrite(fullfile(odir,[sujid '_thrWMpib2csf_' num2str(thr,2) 'perc_baseline.txt']),vett_val)

exit


