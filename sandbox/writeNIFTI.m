function writeNIFTI(filename,ima,hdr,forcefloat,dozip)
% Function to read .nii and .nii.gz files
% writeNIFTI(filename,ima,hdr,forcefloat,dozip)
%
% if rezip=0, image is not gzip again, useful for several inputs
% It is based completely in SPM and Linux
% It checks if extension is .gz and gunzip image.
% If necessary it gunzip it at the end.

if nargin<5
    dozip=0;
end


if regexp(filename,'gz$')
    filename=regexprep(filename,'.gz$','')
    fprintf(' -- Compressed output %s',filename);
    dozip=1
end

for n=1:numel(hdr)

    if nargin>=4 && forcefloat
            hdr(n).dt=[16 0];
    end

    hdr(n).fname=deblank(char(filename));
    spm_write_vol(hdr(n),ima(:,:,:,n));
end
% gzip image if necessary
if dozip
    system(['gzip -f ' deblank(char(filename))]);
end