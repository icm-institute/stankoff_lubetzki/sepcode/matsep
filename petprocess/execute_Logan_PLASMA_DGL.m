
function [Vt,SD_Vt,CV_Vt]= execute_Logan_PLASMA_DGL(time,tac,time_pl,plasma,t_star,displ,title_str)
% Generic times are allowed
if nargin< 7
    notitle=1;
else
    notitle=0;
end
if nargin <6
    displ=0;
end

if max(time)>300
    time=time./60;
end
if max(time_pl)>300
    time_pl=time_pl./60;
end

if max(tac)>1000
    tac=tac./1000;
end
if max(plasma)>1000;
    plasma=plasma./1000;
end



tt=linspace(time_pl(1),time_pl(end),round(time_pl(end))*100);
pp=interp1(time_pl,plasma,tt,'linear');

%% Remove points if outside the range of the image
tac = tac(time<time_pl(end));
time=time(time<time_pl(end));
% Compute integral for plasma frames
%frames_pl=zeros(size(tt));
%frames_pl(1)=2*tt(1);
%for i=2:length(tt)
%    frames_pl(i)=2*(tt(i)-sum(frames_pl));
%end
%integral_Cp=cumsum(pp.*frames_pl);
%
integral_Cp = cumtrapz(tt,pp);
integral_Cp = interp1(tt,integral_Cp,time,'linear');
% Compute integral for tac
%frames(1)=2*time(1);
%for i=2:length(time)
%    frames(i)=2*(time(i)-sum(frames));
%end
%integral_Ct=cumsum(tac.*frames');
integral_Ct=cumtrapz(time,tac);

y_axis=integral_Ct./tac;
x_axis=integral_Cp./tac;


%indices for which t>t*
qq=find(t_star<time);

y_axis2=y_axis(qq);
n_good_frames=length(y_axis2);

X=[x_axis(qq),ones(1,n_good_frames)'];

param=inv(X'*X)*X'*y_axis2;
y=x_axis*param(1)+param(2);
J=sum((X*param-y_axis2).^2);
covar=J/(n_good_frames-2)*inv(X'*X);
pstderr=sqrt(diag(covar));

ystddev=zeros(1,length(time));

if displ
    %arbitrarily exclude the first 3 frames from the plot of the Ct /Cref ratio
    figure
    plot(x_axis(4:length(x_axis)),y_axis(4:length(x_axis)),'+r','linewidth',4)
    hold on
    plot(x_axis(qq),x_axis(qq)*param(1)+param(2),'b')
    xlabel('INTEGRAL(Plasma) / Tissue  (minutes)')
    ylabel('INTEGRAL(Tissue) / Tissue  (minutes)')
    if ~notitle
        title(title_str)
    end
    hold off
end
Vt=param(1);
SD_Vt=pstderr(1,1);
CV_Vt=SD_Vt/Vt*100;
if isinf(CV_Vt) || isnan(CV_Vt) || (CV_Vt<0)
    Vt=-1;
    CV_Vt=-1;
    SD_Vt=-1;
end
