function cluster=multitacExtraction(permPET,rois,labels,time,delta,ids)
% Allows to extract several tacs from a labeled image
%
% dynPET: 4D PET image 
% rois  : 3D image with several labels
% labels: names for each label
% time  : time line
% delta : delta
% ids   : select the number of the labes to extrat
if nargin < 6 || isempty(ids)
    classes=max(rois(:));
    ids=1:classes;
end


cl=1;
for x=ids(:)'
           
    select=rois==x;
    if sum(select(:)) == 0
        fprintf(' - Empty label %d\n',x);
        continue
    end
    
    if nargin>=3
        cluster(cl).name=char(labels(cl));
    else
        cluster(cl).name=sprintf('%d',x);
    end
    fprintf(' - Extracting label: %s \n',cluster(cl).name);
    
    [cluster(cl).tac,cluster(cl).TACs]=tacExtraction_perm(permPET,select);
    cluster(cl).NSD=sqrt(cluster(cl).tac./delta);
    cluster(cl).index=find(select);
    
    if nargin>=4
        cluster(cl).time=time;
    end
    cl=cl+1;
end