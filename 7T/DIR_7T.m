clc

% à 7T, selon Rooney - 2007 : 
T1_gm = 2.130; %s
T1_wm = 1.220; %s
T1_csf = 4.3; %s
T2_gm = 0.055; %s
T2_wm = 0.045; %s

TR = 8:0.5:10;%7:0.5:10;
TI2_fix = 0.450; %s
%TE = 0.1;
tcp = 0.00486; %TE/2; %s
TI1exp = [2.05; 2.63 ; 2.75 ; 3.05];
TI2exp = [0.45 ; 0.45 ;0.45 ;0.45 ];
figure;
for i=1:size(TR,2)
    
    TI1 = 0:1/(5*size(TR,2)-1):4; %s
    
    E1_csf = exp(-TI1/T1_csf);
    Ec_csf = exp(-TR(i)/T1_csf);
    Etcp_csf = exp(-tcp/T1_csf);
    TI2_csf = -T1_csf.*log( (Ec_csf.*(2.*(1./Etcp_csf)-1)-1) ./ (2.*(E1_csf-1)) );

    E1_wm = exp(-TI1/T1_wm);
    Ec_wm = exp(-TR(i)/T1_wm);
    Etcp_wm = exp(-tcp/T1_wm);
    TI2_wm = -T1_wm.*log( (Ec_wm.*(2.*(1./Etcp_wm)-1)-1) ./ (2.*(E1_wm-1)) );

    E1_gm = exp(-TI1/T1_gm);
    Ec_gm = exp(-TR(i)/T1_gm);
    Etcp_gm = exp(-tcp/T1_gm);
    TI2_gm = -T1_gm.*log( (Ec_gm.*(2.*(1./Etcp_gm)-1)-1) ./ (2.*(E1_gm-1)) );
    
    droite = TI2_fix.*(0:1/(4*size(TR,2)-1):8);
    
    titletext = ['DIR for TR = ' num2str(TR(i)*1000) ' ms'];
    subplot(3,3,i);
    plot(TI2_csf,TI1,'r',TI2_wm,TI1,'b',TI2_gm,TI1,'g',0.45,2.05,'*m',0.45,2.63,'*g',0.45,2.75,'*c',0.45,3.05,'*r');
    line([0 ],[0 3.5],'Color','m');
    title(titletext);ylabel('TI1 (s)'); xlabel('TI2 (s)');
    xlim([0 2]);% ylim([1.15 1.30]);
    legend('CSF','WM','GM','TI=2500ms','TI=3080ms','TI=3200ms','TI=3500ms');
end
