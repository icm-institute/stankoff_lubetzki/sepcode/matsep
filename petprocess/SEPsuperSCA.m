function exception=SEPsuperSCA(petname,listname,projectdir)
exception='';

regexROI='allWM';
regexExclude='WMles';

if nargin<2
    listname = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

%% Read the list
subjects=readList(listname);

%% DOING THE PROCESSING

for n=1:size(subjects,2)
    
    inpetfolder=[petname 'preproc'];
    outpetfolder=[petname 'super'];
    
    % Find data acquisition sif information
    rawpetdir=get_subdir_regex(projectdir,'raw',subjects{n}{1},subjects{n}{2},['^' petname]);
    if isempty(rawpetdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    siffile =get_subdir_regex_files(rawpetdir,'sif$');
    [time,~,~,delta]=readSIF(siffile);
    
    % Find petfolder in the process folder
    petdir=get_subdir_regex(projectdir,'process',subjects{n}{1},subjects{n}{2},['^' inpetfolder]);
    if isempty(petdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    
    % Find pet image, corrected for movement
    pet=get_subdir_regex_files(petdir,'movcorr.nii');
    if isempty(pet)
        fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    pet=char(pet);
    
    
    % Find cortex, basal ganglia, wm, dir lesions
    roidir  =get_subdir_regex(projectdir,'process',subjects{n}{1},subjects{n}{2},['^' petname 'ROI']);
    ctxfile =get_subdir_regex_files(roidir,[regexROI '.*.nii']);
    if numel(ctxfile) ==0
        fprintf(' -- ERROR cortex mask not found! %s\%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end
    
    ctxlesfile=get_subdir_regex_files(roidir,[regexExclude '.*.nii']);
    if numel(ctxlesfile) ==0
        fprintf(' -- No gm lesions found %s-%s\n\n',subjects{n}{1},subjects{n}{2});
    end
    
    % get output dir
    outdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},outpetfolder,'');
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    
    % output images
    reffile  =fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_refregion.nii']);
    grayfile =fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_grayratio.nii']);
    bindfile =fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_bindratio.nii']);
    bloodfile=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_bloodratio.nii']);
    clusterfile=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_refregion_tac.mat']);
   
    if exist(clusterfile,'file') && exist(reffile,'file')
        fprintf(' -- SKIPPING: results already exists! %s-%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end
 

    % read 4D pet
    fprintf(' -- Reading dynPET\n');
    dynPET=readNIFTI(pet);
        
    % open rois
    fprintf(' -- Reading ROIs\n');
    [cortex,hdr]=readNIFTI(ctxfile);

    if numel(ctxlesfile) > 0
        ctxles=readNIFTI(ctxlesfile);
        cortex(ctxles>0)=0; 
        fprintf(' -- Removing lesions from the Cortex!!\n');
    end
     % do super
    if strcmp(petname(1:2),'re')
        [petname(3:end) '_POPClass3.txt']
        [ref,gray,bind,blood]=superpib(dynPET,cortex,time,[petname(3:end) '_POPClass3.txt']); % we normalize only using the cortex
    else
        [ref,gray,bind,blood]=superpib(dynPET,cortex,time,[petname '_POPClass3.txt']); % we normalize only using the cortex
    end
      
       
    %% write output images
    writeNIFTI(reffile  ,ref  ,hdr,0);
    writeNIFTI(grayfile ,gray ,hdr,1);
    writeNIFTI(bindfile ,bind ,hdr,1);
    writeNIFTI(bloodfile,blood,hdr,1);
      
    % write output cluster
    tacs(1).time=time;
    [tacs(1).tac,tacs(1).TACs]=tacExtraction(dynPET,ref);
    tacs(1).NSD=sqrt(tacs(1).tac./delta);
    tacs(1).name='REFREGION';
    tacs(1).index=find(ref>0);
    
    save(clusterfile,'tacs');
    
end
