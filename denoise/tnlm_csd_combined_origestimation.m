function [denoised]=tnlm_csd_combined_origestimation(permPET,mask,beta,searchsize,cout,tzero,helpPET,tconst,tconstassigma,coutonlyinestimation)
%% temporal NLM including:
% - cout  : to remove correlated box from estimation
% - tconst: to weight each frame independently
% - mpet  : to compute the weights in it


if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('cout','var')
    cout=1;
end
if ~exist('beta','var')
    beta=0.4;
end
if ~exist('tzero','var')
    tzero=5;
end

if ~exist('tconstassigma','var')
    tconstassigma=0;
end
if ~exist('coutonlyinestimation','var')
    coutonlyinestimation=0;
end

%% Create average image
fsize=5;
mpet = convn(permute(helpPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
mpet = single(permute(mpet,[4 1 2 3]));    

%% 
dims   =size(permPET);


% output image
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 


%% Start processing voxels
for k=1:dims(4) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(3)
for i=1:dims(2)
    
    if mask(i,j,k)==0
        continue
    end
    
    tac=helpPET(:,i,j,k);
    mtac= mpet(:,i,j,k);
    truetac=permPET(:,i,j,k);
    
    if tconstassigma
        sdinv2=1./tconst.^2;
    else
        sdinv2  = 1.0 ./(mtac .* tconst);
    end
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(2),i+searchsize);
    Jmax=min(dims(3),j+searchsize);
    Kmax=min(dims(4),k+searchsize);
    %% First: estimate min distance USING ORIGINAL IMAGE!!
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        
        %% skipping center of the square
        if (i-x) <= cout && (x-i) <=cout && ...
           (j-y) <= cout && (y-j) <=cout && ...
           (k-z) <= cout && (z-k) <= cout
            continue
        end
        
        d=((truetac - permPET(:,x,y,z) - mtac + mpet(:,x,y,z)).^2).*sdinv2;
        
        mindist=min([mindist,sum(d(tzero:end))]);

    end
    end
    end


    %% Apply weights
    wmax=0;
    dtac=zeros(dims(1),1);
    wsum=0;

    betamin=beta*mindist;
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        %% skipping center of the square
        if ~coutonlyinestimation && ...
           (i-x) <= cout && (x-i) <=cout && ...
           (j-y) <= cout && (y-j) <=cout && ...
           (k-z) <= cout && (z-k) <= cout
            continue
        end
        if (i==x) && (y==j) && (z==k)
            continue
        end

        d=((tac - helpPET(:,x,y,z)).^2)  .*sdinv2;

        w=exp(-sum(d(tzero:end))./betamin);
        if w > wmax
            wmax=w;
        end
        

        dtac =dtac+ w .*  permPET(:,x,y,z);
        wsum=wsum + w;
        
    end
    end
    end

    if wsum >0
        denoised(i,j,k,:)=(wmax * permPET(:,i,j,k) + dtac) ./(wsum+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end

