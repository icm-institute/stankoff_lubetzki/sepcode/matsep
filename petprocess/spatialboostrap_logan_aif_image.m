function [Vt,SD_Vt,CV_Vt]=spatialboostrap_logan_aif_image( time, dynPET, mask, time_pl, plasma, t_star, proba_real, iters, block,visualize)

% Pre-processing
if time_pl(1)~=0;
    time_pl=[0;time_pl];
    plasma =[0;plasma];
end

if time_pl(end)> 150
    time_pl=time_pl./60;
end
if time(end) > 150
    time = time./60;
end

Vt   =zeros(size(mask));
SD_Vt=Vt;
CV_Vt=Vt;

items=size(dynPET,4);
if ~exist(visualize)
  visualize=0;
end

if ~exist(iters)  
  iters=200;
end
if ~exist(block)
  block=2;
end

%% Changed to optimize code
% We create a 2D matrix


for z=1:size(mask,3)
    for y=1:size(mask,2)
        for x=1:size(mask,1)
            if mask(x,y,z) <= 0
                continue
            end

            
            %% take original tac
            tac=squeeze(dynPET(x,y,z,:));
            
            %% prepare random samples
            % prepare neighborhood
            zvec=z+(-block:block);
            zvec=zvec(zvec>0 & zvec <= size(dynPET,3));
                        
            yvec=y+(-block:block);
            yvec=yvec(yvec>0 & yvec <= size(dynPET,2));
                        
            xvec=x+(-block:block);
            xvec=xvec(xvec>0 & xvec <= size(dynPET,1));
            
            % store samples in a vector without the center
            samples=zeros(numel(zvec)*numel(yvec)*numel(xvec)-1,items);
            
            % remove the tac point and store in vector
            for j=1:items
                theblock=dynPET(xvec,yvec,zvec,j);
                theblock=theblock(:);
                idx=find(theblock==tac(j),1);
                theblock(idx)=[];
                samples(:,j)=theblock(:);
            end
            
            %% figure, plot(time,samples)
            
            rnd=rand([iters items]); % choosing point
            vt_iters=zeros(iters,1);
            sd_iters=zeros(iters,1);
            
            %% DOING BOOTSTRAP
            btacs={};
            parfor i=1:iters
                % choos point for each item
                btac=tac;
                for j=1:items            
                    if rnd(i,j) > proba_real
                        %fprintf(' rand (%d,%d) = %1.2f\n',i,j,rnd(i,j))
                        btac(j)=samples(randi(size(samples,1),1),j);
                    end
                end
                btacs{i}=btac;
                [vt_iters(i),sd_iters(i)]=execute_Logan_PLASMA_DGL(time,btac,time_pl,plasma,t_star,0,'none');
            end
            
            %% weighted result
            boottacs=[btacs{:}];
            cv_iters=100.*sd_iters./vt_iters;
            sigma=quantile(cv_iters,0.05);
            wtac=wmean(sd_iters,boottacs,sigma);
            [Vt(x,y,z),SD_Vt(x,y,z),CV_Vt(x,y,z)]=execute_Logan_PLASMA_DGL(time,wtac,time_pl,plasma,t_star,0,'none');
            
            if visualize
                
                
                [vt_tac,sd_tac]=execute_Logan_PLASMA_DGL(time,tac,time_pl,plasma,t_star,0,'none');
                
                tacsss=[btacs{:}];
                
               
                
               
                

                fprintf('True vt %1.2f   - Wvt  %1.2f\n',vt_tac,vt_wtac); 
                figure,
                subplot(1,2,1),scatter(vt_iters,100.*sd_iters./vt_iters)
                hold on
                scatter(vt_tac,100.*sd_tac./vt_tac,'r')
                scatter(vt_wtac,100.*sd_wtac./vt_wtac,'g')
                hold off
                
                [~,minidx]=min(cv_iters);
                
                
                
                subplot(1,2,2),plot(time,tac,time,btacs{minidx},time,mean(boottacs,2),time,wtac)
                legend('Voxel','MinCV','Mean','Wmean')

                pause
                close all
            end
        end
    end
end

function mval=wmean(weights,values,sigma)

    weights=exp(-weights./sigma);
    mval=(values*weights)./sum(weights(:));
    
