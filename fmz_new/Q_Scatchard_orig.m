function [Bmax,Kd] = Q_Scatchard_orig(B,F)

mat(:,1) = B;
mat(:,2) = B./F;

po = [-0.4, 15];
[p,~,~]  = fminsearch(@funps,po,[],mat);

Bmax = -p(2)/p(1);
Kd   = -1/p(1);


function dist = funps(xxx,mat)

    [n,~] = size(mat);
    w = [ 1 2 2 2 1.5 ones(1,n-5) ];
    dist = 0;
    for i = 1:n
        dist = dist + (((mat(i,2)-xxx(1)*mat(i,1)-xxx(2))*5)^2 + 0.5*(mat(i,1)-mat(i,2)/xxx(1)+xxx(2)/xxx(1))^2)*w(i);
    end