function dist = funnl(par,mat,time)
    
    
    F = mat(:,1);
    B = mat(:,2);
    
    Bmax = par(1);
    Kd   = par(2);
    
    dataWeight = sqrt(abs(1./(B.*exp(0.034*time))));
    
    dist = dataWeight.*(B - (Bmax*F)./(Kd + F));

  