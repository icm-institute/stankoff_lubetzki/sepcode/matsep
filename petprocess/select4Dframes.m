function newlist = select4Dframes(file,frames)
% Create a spm-compatible list to use in spm programs selected frames from a 4D image
%
%
% filelist must be a cell array where each element is a char-array
% containing a list of file names.


nbframes=size(frames(:),1);
newlist=cell(1,nbframes);

for i=1:nbframes
    
    newlist{i}=sprintf('%s,%d',file,frames(i));
end
