function [ normDYN ] = permPET_normalization(DYN,map)
%the function normalizes the PET dynamic data respect to the voxel define in map

DIM=size(DYN);
normDYN=zeros(size(DYN));
ind=find(map>0);

mval=mean(DYN(:,ind),2);
sval=std(DYN(:,ind),0,2);

normDYN=bsxfun(@rdivide,bsxfun(@minus,DYN,mval),sval);
