%	FUNhw
%	fit du Hanes-Woolf dans la methode a saturation partielle

function dist = funhw(par,mat)
    
    n   = size(mat,1);
    FoB = mat(:,2);
    F   = mat(:,1);
    
    Bmax = par(1);
    Kd   = par(2);
    
    % number below based on FUNps (fit du Scatchard dans la methode a
    % saturation partielle). 
    % ---------------------------------------------------------------------
    dataWeight  = sqrt([ 1 2 2 2 1.5 ones(1,n-5) ]');
    part1Weight = 5;
    part2Weight = sqrt(0.5);
    % ---------------------------------------------------------------------
    
    dist1 = dataWeight.*(FoB - F/Bmax - Kd/Bmax);
    dist2 = dataWeight.*(F - FoB*Bmax + Kd);
    
    dist = [part1Weight*dist1(:); part2Weight*dist2(:)];
  