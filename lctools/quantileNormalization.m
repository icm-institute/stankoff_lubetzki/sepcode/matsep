function norm=quantileNormalization(ima,ymax,ymin,qmax,qmin)

%% Normalize each slice using two quantiles...
if (nargin < 5) || isempty(qmin)
    qmin=0.0;
end
if (nargin < 4) || isempty(qmax)
    qmax=.98;
end
if (nargin < 3) || isempty(ymin)
    ymin=0;
end
if (nargin < 2) || isempty(ymax)
    ymax=100;
end


% create output images
sizes=size(ima);
norm =zeros(sizes); % output image

% for each slice
for s=1:sizes(3)

    slice=ima(:,:,s);
    % compute min max quantiles
    xmin=quantile(slice(:),qmin);
    xmax=quantile(slice(:),qmax);

    % create output image
    slope=(ymax-ymin)/(xmax-xmin);
    norm(:,:,s)=slope.*(slice(:,:)-xmin)+ymin;
end

