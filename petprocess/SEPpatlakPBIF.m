function exception = SEPpatlakPBIF(petname,listname,projectdir,n_points)

exception = '';

if nargin < 2 || isempty(listname)
    listname    = spm_select(1,'any','Select subjects list');
    subjects    = readList(listname);
elseif ~exist(listname,'file')
    fprintf(' -- Considering input as ID');
    names       = regexp(listname,'/','split')
    subjects{1} = names;
else
    subjects    = readList(listname);
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir = getenv('PROJECT_DIR');
    end
end

if nargin < 4
    n_points = 6;  
end

%% DOING THE PROCESSING

load('NLMEM_phi1_musc_norm.mat')

for n = 1 : size(subjects,2)
    
    inpetfolder  = [petname 'preproc'];
    outputfolder = [petname 'patlak'];
    
    fprintf(' Subject %s/%s\n',subjects{n}{1},subjects{n}{2});
    
    % Find data acquisition sif information and venous samples
    rawpetdir = get_subdir_regex(projectdir,'raw',subjects{n}{1},subjects{n}{2},['^' petname]);
    if isempty(rawpetdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    siffile = get_subdir_regex_files(rawpetdir,'sif$');
    [time,~,~,delta] = readSIF(siffile);
    
    iffile  = get_subdir_regex_files(rawpetdir,'mat$');
    load(iffile{1},'info');
    infoCp = info;
    clear info
    
    % Find pet image, corrected for movement
    petdir  = fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},inpetfolder,'');
    pet     = get_subdir_regex_files(petdir,'movcorr.nii');
    
    if isempty(pet)
        fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    pet = char(pet);

    % Find brain mask to reduce processing
    roidir   = fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},[petname 'ROI'],'');
    maskfile = get_subdir_regex_files(roidir,'brainmask.*.nii');
    if isempty(maskfile) 
        fprintf(' -- ERROR brainmask not found! %s - %s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end

    % get output dir
    outdir = fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},outputfolder,'');
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    
    % test output exists
    Kifilename = fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_patlak_Ki.nii']);
    sdfilename = fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_patlak_sd.nii']);
    pbiffilename   = fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_pbif.fig']);
    patlakfilename = fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_patlak.fig']);
%     if exist(Kifilename,'file')
%         fprintf(' -- SKIPPING: results already exists! %s-%s\n\n',subjects{n}{1},subjects{n}{2});
%         continue
%     end
    
    % read 4D pet
    fprintf(' -- Reading dynPET\n');
%     dynPET = readNIFTI(pet); 
    
    nii    = load_nii(pet);
    dynPET = double(nii.img);
    
    % open brainmask
    fprintf(' -- Reading brainmask\n');
%     [mask,hdr] = readNIFTI(maskfile);
    nii  = load_nii(char(maskfile));
    mask = logical(nii.img);
    
    % unit conversion
    dynPET     = dynPET/1000;           % Bq/cc -> KBq/cc       
    infoCp.Cpv = infoCp.Cpv/1000;       % Bq/g -> KBq/g
    time       = time/60;               % s -> min
    
    % input function
    fprintf(' -- Generating Input Function\n');
    idx = infoCp.tCpv > 29;
    
    cal = mean(infoCp.Cpv(idx)./modelCp_4exp_norm(phi1',[],infoCp.tCpv(idx)));
    
    infoCp.par       = phi1';
    infoCp.fixed_par = cal;
    infoCp.FUN       = @modelCp_4exp_norm;
    
    
    % some plots
    % PBIF
    tv = [0;logspace(-4,log10(time(end)),1000)'];
    h1 = figure(1);
    plot(infoCp.tCpv,infoCp.Cpv,'o')
    hold on
    plot(infoCp.tCpv(idx),infoCp.Cpv(idx),'ro')
    plot(tv,modelCp_4exp_norm(phi1',cal,tv),'-')
    legend('Measured venous samples','Samples used for calibration','Calibrated PBIF')
    hold off
    savefig(h1,pbiffilename)
    
    % Patlak 
    Cp_tTAC    = modelCp_4exp_norm(infoCp.par,infoCp.fixed_par,time);
    Cpint_tTAC = zeros(size(time));
    for j = 1 : length(time)
        Cpint_tTAC(j) = integral(@(t)infoCp.FUN(infoCp.par,infoCp.fixed_par,t),0,time(j),'ArrayValued',true);
    end
    
    idx_pat = length(time) - n_points + 1 : length(time);   
    X       = [Cpint_tTAC./Cp_tTAC ones(size(Cp_tTAC))];
    TAC     = conv4Dto2D(dynPET,mask);
    Y       = mean(TAC,2)./Cp_tTAC;    
    [P,sdP] = lscov(X(idx_pat,:),Y(idx_pat));
    
    h2 = figure(2);
    plot(X(:,1),Y,'o-')
    hold on
    plot(X(idx_pat,1),Y(idx_pat),'or')
    plot(X(:,1),(P(1)*X(:,1) + P(2)),'.-r')
    legend('Patlak','Samples used for patlak','Patlak fit')
    title(['Whole brain Ki = ',num2str(P(1)),'  CV = ',num2str(100*sdP(1)/P(1))])
    hold off
    savefig(h2,patlakfilename)
    
    close all    

    % run Patlak
    fprintf(' -- Patlak\n');
    [mapKi,mapstdKi] = Patlak_voxel(dynPET,mask,time,infoCp,'modelled',n_points); 
    
    clear dynPET;
    
    %% write output images
    fprintf(' -- Save Results\n');
    
%     writeNIFTI(Kifilename,mapKi   ,hdr,1);
%     writeNIFTI(sdfilename,mapstdKi,hdr,1,1);
    
    nii.img = mapKi;
    save_nii(nii,Kifilename);
    nii.img = mapstdKi;
    save_nii(nii,sdfilename);
    
    
end
