function [WRSS,RSS,ALFA,SD_ALFA,CV_ALFA]=RPM_Solve_LinearModel(B1,B2,Ctarget,w)
% LS
X=[B1,B2];
Y=Ctarget;
n_good_data=length(find(w>0));
W=diag(w);
warning off
param=inv(X'*W*X)*X'*W*Y;
warning on
J=(Y-X*param)'*W*(Y-X*param);
warning off
covar=(J/(n_good_data-2))*inv(X'*W*X);
warning on
pstderr=sqrt(diag(covar));

%OUTPUT PARAMETERS
WRSS=J; 
RSS=(Y-X*param)'*(Y-X*param);
ALFA=param;
SD_ALFA=pstderr;
CV_ALFA=SD_ALFA./ALFA*100;
if max(isinf(CV_ALFA))==1 || max(CV_ALFA<0)==1
    CV_ALFA=-1*ones(size(param));
    SD_ALFA=-1*ones(size(param));
    ALFA=-1*ones(size(param));
end



