function [Vt,SD_Vt,CV_Vt]=logan_refregion(time,tac,reftac,n_points,nsd,name,disp)

if ~exist('name','var')
    name='';
    disp=0;
end

if ~exist('disp','var')
    disp=0;
end

% Pre-processing
if time(1)~=0;
    tt_time=[0;time(:)];
    tt_Cref=[0;reftac(:)];
    modified=true;
else
    tt_time=time(:);
    tt_Cref=reftac(:);
    modified=false;
end

integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end

[Vt,SD_Vt,CV_Vt]= execute_Logan_REF_weights(time,tac,integral_Cref,n_points,nsd,name,disp);