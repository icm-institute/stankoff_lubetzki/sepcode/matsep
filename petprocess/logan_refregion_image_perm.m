function [Vt,SD_Vt,CV_Vt]=logan_refregion_image_perm(time,permPET,reftac,n_points,nsd,mask)
DIM=size(permPET);

if ~exist('mask','var')
    
    mask=zeros(DIM(2),DIM(3),DIM(4));
end

if(max(permPET(:)) > 5000)
    permPET=permPET./1000; % converty to kBq
end
if(max(reftac(:)) > 5000)
    reftac=reftac./1000; % converty to kBq
end
if time(end) > 290
    time=time./60; % time in minutes
end

% Pre-processing
if time(1)~=0;
    tt_time=[0;time(:)];
    tt_Cref=[0;reftac(:)];
    modified=true;
else
    tt_time=time(:);
    tt_Cref=reftac(:);
    modified=false;
end

integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end


Vt   =zeros(size(mask));
SD_Vt=Vt;
CV_Vt=Vt;


%% Changed to optimize code % DOESN'T WORK, we should remove dynPET first!
% We create a 2D matrix

idx=find(mask);
for x=idx'
    [Vt(x),SD_Vt(x),CV_Vt(x)]= execute_Logan_REF_weights(time,permPET(:,x),integral_Cref,n_points,nsd,'none',false);
end
