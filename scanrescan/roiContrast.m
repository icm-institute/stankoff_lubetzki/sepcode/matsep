
function out=roiContrast(ima1,ima2,aseg1,aseg2,lut,regsizes,iters)
% %% DEPRECATED
% ima1=;
% ima2=;
%
% aseg1=;
% aseg2=;
%
% lutfile=;
% regsizes=;
% iter =;

% create output structure
out.regsizes=regsizes;

out.labels =lut.textdata;
out.mean   =zeros(numel(out.labels),numel(regsizes));
out.absmean=zeros(numel(out.labels),numel(regsizes));
out.quant95=zeros(numel(out.labels),numel(regsizes));
out.absqu95=zeros(numel(out.labels),numel(regsizes));


% compute for each label
for y=1:size(lut.data,1)
    
    
    valwm  =lut.data(y,1)
    valgm  =lut.data(y,2)
    themask1=(aseg1==valwm & ima1>0);
    themask2=(aseg2==valgm & ima2>0);
    fprintf(' -- Doing Region %s\n',char(lut.textdata(y)));
    sum(themask1(:))
    sum(themask2(:))
    % compute statistis for each region size
    for r=regsizes
        % get the random averages
        p1=getAveragePoints(ima1,themask1,r,iters,themask1);
        p2=getAveragePoints(ima2,themask2,r,iters,themask2);
        
       
        % compute difference
        % %relative_difference=difference/average
        diff=200.*(p1-p2)./(p1+p2);

        % option b: more stable?
        % diff=200.*(p1-p2)./(mean(p1(:))+mean(p2(:)));
        
        
        %% compute the final statistics
        out.mean(y,r)   = mean(diff);
        out.absmean(y,r)= mean(abs(diff));
        out.quant95(y,r)= quantile(    diff ,0.95);
        out.absqu95(y,r)= quantile(abs(diff),0.95);
        fprintf('    %d :: mean= %f2.2 absmean= %f2.2 q95= %f2.2  aq95= %f2.2\n', ...
                        r,out.mean(y,r),out.absmean(y,r),out.quant95(y,r),out.absqu95(y,r))
       
       
        
    end
end

