function [REFERENCE,GRAYRATIO,BINDINGRATIO,WHITERATIO,BLOODRATIO]=superdpa(dynPET,mask,time,profiletxt,thrs_ref,brainmask)
% Code for extraction of reference region
% dynPET = 4D matrix for the reference region
% mas    = 3D matrix with a binary mask
% time   = vector with the time for each frame (in seconds!)
% profile= txt file path with the profile for each class
%              REF BLOOD WHITE BINDING

if nargin<5
    thrs_ref=0.9;
end
if nargin<6
    brainmask=mask;
end

if ~exist('dynPET_normalization')
    p=genpath('/home/dgarcia/src/cenir_irm/MATTIA_SOFTWARE/UTILITY_forSUPERPIB');
    addpath(p);
end

DIM=size(dynPET);   %PET dimension
if time(end) <150
    time=time*60; % convert to seconds
end

dynPET(isnan(dynPET))=0;
fprintf('\nNORMALIZATION....');
[normDYN]=dynPET_normalization(dynPET,brainmask);
fprintf('COMPLETED\n');

%% Phase 3 - Loading class
a	= importdata(profiletxt);
[m,n]	= size(a);
tref	= a(:,1);
class	= a(:,2:n);


% Maximum frame usable (in theory all)
for maxk=length(time):-1:1,
   if time(maxk)<=tref(m),
       break;
   end
end

classI	= zeros(maxk,n-1);
for k=1:n-1,
    classI(:,k) = interp1(tref,class(:,k),time(1:maxk));
end

    %% Phase 5 - Reference Extraction
    fprintf(1,' \n Classes Extraction: %d Planes\n',DIM(3));
    %Class definition
    GRAYRATIO       = zeros(DIM(1:3));
    BLOODRATIO      = zeros(DIM(1:3));
    WHITERATIO      = zeros(DIM(1:3));
    BINDINGRATIO	= zeros(DIM(1:3));
    %Subset of voxel for the research of Reference 
    v		= zeros(maxk,1);
    %Clustering
    failed=0;
    total =1;
    for z=1:DIM(3),
        fprintf(1,'       ___Plane no. %d\n',z);
        for x=1:DIM(1),
            for y=1:DIM(2),
                if(mask(x,y,z)>0)
                    total=total+1;
                    v(:) 		= normDYN(x,y,z,1:maxk);
                    [bv,rnorm,~,eflag]  = lsqnonneg(classI,v);
                    if (sum(bv)>0)
                        GRAYRATIO(x,y,z)   = bv(1)/sum(bv);
                        BLOODRATIO(x,y,z)  = bv(2)/sum(bv);
                        WHITERATIO(x,y,z)  = bv(3)/sum(bv);
                        BINDINGRATIO(x,y,z)= bv(4)/sum(bv);
                    else
                        %fprintf(' %d,%d,%d error: %1.2f\n',x,y,z,100.*failed./total);
                        failed=failed+1;
                        %figure
                        %plot(time(1:maxk),v(:),tref,class)
                        %keyboard
                    end
                end
            end
        end
    end
    fprintf(' Total errors: %1.2f\n',100.*failed./total)
    %Extraction
    REFERENCE  = GRAYRATIO>thrs_ref;
    