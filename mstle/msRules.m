function segmentation=msRules(data,gmclass,lesTh)
%
%  segmentation=msRules(data,gmclass[,lesTh])
%
%  data: input data
%  gmclass: estimated gmdistribution
%  lesTh: threhsold for lesions (with respect to the normal probability
%          default = 0.05
%        It can be a vector of size dim(data,2) giving a different Th for each modality
%
N=size(data,1);
dim=size(data,2);

if dim==1
    error(' Insufficient number of dimensions in the data %d',dim);
end

if ~exist('lesTh','var')
    lesTh=0.05.*ones(1,dim);
elseif numel(lesTh) == 1
    lesTh=lesTh.*ones(1,dim);
elseif numel(lesTh) ~= dim
    error(' Different number of lesion Thresholds lesTh and dimensions');
end




muWM=gmclass.mu(end,:)
sdWM=sqrt(diag(gmclass.Sigma(:,:,end))');

th  =norminv(1-lesTh,muWM,sdWM)

if dim==3
    segmentation=( data(:,2) > th(2) & data(:,3) > th(3));
elseif dim==2
    segmentation=( data(:,2) > th(2) );
end


end