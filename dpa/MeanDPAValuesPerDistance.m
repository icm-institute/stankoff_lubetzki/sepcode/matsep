function MeanDPAValuesPerDistance(im_distance_name,im_dpa_name,thrin,ofile,sujid)
% im_distance_name
% im_dpa_name
% thrin : threshold you want to apply to add or remove x percent - to add
% 30%, write 1.3 - to remove 30%, write 0.7
% ofile
% sujid

	[im_dpa,hdr_dpa]  = readNIFTI(im_dpa_name,1);
	[im_dist,hdr_dist] = readNIFTI(im_distance_name,1);
	thr = str2num(thrin);

	[dx,dy,dz,dt]=size(im_dpa);

	vett = 0:0.2:20;
	[~,di] = size(vett);
	mask_dist = im_dist>0;
	im_index = mask_dist.*(floor(im_dist/0.2));

	vett_threshold=zeros(di,2);
	for ii=1:1:di
	    if ii<101
		vett_threshold(ii,1)=mean(im_dpa(im_index==ii));
		vett_threshold(ii,2)=std(im_dpa(im_index==ii));
	    else
		vett_threshold(di,1)=mean(im_dpa(im_index>=di));
		vett_threshold(di,2)=std(im_dpa(im_index>=di));
	    end
	end

	mask_nan = isnan(vett_threshold);
	[a,b]    = bwlabel(mask_nan(:,1));
	val_max  = di-sum(a==max(b)); 
	vett_threshold(mask_nan>0) = 0;

	thresholds = zeros(di,1);

	for ii=1:val_max
	    if ii>1 
		if vett_threshold(ii,1)==0;
		thresholds(ii,1)=abs((vett_threshold(ii+1,1)*thr)+(vett_threshold(ii-1,1)*thr))/2;
		else
		thresholds(ii,1)=vett_threshold(ii,1)*thr;
		end
	    end
	end

	thresholds(val_max+1:end,1)=mean(thresholds(val_max-20:val_max,1));
	obj_fit=spline(vett',thresholds); thresholds=ppval(obj_fit,vett');
	thresholds(thresholds<0)=0;

	vett_val = [repmat(sujid,di,1),repmat(' ',di,1),num2str(vett'),repmat(' ',di,1),num2str(vett_threshold(:,1)),repmat(' ',di,1),num2str(thresholds)];
	dlmwrite(ofile,vett_val,'-append','delimiter','');
