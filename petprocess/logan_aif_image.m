function [Vt,SD_Vt,CV_Vt]=logan_aif_image(time,dynPET,mask,time_pl,plasma,t_star)

% Pre-processing
if time_pl(1)~=0;
    time_pl=[0;time_pl];
end

Vt   =zeros(size(mask));
SD_Vt=Vt;
CV_Vt=Vt;

DIM=size(dynPET);
if 0
    %% Changed to optimize code
    % We create a 2D matrix
    % DOESN'T WORK: WE MUST remove dynPET!!

    idx=find(mask);

    imasize=size(dynPET,1)*size(dynPET,2)*size(dynPET,3);
    nsize=[imasize size(dynPET,4) ];

    mm=reshape(dynPET,nsize);
    for x=idx'
            [Vt(x),SD_Vt(x),CV_Vt(x)]=execute_Logan_PLASMA_DGL(time,mm(x,:)',time_pl,plasma,t_star,0,'none');
    end
end


for z=1:DIM(3),
    fprintf(1,'\n       ___Plane no. %d',z);
for x=1:DIM(1),
for y=1:DIM(2),
    if(mask(x,y,z)>0)
        [Vt(x,y,z),SD_Vt(x,y,z),CV_Vt(x,y,z)]=   ...
            execute_Logan_PLASMA_DGL(time,squeeze(dynPET(x,y,z,:)),time_pl,plasma,t_star,0,'none');
    end
end
end
end
