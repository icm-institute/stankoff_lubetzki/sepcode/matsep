function Modif_Header(list)
   
    load(list);
    for i = 1:size(img_list,1)
        [img,hdr] = readNIFTI(img_list(i,:));
        hdr.mat = hdr.mat*17;
        hdr.mat(end) = 1;
        hdr.mat = [ hdr.mat(1,:) ; hdr.mat(3,:) ; hdr.mat(2,:) ; hdr.mat(4,:) ];
        hdr.private.mat = hdr.mat;
        hdr.private.mat0 = hdr.mat;
    
        writeNIFTI(hdr.fname,img,hdr)
    end
   
