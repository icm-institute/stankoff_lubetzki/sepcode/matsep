function runVBM8(input)
%% help
% input : absolute path to the sequence


    t1 = {input};
    j = job_vbm8(t1);
    spm_jobman('run',j);
    disp(' -- Job done!');
    exit;
    
end