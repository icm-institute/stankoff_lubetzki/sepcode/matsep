function [Vt,SD_Vt,CV_Vt]=logan_refregion_image(time,dynPET,reftac,n_points,nsd,mask,quiet)
DIM=size(dynPET);

if ~exist('mask','var')
    
    mask=zeros(DIM(1),DIM(2),DIM(3));
end

if ~exist('quiet','var')    
    quiet=0;
end


% Pre-processing
if time(1)~=0;
    tt_time=[0;time(:)];
    tt_Cref=[0;reftac(:)];
    modified=true;
else
    tt_time=time(:);
    tt_Cref=reftac(:);
    modified=false;
end

integral_Cref = cumtrapz(tt_time,tt_Cref);
if modified
    integral_Cref=integral_Cref(2:end);
end


Vt   =zeros(size(mask));
SD_Vt=Vt;
CV_Vt=Vt;


%% Changed to optimize code % DOESN'T WORK, we should remove dynPET first!
% We create a 2D matrix
if 0
    idx=find(mask);
    nsize=[size(dynPET,1)*size(dynPET,2)*size(dynPET,3) size(dynPET,4) ];
    mm=reshape(dynPET,nsize);

    for x=idx'
        [Vt(x),SD_Vt(x),CV_Vt(x)]= execute_Logan_REF_weights(time,mm(x,:)',integral_Cref,n_points,nsd,'none',false);
    end
end

for z=1:DIM(3),
    if ~quiet
        fprintf(1,'\n       ___Plane no. %d',z);
    end
    for x=1:DIM(1),
        for y=1:DIM(2),
            if(mask(x,y,z)>0)
                [Vt(x,y,z),SD_Vt(x,y,z),CV_Vt(x,y,z)]= execute_Logan_REF_weights(time,squeeze(dynPET(x,y,z,:)),integral_Cref,n_points,nsd,'none',false);                       
            end            
        end 
    end    
end



