function idxs=getNeighborIdxs(idx,mask)
% fonction to find neighbor voxels to the idx
% they have to be inside the given mask
%
% return: list of neighbor idxs inside the mask

dims=size(mask);
[i,j,k]=ind2sub(dims,idx);
% get the origin
idxs=[];
if k< size(mask,3) && mask(i,j,k+1) 
    idxs=[idxs;    sub2ind(dims,i,j,k+1)];
end
if k>1             && mask(i,j,k-1) 
    idxs=[idxs;    sub2ind(dims,i,j,k-1)];
end
if j< size(mask,2) && mask(i,j+1,k) 
    idxs=[idxs;    sub2ind(dims,i,j+1,k)];
end
if j> 1             && mask(i,j-1,k) 
    idxs=[idxs;    sub2ind(dims,i,j-1,k)];
end
if i<size(mask,1) && mask(i+1,j,k) 
    idxs=[idxs;    sub2ind(dims,i+1,j,k)];
end
if i>1             && mask(i-1,j,k) 
    idxs=[idxs;    sub2ind(dims,i-1,j,k)];
end

