function tracks_interp = trk_interp_center(tracks,position, nPoints_new)
% TRK_INTERP_CENTER - Interpolate the tracks using the position as center
% of the track
% You should normally interpolate using trk_interp or trk_geointerp so all 
% tracks are aligned and so the position is meaninful for all tracks.
%
% Inputs: 
%  position = index in the tracks to be put in the middle
%  tracks   = tracks struct, normally previously interpolated
%  nPoints_new = number of final points, normally less than tracks


if nargin<2 || isempty(nPoints_new), nPoints_new = 51; end


nPoints_new = floor(nPoints_new/2)*2+1;
side = floor(nPoints_new/2);
nPoints_old = size(tracks(1).matrix,1);

tracks_interp   = zeros(nPoints_new, 3, length(tracks));

% Streamlines will all have the same # of vertices, but now they will be spread
% out so that an equal proportion lies on either side of a central origin. 

nbTrk=length(tracks);

for iTrk=1:nbTrk
    tracks_tmp = tracks(iTrk);
    
    % Determine streamline segment lengths
    segs = sqrt(sum((tracks_tmp.matrix(2:end,1:3) - tracks_tmp.matrix(1:(end-1),1:3)).^2, 2));
    dist = [0; cumsum(segs)];
    [ndist idx]=unique(dist,'first');

    % Redistribution the distances according to the new center
    newX=linspace(1,position,side+1);
    newX=[newX(1:(end-1))   linspace(position,idx(end),side+1)];
    newdist=interp1(idx, ndist, newX)';
        
    % Fit spline and apply the new points
    try
        tracks_interp(:,:,iTrk) = spline(ndist, tracks_tmp.matrix(idx,:)',newdist)';
    catch exception
        disp( exception)
        tracks_interp.dist=dist;
        tracks_interp.trk=tracks_tmp.matrix';
        tracks_interp.newdist=newdist;
        tracks_interp.iTrk=iTrk;
        break
    end
        
end
    


