function [denoised,nweights]=spatiotemporal_nlm_perm(permPET, mask, beta,patchsize,searchsize,tzero)


if ~exist('patchsize','var')
    searchsize=1;
end
if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end
if ~exist('mpet','var')
    %% Create average image
    fsize=4;
    mpet = convn(permute(permPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
    mpet = single(permute(mpet,[4 1 2 3]));
    
end



%% 
dims   =size(permPET);
nframes=dims(4)-tzero;



% output image
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 
nweights=zeros(dims(1:3));


%% Start processing voxels

for k=1:dims(4) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(3)
for i=1:dims(2)
    
    if mask(i,j,k)==0
        continue
    end
    
    tac =permPET(:,i,j,k);
    mtac= mpet(:,i,j,k);
    
    
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(2),i+searchsize);
    Jmax=min(dims(3),j+searchsize);
    Kmax=min(dims(4),k+searchsize);

    %% First: estimate min distance
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && y==j && z==k
            continue
        end

        d=distance2(permPET,mpet,i,j,k,x,y,z,patchsize,tzero);        
        mindist=min(mindist,d);
    end
    end
    end







    %% Apply weights
    wmax=0;
    dtac=zeros(dims(1),1);
    wmean=0;

    betamin=beta*mindist;
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && y==j && z==k
            continue
        end


        d=distance(permPET,i,j,k,x,y,z,patchsize,tzero);
        

        
        w=exp(-d/betamin);
        if w > wmax
            wmax=w;
        end
        dtac =dtac+ w .*  permPET(:,x,y,z);
        wmean=wmean + w;
        
    end
    end
    end
        
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) /(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end


end

function d=distance(ima,i,j,k,x,y,z,psize,tzero)

dims=size(ima);

%% Checking borders
xmin=- min([psize,x-1,i-1]);
ymin=- min([psize,y-1,j-1]);
zmin=- min([psize,z-1,k-1]);

xmax= min([psize,dims(2)-x,dims(2)-i]);
ymax= min([psize,dims(3)-y,dims(3)-j]);
zmax= min([psize,dims(4)-z,dims(4)-k]);

%% do the sum

tmp=  ima(tzero:end,i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax)) ...
    - ima(tzero:end,x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax));
   
d=sum(tmp(:).^2)./((xmax-xmin)*(ymax-ymin)*(zmax-zmin));
end

function d=distance2(ima,mima,i,j,k,x,y,z,psize,tzero)

dims=size(ima);

%% Checking borders
xmin=- min([psize x-1 i-1]);
ymin=- min([psize y-1 j-1]);
zmin=- min([psize z-1 k-1]);
xmax= min([psize,dims(2)-x,dims(2)-i]);
ymax= min([psize,dims(3)-y,dims(3)-j]);
zmax= min([psize,dims(4)-z,dims(4)-k]);
%% do the sum
tmp=   ima(tzero:end,i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax)) ...
    -  ima(tzero:end,x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax)) ...
    - mima(tzero:end,i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax)) ...
    + mima(tzero:end,x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax)); 
   
d=sum(tmp(:).^2)./((xmax-xmin)*(ymax-ymin)*(zmax-zmin));

end
