function FMZQuantificationROI(petfile,sujid,outdir,rawdir,ponsmask,roimask,dosef,siffile)
%% Quantification of the Flumazenil pet image
% Input : 
% - vfile : pet image in .nii/.nii.gz
% - sujid : label of your subject like C_XXX
% - outdir : directory where you want to store outputs
% default - if no outdir provided, outputs will be stored in the same
% directory as pet input
% - rawdir : raw directory containing Template directory with informations
% about pons on MNI for correction
% Output:
% - bmax : Quantification of changes in  benzodiazepine receptor density
% - kd : apparent FMZ equilibrium dissociation constante
    
    disp('');
    disp(' ---------------------------------------- ');
    disp(['Start quantification for ',sujid]);
    disp(' ---------------------------------------- ');
    disp('');
    tic;
    
    %% Input files
    
    % PET image
    [dynPET,~] = readNIFTI(char(petfile));
    
    % dose file
    dose  = load(char(dosef));
    
	% pons mask
    [Maskpons,~] = readNIFTI(char(ponsmask));
    
	% ROI template
    [atlas,~] = readNIFTI(char(roimask));
    
    % pons template for correction
    ponscorrdir = get_subdir_regex(rawdir,'Template');
    ponscorr    = get_subdir_regex_files(ponscorrdir,'corr_pons.dat');
    pons_corr   = load(char(ponscorr));
    
	% time
    [time,~,~,~] = readSIF(siffile); 
    time = time/60;
    
    %% PET conversion to pmol/ml
    ras   = dose(1);
    doseC = dose(2);
    doseF = dose(3);
    rap   = (doseC + doseF)/doseC;
    
    dynPET = dynPET./ras.*rap/37; % conversion to pmol/ml
    
    %% Temporary code
    TACs_name = {'Frontal','Cingulate','Occipital','Temporal','Parietal','Insular','Cerebellar','Thalamus'};
    atlas_prop = zeros(size(atlas));
    atlas_prop(atlas==1001 | atlas==2001) = 1;  % Frontal
    atlas_prop(atlas==1003 | atlas==2003) = 2;  % Cingulate
    atlas_prop(atlas==1004 | atlas==2004) = 3;  % Occipital
    atlas_prop(atlas==1005 | atlas==2005) = 4;  % Temporal
    atlas_prop(atlas==1006 | atlas==2006) = 5;  % Parietal
    atlas_prop(atlas==1007 | atlas==2007) = 6;  % Insular
    atlas_prop(atlas==8    | atlas==47)   = 7;  % Cerebellar
    atlas_prop(atlas==10   | atlas==49)   = 8;  % Thalamus
    
    [TACs,~,TACs_N] = extract_TACs(dynPET,atlas_prop);

    %% Pons gamma mode
    refTAC_vox = conv4Dto2D(dynPET,Maskpons);
    
    for j = 1 : length(time)
        pd   = fitdist(refTAC_vox(j,:)','Gamma'); 
        if pd.a > 1
            pons(j,1) = (pd.a - 1)*pd.b;
        else
            pons(j,1) = 0;
        end
    end
    ypons = fitPons(time,pons);

    % Correction of pons values
    per_corr = interp1(pons_corr(:,1),pons_corr(:,2),time);    
    F        = (1 - per_corr).*ypons; 
    
    % data to use
    m_debut = 3;
    idx     = (1 : length(F))' >= m_debut;
    
    %% Quantification
    for j = 1 : length(TACs_name)
        C = TACs(:,j);                  
        B = C - F;

        % Scatchard non linear        
        [Bmax_scaNL(j),Kd_scaNL(j)] = Q_Scatchard_orig(B(idx),F(idx));

    end
    
    %% Output files
    
    resultsfile = fullfile(outdir,strcat(sujid,'_ROIBmax.txt'));

    txt{1 ,1}={'# SUBJID'};
    txt{2 ,1}={'# ROI NVOXELS BMAX KD'};
    for j = 1 : length(TACs_name)
        txt{j+2 ,1} = {[TACs_name{j},' ',num2str(TACs_N(j)),' ', num2str(Bmax_scaNL(j)),' ',num2str(Kd_scaNL(j))]};
    end
    
    fileID = fopen(resultsfile,'w');
    for j = 1 : size(txt,1)
        fprintf(fileID,'%s ', char(txt{j,:}) );
        fprintf(fileID,'\n');
    end
    fclose(fileID);

end

