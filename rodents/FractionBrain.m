function FractionBrain(T1,spectros,output)
% inputs:
%   - T1 : String containing the full path to the T1 of the subject
%   - spectros : Matrix named 'spectro_list' (mandatory) containing all the
%   full path to your spectro images
%   - output: Full path to your output directory for the spectro volume

    job = job_vbm8(cellstr(T1));
    spm_jobman('run',job)
    
    load(spectros)
    
    simg = cellstr(spectro_list);
    wr = explore_spectro_data(char(simg));
    write_fid_to_nii(wr,output)
    
    jobs={};
    spimg = get_subdir_regex
    for ns=1:length(sdir)
        spec = get_subdir_regex(sdir(ns),'_REF$');
        ana =  get_subdir_regex(sdir(ns),'t1_weighted');
        anaf = char(get_subdir_regex_files(ana,'^sP.*img$',1));

        for kk=1:length(spec)
            [p f] = fileparts(spec{kk});     [p f] = fileparts(p);
            nuser = str2num(f(2:3));
            numepi = sprintf('^S%.2d.*LOCA$',nuser+1);

            epi = get_subdir_regex(sdir(ns),numepi);
            if length(epi)~=1 
                error('can not find epi ref to %s',spec{kk})
            end

            epif = char(get_subdir_regex_files(epi,'img$',1));
            specf= char(get_subdir_regex_files(spec(kk),'nii$',1));

            jobs = do_coregister(epif,anaf,specf,'',jobs);

        end
    end

    spm_jobman('interactive',jobs)
    