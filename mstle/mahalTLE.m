function [pval,maha]=mahalTLE(data,gmclass)
%
% [pval,minmaha]=mahalTLE(data,gmclass)
%
% % Computes mahalanobis distance to each point to the gausian mixture gmclass
%
% data: input data
% gmclass: estimated gaussian mixture
%
% outputs:
% pval: chi2 output p-value
% minmaha: mahalanobis distance which correspond to the minimal mahalanobis distance with respect all classes
%

dim=size(data,2);
maha=min(mahal(gmclass,data),[],2);

pval=cdf('chi2',maha,dim);


end