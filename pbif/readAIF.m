function [rawtime,rawaif]=readAIF(filename,unit_time,unit_act)
%% Read Arterial input function file
%
%  [rawtime,rawaif]=readAIF(filename)
% OUTPUT UNITS: seconds and Kbq
%
%
% filename: file with aif data
% unit_time : 0 automatic:: try to guess units (if max < 200 is min)
%             1 minutes:: convert them to seconds
%             2 seconds:: do nothing
% unit_act : 0 automatic:: try to guess units (if max < 100  Kbq)
%             1 Bq :: Divide by 1000
%             2 Kbq:: Do nothing
%
%
%
rawdata=importdata(filename);
rawtime=rawdata(:,1);
rawaif =rawdata(:,2);


%% Check unit time
if ~exist('unit_time','var') || unit_time==0
    unit_time=2;
    if max(rawtime) < 200
        unit_time=1;
    end
end
%% Check unit activity
if ~exist('unit_act','var') || unit_act==0
    unit_act=2;
    if max(rawaif) > 100
        unit_act=1;
    end
end

if rawtime(1) > 0
    rawtime=[0; rawtime];
    rawaif =[0; rawaif  ];
end



if unit_time==1
    rawtime=rawtime*60;
end
if unit_act==1
    rawaif=rawaif./1000;
end