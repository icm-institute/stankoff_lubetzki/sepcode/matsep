function normpbif=applyPBIF(ptime,pbif,aif_times,aif_values)
%% Apply pbif with a normalization point
%
% normpbif=applyPBIF(ptime,pbif,aif_times,aif_values)
%
% 

if numel(aif_times) ~= numel(aif_values)
    error(' -- Wrong number of aif values')
end

% compute average if more than one timepoint
mean_aif_val =mean(aif_values);

% find the points in the pbif
idx_pbif=zeros(numel(aif_values,1));
for x=1:numel(aif_times)
    ff=find(ptime >= aif_times(x));
    idx_pbif(x)=ff(1);
end

% compute the mean pbif in the given points for normalization
mean_pbif_val=mean(pbif(idx_pbif));


normpbif= (mean_aif_val/mean_pbif_val)  .* pbif;