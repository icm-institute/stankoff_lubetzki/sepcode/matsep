function gradientInverse(distance,map,mtr,les,maskIn,maskLim,sujid,odir)

    [imgLes,hdrLes]   = readNIFTI(les);
    [imgDist,hdrDist] = readNIFTI(distance);
    [imgAct,hdrAct]   = readNIFTI(map);
    [imgMtr,hdrMtr]   = readNIFTI(mtr);
    [imgMask,hdrMask] = readNIFTI(maskIn);
    [imgLim,hdrLim]   = readNIFTI(maskLim);
    
    ind       = imgMask(:) > 0 & imgLim(:) >= 5;
    data      = [floor(imgDist(ind>0)) imgAct(ind>0)  imgMtr(ind>0) imgLes(ind>0)];
    dist_uniq = unique(data(:,1));

    for j = 1:length(dist_uniq)
        les_prob(j) = mean(data(data(:,1) == dist_uniq(j),4),'omitnan');
        act_prob(j) = mean(data(data(:,1) == dist_uniq(j),2),'omitnan');
        mtr_prob(j) = mean(data(data(:,1) == dist_uniq(j),3),'omitnan');
    end
    
    fileID = [odir '/' sujid '_floorDist_actvox_mtr_les_TH_inv.txt'];
    dlmwrite(fileID,[dist_uniq act_prob' mtr_prob' les_prob'],'precision','%.8f');
end
