function [rfile,meanfile,rother]=petRealign(filepath,fr_fixed,fr_moved,other,prefix)
% Correct movement of 4D pet images using spm.coregister
%
% filepath: name of the nifti 4D
% fr_fixed: first frames that remains fixed
% fr_moved: frames that will be registered
% other   : other image that will be resliced as the moving one
% prefix  : prefix given to the output images
if nargin<5
    prefix='r';
end
[dir, name, ext]=fileparts(filepath);


% select the frames for the registration
frames=select4Dframes(filepath,fr_moved);


% check if reslice was already performed
rfile = fullfile(dir,[prefix name ext]);
meanfile = fullfile(dir,['mean' name ext]);
matfile=fullfile(dir,[name '.mat']);
rother='';
if exist(rfile,'file')
    fprintf(' -- reslice found: SKIPPING!\n');
else
    jobs={};
    %% Estimate parameters
    jobs{1}.spm.spatial.realign.estimate.data{1} = cellstr(char(frames));
    jobs{1}.spm.spatial.realign.estimate.eoptions.quality = 1;
    jobs{1}.spm.spatial.realign.estimate.eoptions.sep = 2;
    jobs{1}.spm.spatial.realign.estimate.eoptions.fwhm= 3;
    jobs{1}.spm.spatial.realign.estimate.eoptions.rtm = 1; 
    jobs{1}.spm.spatial.realign.estimate.eoptions.interp = 1; % tri estim.
    spm_jobman('run',jobs);

    %% load and modify matrices
    if ~exist(matfile,'file')
        fprintf( ' -- ERROR: no .mat found!\n');
        return
    end
    mats=load(matfile);
    for y=fr_fixed;
        mats.mat(:,:,y)=mats.mat(:,:,fr_moved(1));
    end
    save(matfile,'-struct','mats','mat')
    
    
    %% Apply reslice
    jobs={};
    jobs{1}.spm.spatial.realign.write.data = cellstr(filepath);
    jobs{1}.spm.spatial.realign.write.roptions.which = [2 1];
    jobs{1}.spm.spatial.realign.write.roptions.interp = 1;
    jobs{1}.spm.spatial.realign.write.eoptions.mask= 1;
    jobs{1}.spm.spatial.realign.write.eoptions.prefix = prefix; 
    spm_jobman('run',jobs);

end



rother='';
if nargin>=4
    [dir, name, ext]=fileparts(other);
    rother = fullfile(dir,['r' name ext]);
    
    if ~exist(rother,'file')
        fprintf( ' -- Reslicing the other');
        othermat = fullfile(dir,[name '.mat']);
        system(sprintf('cp -f %s %s',matfile,othermat));
        jobs={};
        jobs{1}.spm.spatial.realign.write.data = cellstr(other);
        jobs{1}.spm.spatial.realign.write.roptions.which = [2 1];
        jobs{1}.spm.spatial.realign.write.roptions.interp = 1;
        jobs{1}.spm.spatial.realign.write.eoptions.mask= 1;
        jobs{1}.spm.spatial.realign.write.eoptions.prefix = prefix; 
        spm_jobman('run',jobs);
    else
        fprintf(' -- rother already exists');
    end
end
