function [demy,remy,stankoff]=analysis_threshold_demy_remy(im_4d_distance_name,im_4d_mtr_name,mtr1_name,mtr2_name,dist2csf_name,dist2wm_name,thrin,odir,sujid)

[im_4d_mtr,hdr_4d_mtr]  = readNIFTI(im_4d_mtr_name,1);
[im_4d_dist,hdr_4d_dist] = readNIFTI(im_4d_distance_name,1);
thr            = str2num(thrin);

[dx,dy,dz,~]   = size(im_4d_mtr);

vett           = 0:0.2:20;
[~,di]         = size(vett);
mask_gm4d      = im_4d_dist > 0;
im_index       = mask_gm4d.*(floor(im_4d_dist/0.2));

vett_threshold = zeros(di,2);
for ii = 1:1:di
    if ii < 101
        vett_threshold(ii,1) = mean(im_4d_mtr(im_index==ii));
        vett_threshold(ii,2) = std(im_4d_mtr(im_index==ii));
    else
        vett_threshold(di,1) = mean(im_4d_mtr(im_index>=di));
        vett_threshold(di,2) = std(im_4d_mtr(im_index>=di));
    end
end

mask_nan   = isnan(vett_threshold);
[a,b]      = bwlabel(mask_nan(:,1));
val_max    = di-sum(a==max(b));
vett_threshold(mask_nan>0) = 0;

thresholds = zeros(di,1);

for ii = 1:val_max
    if ii > 1
    if vett_threshold(ii,1) == 0
        thresholds(ii,1) = abs((vett_threshold(ii+1,1)*thr)+(vett_threshold(ii-1,1)*thr))/2;
    else
        thresholds(ii,1) = vett_threshold(ii,1)*thr;
    end
    end
end

thresholds(val_max+1:end,1) = mean(thresholds(val_max-20:val_max,1));
obj_fit    = spline(vett',thresholds);
thresholds = ppval(obj_fit,vett');
thresholds(thresholds<0)=0;

[mtr1,hdr3] = readNIFTI(mtr1_name,1);
[mtr2,hdr2] = readNIFTI(mtr2_name,1);

[mtr_dist,hdr_dist] = readNIFTI(dist2csf_name,1);
mask_gm         = mtr_dist>0;
index_mtr_im    = floor(mtr_dist/0.2)+1;
index_mtr_im(index_mtr_im>di)=di;

demy_tp1 = zeros(dx,dy,dz);
demy_tp2 = zeros(dx,dy,dz);

for zz=1:dz
    for yy=1:dy
        for xx=1:dx
            if mask_gm(xx,yy,zz)>0
                threshold_vox=thresholds(index_mtr_im(xx,yy,zz),1);
                if mtr1(xx,yy,zz)<threshold_vox
                    demy_tp1(xx,yy,zz)=1;
                end
                if mtr2(xx,yy,zz)<threshold_vox
                    demy_tp2(xx,yy,zz)=1;
                end
            end
        end
    end
end

demy_im = (mask_gm-demy_tp1).*demy_tp2;
remy_im = (mask_gm-demy_tp2).*demy_tp1;

writeNIFTI(fullfile(odir,[sujid '_demyBase_' thrin 'perc.nii.gz']),demy_tp1,hdr3,1,1);
writeNIFTI(fullfile(odir,[sujid '_demyFollow_' thrin 'perc.nii.gz']),demy_tp2,hdr3,1,1);
writeNIFTI(fullfile(odir,[sujid '_demymap_' thrin 'perc.nii.gz']),demy_im,hdr3,1,1);
writeNIFTI(fullfile(odir,[sujid '_remymap_' thrin 'perc.nii.gz']),remy_im,hdr3,1,1);

demy     = sum(sum(sum(demy_im>0)));
remy     = sum(sum(sum(remy_im>0)));
stankoff = remy-demy;

mask_inner     = zeros(size(mtr1));
mask_outer     = zeros(size(mtr1));
[dist2wm,~] = readNIFTI(dist2wm_name,1);


for zz=1:1:dz
    for yy=1:1:dy
        for xx=1:1:dx
                     
            if mask_gm(xx,yy,zz)==1
                
               if (dist2wm(xx,yy,zz)<mtr_dist(xx,yy,zz))
                   mask_inner(xx,yy,zz)=1;
               else
                   mask_outer(xx,yy,zz)=1;
               end
                
            end
            
        end
    end
end


demy_inner = demy_im.*mask_inner;
remy_inner = remy_im.*mask_inner;

demy_tp1_outer = demy_tp1 .* mask_outer;

pctg_demyinner = 100*(sum(sum(sum(demy_inner>0)))/(sum(sum(sum(demy_im>0)))));
pctg_remyinner = 100*(sum(sum(sum(remy_inner>0)))/(sum(sum(sum(remy_im>0)))));
pctg_pt1_outer = 100*(sum(sum(sum(demy_tp1_outer>0)))/(sum(sum(sum(demy_tp1>0)))));

writeNIFTI(fullfile(odir,[sujid '_mask_inner_' thrin 'perc.nii.gz']),mask_inner,hdr3,1,1);
writeNIFTI(fullfile(odir,[sujid '_mask_outer_' thrin 'perc.nii.gz']),mask_outer,hdr3,1,1);

vett_val=[sum(sum(sum(mask_gm))), sum(sum(sum(demy_tp1))),sum(sum(sum(demy_tp2))),demy,remy, stankoff,pctg_demyinner,pctg_remyinner,pctg_pt1_outer];
dlmwrite(fullfile(odir,[sujid '_thrGMmtr2csf_' thrin 'perc_longitudinal.txt']),vett_val)

exit


