function index=findCluster(ctacs,name)
% From a cluster vector, find the index of the class called name
%  FINDCLUSTER(CTACS,name)
%
% return, index of the cluster or -1 if not found
%
index=-1;
for n=1:length(ctacs)
    if strcmp(name,ctacs(n).name)
        fprintf(' -- Found cluster %s! -> %d\n',name,n)
        index=n;
        break
    end
end
