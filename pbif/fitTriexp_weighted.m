function [tritime,triaif,params,resnorm]=fitTriexp_weighted(rawtime,rawaif,time,visualize,thetitle)
%% Interpolate arterial input function time using a linear + tri-exponential function
% values are weighted according to their intensity to provide a better tail fitting.
%
% Inputs
%  - rawtime  : input time in seconds
%  - rawplasma: input plasma activity in Kbs
%  - time     : final time division in seconds
%  - visualize : binary input to plot the solution (default: 0 no display)
%  - thetitle : Title of the plot if visualize=1
%
%
% Outputs
%  - tritime: final timedivision (exactly the same as time)
%  - triplasma: activity after interpolation
%  - params: optimization parameters
%  - resnorm: residual given by the fitting
% date: 09/02/2015
% author: D.Garcia-Lorenzo

if nargin<4
    visualize=0;
end

tritime=time;
triaif =zeros(size(tritime));

% find max
[amx,i]=max(rawaif);
tmx=rawtime(i);

%% time in the tri time
tmptime=find(tritime<tmx);
triind=tmptime(end);

%% 1) Linear up to the max
time1st=tritime(1:triind);
triaif(1:triind) = interp1(rawtime(1:i),rawaif(1:i),time1st);
triaif(1:triind);


%% 2) Tri exponential after the max
F=@(x,xdata)x(1).*exp(-x(2)*xdata)+x(3).*exp(-x(4)*xdata)+x(5).*exp(-x(6)*xdata);

SoS=@(x,f,xdata,tac) sum((amx./tac).^2.*(tac - f(x,xdata)).^(2));


% Set init
init=[amx/2   1.5  amx/4  0.5 amx/4    0.01 ];

% center max for the fitting
to=(rawtime(i:end)-rawtime(i))/60;

opts=optimset('Display','off');

[params,resnorm]=fminsearch(@(x)SoS(x,F,to,rawaif(i:end)),init);

% interpolate points

triaif(triind:end)=F(params,(tritime(triind:end)-rawtime(i))./60);

fprintf('f(x-%1.2f) = %1.2f x exp(- %1.2f x t ) + %1.2f x exp(- %1.2f x t ) +  %1.2f x exp(- %1.2f x t )\n', ...
               rawtime(i)/60,params(1),params(2),params(3),params(4),params(5),params(6));

if visualize
    figure
    subplot(1,2,1)
    plot(rawtime,rawaif,'ro',tritime,triaif)
    if nargin==5
        title(thetitle)
        fprintf('  -- %s params:',thetitle);
    end
    subplot(1,2,2)
    plot(rawtime(rawtime>1800),rawaif(rawtime>1800),'ro',tritime(tritime>1800),triaif(tritime>1800))
    
    fprintf('f(x) = %1.2f x exp(- %1.2f x t ) + %1.2f x exp(- %1.2f x t ) +  %1.2f x exp(- %1.2f x t )\n', ...
               params(1),params(2),params(3),params(4),params(5),params(6));
end

