function importIF(petname,projectdir,ID_cea,ID_ICM,visit)

xls_folder    = fullfile(projectdir,'backup','PET_EP',strcat('IF_',petname));
xls_file_name = dir(fullfile(xls_folder,strcat('fdg_',ID_cea,'.x*')));

xls_file = fullfile(xls_folder,xls_file_name(1).name);

try
%     [~,SHEETS] = xlsfinfo(xls_file);
%     for j = 1 : length(SHEETS)
%         switch SHEETS{j}
%             case 'Le CalcSang'
%                 [NUM,TXT,RAW] = xlsread(xls_file,j);
%         end
%     end
    [NUM,TXT,RAW] = xlsread(xls_file,'Le CalcSang');
catch
    keyboard
end

info.tCpv = NUM(9:end,2);
info.Cpv  = NUM(9:end,8);

info.Cpv(isnan(info.tCpv))  = [];
info.tCpv(isnan(info.tCpv)) = [];

rawpetdir = fullfile(projectdir,'raw',ID_ICM,visit,petname);

save(fullfile(rawpetdir,strcat(ID_ICM,'_',visit,'_',petname,'_if.mat')),'info')