function [outimg,Wf] = IntensityCorrection2(inimgfile,mask,alpha,lapth)
%
% lapth: pourcentage to threshold the laplacian image and remove edge
    if ~exist('alpha','var')
        alpha = 0;
    end
    
    if ~exist('lapth','var')
        lapth = 0.2;
    end
    
    [inimg,hdr] = readNIFTI(inimgfile);
    [l,c,p] = size(inimg);
    
    Lap = zeros(l,c,p);
    BMf = zeros(l,c,p);

    
    %% Create brain mask
    if ~exist('mask','var')
        mask = BrainMask(inimg);
    end

    %% Select area for processing
    for k = 1:p
        Lap(:,:,k) = abs(imfilter(inimg(:,:,k),fspecial('laplacian'),'replicate'));
        Lap(:,:,k) = Lap(:,:,k) < lapth*max(max(Lap(:,:,k)));
    end

    BM = mask.*Lap;
    
    for k = 1:p-1
        BMf(:,:,k) = BM(:,:,k).*BM(:,:,k+1);
    end
    %writeNIFTI('/tmp/BMf.nii',BMf,hdr);
    % BMf is a binary mask containing position for proccessing intensity
    % correction -> intersection of binary mask k and k+1
    
    
    %% Initialization
    W0 = zeros(1,p);
    opts=optimset('Display','off','MaxIter',200);
    [Wf,fval] = fminsearch(@(x)objfnc(x,inimg,BMf,alpha),W0,opts);   
    
    Wff=zeros(1,1,p);
    Wff(:)=Wf(:);
    
    outimg=bsxfun(@times,inimg,exp(Wff));
end


