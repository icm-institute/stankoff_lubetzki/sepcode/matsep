function gmclass=TrimmedLikelihoodEstimator(data,k,h,initia,maxiter,emiters)
%
% gmclass=TrimmedLikelihoodEstimator(data,k,h,initia,maxiter,emiters)
%
% Quick implementation of the Trimmed Likelihood Estimator
% Based on gmdistribution.fit to peform the EM
%
% Inspired on the implementation of STREM paper
%



if ~exist('maxiter','var')
    maxiter=200;
end
if ~exist('emiters','var')
    emiters =10;
end

N  =size(data,1);
Nr =round(h.*N);  



for m=1:round(maxiter/emiters)
    % C-step
    probas = pdf(initia,data);
    [~,IX]=sort(probas); %sort to find outliers
    % keep only 1-h % of the "best" data
    remdata=data(IX(Nr:end),:);

    % M-step
    % Perform several EM iteration without the removed data
    optsFit=statset('TolFun',1e6,'MaxIter',emiters);
    nn.mu=initia.mu;
    nn.Sigma=initia.Sigma;
    nn.PComponents=initia.PComponents;
    gmclass=gmdistribution.fit(remdata,k,'Start',nn,'Options',optsFit);
    initia=gmclass;

end

gmclass=ordergmdistribution(gmclass);

end