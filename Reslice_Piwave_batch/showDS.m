function showDS(dataSet, mask, alpha, dim, padding, gfilt, options)

% show in a unique axis a 3D dataset used in imaging application.
%
% Usage: showDS(dataSet, [mask], [alpha], [dim], [padding], [gfilt], [options])
%
% dataSet           - dataSet variable. It must be a [n x m x p] matrix: rows, 
%       columns, and samples (time points, repetitions or slices).
%
% mask (optional)   - masking volume for dataSet. Mask can be a [n x m x p]
%       or a [n x m] matrix. In the latter case, the maxtrix provided is
%       replicate p-times over the third dimension (typically used to show
%       the same slice over diffrent time points or multiple repetitions).
%       If it is absent, the default mask is represent by the entire [n x
%       m x p] matrix
%
% alpha (optional)  - a vector or a scalar number that defines the scale of
%       the colorbar. If it is a vector (of length 2), the first element
%       represents the lower bound of colorbar and the second is the upper
%       bound of the colorbar. If it is a scalar number, the colorbar lower
%       bound is the alpha-th percentile of the dataSet values distribution,
%       and the upper bound is the (100-alpha)-th percentile of the dataSet
%       values distribution. If it is absent, the default limits for
%       colorbar are the minimum and maximum values in dataSet.
%
% dim (optional)    - a two-element vector defining the grid of rows and 
%       cols in which the p-dimension of dataSet are arranged (first element 
%       is the number of rows, the second the number of cols). If the total 
%       number of partitions is higher of p, the additional subplot are set to
%       zero. If it is absent, the default subplot grid is indivduated by
%       the first two MCDs of p (the higher define cols number).
%
% padding (optional)- a two-vector vector defining the overlay on right
%       margin and lower margin respectively for subsequent images. If it
%       is abesent, no padding is set.
%
% gfilt (optional)  - a two-element vector that defines the parameters of a
%       gaussian low-pass filter (1- kernel size, 2- sigma). It is used to
%       smooth the dataSet imaged. It it is absent, no spatial smoothing is
%       applied.
% options (optional)- options structure to define some properties of the
%       figure. FontSize, colorbarLocation, title, colormap,
%       figOuterDimension...
%
% - Marco Battiston (marco.battiston.1@studenti.unipd.it)   -
% - Department of Information Engineering                   -
% - University of Padova                                    -
% - December 19 2013                                        -


%input1-dataSet
dataSet = correctNaN(squeeze(dataSet), 0);
dataSet_dim = size(dataSet);

%input2-mask
if (nargin < 2) | (isempty(mask))
    mask = logical(ones(size(dataSet)));
else
    mask = logical(squeeze(mask));
end
if (length(dataSet_dim) > length(size(mask)))
    %to plot a single slice over multiple  time points or multiple
    %repetitions
    mask = repmat(mask, [1 1 dataSet_dim(3)]);
end

if (length(dataSet_dim) == 3)
    
    [N_row, N_col, N_samples] = size(dataSet);
    %N_samples could be slices or times
    
    %input3-colorbar limits
    if (nargin < 3) | (isempty(alpha))
        alpha = 0;
    end
    values = double(dataSet(mask));
    values(isinf(values)) = max(values(values<inf));
    values(isnan(values)) = 0;
    if (length(alpha) == 1)
        %colorbar limits is defined by the alpha and 100-alpha percentiles on dataSet value distribution
        colorLims = [prctile(values, alpha), prctile(values, 100-alpha)];
        if (colorLims(2) == 0)
            colorLims(2) = max(reshape(dataSet_unique,1, N_row*sb_row*N_col*sb_col));
        end 
    else
        %colorbar limits are explicitly defined by alpha
        colorLims = [alpha(1) alpha(2)];
    end
    
    %input4-dimension of plot
    if (nargin < 4) | (isempty(dim))
        %define rows and columns of subplot optimizing screen size in
        %realtion to dataSet dimension
        fact = factor(N_samples);
        if (length(fact) == 1)
            sb_row = 1;
        else
            sb_row = prod(fact(1:end-1));
        end
        sb_col = fact(end);
        if (sb_col<sb_row)
            %number of column must be higher than number of rows 
            temp = sb_row;
            sb_row = sb_col;
            sb_col = temp;
        end
    else
        %subplots rows and columns defined by user
        sb_row = dim(1);
        sb_col = dim(2);
    end
    
    %input5-padding
    if (nargin < 5) | (isempty(padding))
        padding = [0 0];
    end
    
    %input6-image smoothing filter
    if (nargin < 6) | (isempty(gfilt))
        h_filt = fspecial('average', 1);
    else
        h_filt = fspecial('gaussian', gfilt(1), gfilt(2));
    end
    
    %PLOT
    dataSet_unique = zeros(N_row*sb_row-((sb_row-1)*padding(2)), N_col*sb_col-((sb_col-1)*padding(1)));
    k = 1;
    for r = 1: sb_row
        for c = 1: sb_col
            if (k<=N_samples)
                dataSet_unique((r-1)*(N_row-padding(2))+1: r*(N_row-padding(2)), (c-1)*(N_col-padding(1))+1: c*(N_col-padding(1))) = dataSet(1:N_row-padding(2),1:N_col-padding(1),k).*mask(1:N_row-padding(2),1:N_col-padding(1),k);
                k = k+1;
            end
        end
    end
    
    h_f = figure;
    imagesc(imfilter(dataSet_unique, h_filt), colorLims);
   
    %input7-options
    if (nargin >= 7)
        if (isempty(options))
            options.FontSize = [];
            options.title = [];
            options.colorbarLocation = [];
            options.figOuterPosition = [];
        end
        if ~(isfield(options, 'FontSize')) | (isempty(options.FontSize))
            options.FontSize = 14;
        end
        if ~(isfield(options, 'title')) | (isempty(options.title))
            options.title = 'dataSet';
        end
        if ~(isfield(options, 'colorbarLocation')) | (isempty(options.colorbarLocation))
            options.colorbarLocation = 'eastoutside';
        end
        if ~(isfield(options, 'figOuterPosition')) | (isempty(options.figOuterPosition))
            options.figOuterPosition = [120 60 1140 680];
        end
        if ~(isfield(options, 'colormap')) | (isempty(options.colormap))
            options.colormap = 'hot';
        end
        h_cb = colorbar;
        set(h_cb, 'Location', options.colorbarLocation, 'FontSize', options.FontSize)
        set(h_f, 'OuterPosition', options.figOuterPosition)
        h_t = title(options.title);
        set(h_t, 'FontSize', options.FontSize)
        colormap(options.colormap)
    else
        colorbar('location', 'eastoutside', 'FontSize', 10)
    end
    set(gca, 'DataAspectRatio', [1 1 1], 'XTick', [], 'YTick', [])
else
    disp('Error: dataSet has too much dimensions to be plot')
end

end