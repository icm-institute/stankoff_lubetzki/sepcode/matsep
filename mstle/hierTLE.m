function gmclass=hierTLE(data,k,h,maxiter,emiters)
% gmclass=hierTLE(data,k,h,maxiter,emiters,inits)
%
% Hierarchical Intitialization based on T1-w contrast
%
%


if ~exist('maxiter','var')
    maxiter=200;
end
if ~exist('emiters','var')
    emiters =10;
end
if ~exist('inits','var')
    inits=100;
end



dim=size(data,2);
N  =size(data,1);

%% 1) T1-w classification
gm1=kmeansTLE(data(:,1),k,h./dim,50,10);
gm1=ordergmdistribution(gm1);
%% 2) Classify data
class1=classifyTLE(data(:,1),gm1);

%% For each class, find in the other classes
mu0=zeros(k,dim);
sigma0=zeros(dim,dim,k);
alpha0=zeros(1,k);
mu0(:,1)=gm1.mu;

for c=1:k
    % using alpah of 1D
    alpha0(c)=sum(class1==c)./numel(class1);
    % adding first D in init
    sigma0(1,1,c)=gm1.Sigma(c);
    
    idxs=find(class1==c);

    for d=2:dim
        
        %Compute histogram
        [val,x]=ksdensity(data(idxs,d));
        %figure,plot(x,val);
        %title(sprintf(' Class %d Dimension %d',c,d))
        
        % Find peaks
        [peaks,loc]=findpeaks(val);
        loc=loc(peaks>0.005);
        peaks=peaks(peaks>0.005);
        [loc' peaks'];
        % Assign depending on the sequence
        [~,maxX]=max(val(loc));
        mu0(c,d)=x(loc(maxX));
        
        sigma0(d,d,k)=var(data(idxs,d));
    end
end        
initia = gmdistribution(mu0,sigma0,alpha0);   
initia.disp
keyboard
gmclass=TrimmedLikelihoodEstimator(data,k,h,initia,maxiter,emiters);

end