function QSMsetHeader(inimg,refimg,outimg)

% This function allows to set the qsm header provide by Mathieu Santin.
    tmp = split(char(inimg),'.');
    if tmp(3) == 'nii' || tmp(3) == 'gz'
        [img,~] = readNIFTI(inimg);
    else if tmp(3) == 'mat'
        img = load(inimg);
        end
    end
    [imgRef,hdrRef] = readNIFTI({refimg});
    newhdr = hdrRef(1);
    newhdr.fname = outimg;
    newhdr.dt = [1 0];
    
    img = permute(img,[2 3 1]);
    img = fliplr(img);
    writeNIFTI(outimg,img,newhdr,1,1);

