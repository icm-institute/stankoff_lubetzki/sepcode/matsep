function subjects=readList(filename)
% Read list of patients as in SEP projects
%
% Files are comma separated as this
% SID,VISIT,OTHERINFO
%
%
% lines with the first caracter is '#' are skipped
%
% Spaces are all removed, no spaces in SID, VISIT or OTHER INFO allowed
%

%% read list (not very clean)
fprintf(' -- Reading list: %s\n',filename);
lines=importdata(filename);
subjects={};
nsubjects=1;
for l=1:size(lines,1)
    nospace=regexprep(lines{l},' ','');
    %fprintf('nospace : %s\n',nospace);
    if isempty(nospace) || nospace(1)=='#'
        fprintf(' -- Comment in line! %s\n',nospace);
        continue
    end
    nocomments=regexp(nospace,'#','split');
    
    commas=regexp(nocomments{1},',','split');
    if numel(commas)<2
        fprintf(' -- ERROR in line! %s\n',nospace);
        return
    end

    
    %fprintf('%s/%s\n',sid,visit)
    subjects{nsubjects}=commas;
    nsubjects=nsubjects+1;
end

