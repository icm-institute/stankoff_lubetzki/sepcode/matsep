function [classes,posterior]=classifyTLE(data,gmclass)
%% [classes,posterior]=classifyTLE(data,gmclass)
%
% Classify the data according to the gausian mixture gmclass
%
% - data = matrix Nxdims, data with the correct number of dimensions
% - gmclass = gmdistribution object of the estimated classes
%
%  Outputs
% - classification (Nx1): hard classification of data
% - posterior (Nxclasses): posterior probability of the classification

posterior = gmclass.posterior(data);

[~,classes]= max(posterior,[],2);

end