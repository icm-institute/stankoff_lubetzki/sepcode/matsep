function cout=objfnc(W,image,mask,alpha)
    w=exp(W);

    dims=size(image);
    cout=0;

    %% sum of difference
    for k=2:dims(3)
        x=image(:,:,k-1);
        xplus=image(:,:,k);
        m=mask(:,:,k-1);

        %n = length(find(m>0));
        cout=cout+sum( (x(m>0).*w(k-1) - xplus(m>0).*w(k)).^2);
    end

    nval=sum(mask(:));
    nmean=sum(image(mask>0));

    regul=alpha*sum(abs(W))./numel(W);

    cout=(sqrt(cout)/(nval*nmean))+regul;
end

    
    
