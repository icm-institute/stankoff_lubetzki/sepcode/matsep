 

%% select all the files graphically
dir_sel = spm_select(inf,'image','Select probabilistic masks');
 
 
%% for each selected image
for n=1:size(dir_sel,1)
    filename=deblank(dir_sel(n,:));
    
    % open image
    v=spm_vol(filename);
    ima=spm_read_vols(v);
    
    % get name
    [path,name]=fileparts(filename);
    
    % compute integral, removing nans
    integral=sum(ima(isfinite(ima(:))));
 
    % multiply for voxel size(to check if it's general)
    volume=integral.*abs(det(v.mat(1:3,1:3)));
    % print
    fprintf('%s,\t%1.3f  voxels,\t%1.3f  mm^3\n',name,integral,volume);
end