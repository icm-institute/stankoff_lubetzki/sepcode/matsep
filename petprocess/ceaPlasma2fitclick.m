function ceaPlasma2fitclick(parent_file,plasma_file,outfile)
%ceaPlasma2fitclick   Convert txt files form CEA into parentplasma.if
%   Requires to files for corrected and uncorrected plasma
%  
%   Output file will be overwritten
%
%
%  ceaPlasma2fitclick(parent_file,plasma_file,outfile)

plasma_data=importdata(plasma_file);
parent_data=importdata(parent_file);
% data in Bq/cc and minutes

times =plasma_data.data(:,1).*60; % in seconds
plasma=plasma_data.data(:,2)./1000; % kBq/ml
parent=parent_data.data(:,2)./1000; % kBq/ml

points=length(times);

p=fopen(outfile,'w');
fprintf(p,'%d\n',points);
for x=1:points
    fprintf(p,'%1.3f\t%1.3f\t%1.3f\n',times(x),parent(x),plasma(x));
end
fclose(p);

