function res=xls2aif(xlsfile,prefix)
% Opens an already prepared xls file
% and creates artery vein input function files
% 
% Input is expected in minutes and Bq
% Afifs files are in seconds and KBq
%
%
res=0;
if nargin ~=2
    sprintf(' == Incorrect numberf of arguments')
    res=-1;
    return
end

[~,sheets]=xlsfinfo(xlsfile);
if numel(sheets)<2
    fprintf(' -- Problem with the xls file %s\n',xlsfile);
    res=-1;
    return
end
% Read artery
fprintf(' -- Reading %s \n',sheets{1});

[nmbrs,names]=xlsread(xlsfile);
names

atime=nmbrs(:,1).*60;
aif=[atime nmbrs(:,2)./1000];
caif=[atime nmbrs(:,3)./1000];

% Read vein
fprintf(' -- Reading %s \n',sheets{2});

[nmbrs,names]=xlsread(xlsfile,sheets{2});
names


vtime=nmbrs(:,1).*60;
vif=[vtime nmbrs(:,2)./1000];
cvif=[vtime nmbrs(:,3)./1000];
%sprintf('%1.3f %1.3f\n',[vtime,cvif])

aiffile=sprintf('%s_raw.aif',prefix);
caiffile=sprintf('%s_cor.aif',prefix);
viffile=sprintf('%s_raw.vif',prefix);
cviffile=sprintf('%s_cor.vif',prefix);


csvwrite(aiffile,aif);
csvwrite(caiffile,caif);
csvwrite(viffile,vif);
csvwrite(cviffile,cvif);
