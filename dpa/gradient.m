function gradient(distance,map,mtr,dvr,maskIn,maskLim,sujid,odir,roi,les)

    [imgDist,hdrDist] = readNIFTI(distance);
    [imgAct,hdrAct]   = readNIFTI(map);
    [imgMask,hdrMask] = readNIFTI(maskIn);
    [imgLim,hdrLim]   = readNIFTI(maskLim);
    [imgDvr,hdrDvr]   = readNIFTI(dvr);
    
    if isempty(les)
        imgLes = zeros(size(imgAct));
    else
        [imgLes,hdrLes]   = readNIFTI(les);
    end
    if isempty(mtr)
        	imgMtr = zeros(size(imgAct));
    else
        	[imgMtr,hdrMtr]   = readNIFTI(mtr);
    end
    
    ind       = imgMask(:) > 0 & imgLim(:) >= 3 & imgDist(:) > 0;
    data      = [floor(imgDist(ind>0)) imgAct(ind>0)  imgMtr(ind>0) imgLes(ind>0)>0 imgDvr(ind>0)];
    dist_uniq = unique(data(:,1));
    
    vec = min(dist_uniq):3:max(dist_uniq);
    i = 1;
    for j = vec
        vol(i) = size(data(data(:,1)>=j & data(:,1)<(j+3),1),1);
        nbr(i) = nnz(data(data(:,1)>=j & data(:,1)<(j+3),2));
        act_prob(i) = mean(data(data(:,1)>=j & data(:,1)<(j+3),2),'omitnan');
        mtr_prob(i) = mean(data(data(:,1)>=j & data(:,1)<(j+3),3),'omitnan');
        les_prob(i) = mean(data(data(:,1)>=j & data(:,1)<(j+3),4),'omitnan');
        dvr_prob(i) = mean(data(data(:,1)>=j & data(:,1)<(j+3),5),'omitnan');
        i = i+1;
    end  
    
    fileID = [odir '/' sujid '_3mmDist_actvox_mtr_les_' roi '.txt'];    
    dlmwrite(fileID,[vec' nbr' act_prob' mtr_prob' dvr_prob' vol' les_prob'],'precision','%.8f');
end
