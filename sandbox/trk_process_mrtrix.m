%% INITIALIZATION

% Start directory
pwd='/servernas/home/poirion/data/TEST/FLUMATEP2/data/process/';
% Subjects directories
%suj = get_subdir_regex(pwd,'^[A-Z].*');
suj = get_subdir_regex(pwd,'P0101GF');
% DWI directory
dwi=get_subdir_regex(suj,'V01','dwi');
% DWI corrected files
fdwi=get_subdir_regex_files(dwi,'dwi_corr');
% unzip dwi
undwi = unzip_volume(fdwi);
% param
par.fsl_mask = 'dwi_mask'; 
par.sge = 0;
% PROCESS
process_mrtrix(fdwi,par) 

%% REGISTRATION
% directory of freesurfer data registered and unzip
free=get_subdir_regex(suj,'V01','freesurfer');
% T1
fanat=get_subdir_regex_files(free,'T1');
% FA
ffa=get_subdir_regex_files(dwi,'FA');
% others sequences to process -> '.*a'=all sequences except T1
fother=get_subdir_regex_files(free,'.*a');
% unzip FA
unfa = unzip_volume(ffa);
% PROCESS
j = job_coregister(fanat,unfa); % job_coregister(fanat,unfa,fother) to register others sequences onto unfa
spm_jobman('run',j); % if you want the interactive method: spm_jobman('interactive',j)

%% TRACTOGRAPHY
% mrtrix_dir
mrdir = get_subdir_regex(dwi,'mrtrix');
% seed 
sd = get_subdir_regex_files(mrdir,'CSD8');
par.mask = get_subdir_regex_files(mrdir,'mask.nii');
sdir=get_subdir_regex(suj,'V01','mrtrix_Prefontal_rh');
fseed = get_subdir_regex_files(sdir,'seed');
unseed=unzip_volume(fseed);
% param
par.track_num=50000; 
par.track_maxnum = par.track_num; 
par.sge=0;
par.track_name = 'Prefontal_rh';
% PROCESS
process_mrtrix_trackto(sd,unseed,par);

%% FILTER TRACK
% track dir
ft = get_subdir_regex_files(mrdir,'Prefontal_rh.trk');
% param
clear par
par.sge=0;
par.roi_include=unzip_volume(get_subdir_regex_files(sdir,'include'));
par.roi_exclude=unzip_volume(get_subdir_regex_files(sdir,'exclude')); 
par.track_name='Prefontal_rh_track';
% PROCESS
mrtrix_filter_trackt(ft,par)


% %% ou bien directement filtre dans le streamtrack
% 
% par.track_num=150000; 
% par.track_maxnum = par.track_num; %nombre max de tirage
% 
% par.target=''; %equivaut au include
% 
% par.track_name='';
% 
% par.curvature=1;
% par.stop = 0;
% par.onedirection = 0;
% par.exclude = '';
% par.sge = 0;
% 
% process_mrtrix_trackto(sd,fseed,par)
% 
% %autre exemple
% 
% par.exclude = get_subdir_regex_files(roimni,'wmni_inter_hemispheric.nii',1);
% 
% par.track_num=100000; par.track_maxnum = 100000;
% par.onedirection =1;par.stop =1;
% 
% % Tracks PPN Hemisphere Gauche:
% fseed = get_subdir_regex_files(roiy,'^PPN_SR.nii',1)
% 
% par.target=get_subdir_regex_files(roiy,'SN_SR.nii',1);
% par.track_name='PPNg_to_SNg';
% 
% process_mrtrix_trackto(fCSD8,fseed,par)
