%% Veronese Mattia - KCL April 2013 - Perform RPM Graph Analysis with Reference at REGION LEVEL
%-------------------------------------------------------------------------

% Initialization
clc
clear all
close all
mainDir=pwd;
addpath(mainDir,'-begin');

%ADD UTILITY
phwd 	= fullfile(mainDir, 'UTILITY_forRPM');
addpath(phwd,'-begin');

% Directory cycle for subjects already analyzed
list=dir('C_*');

for s=1:length(list)
    % GO if not empty
    if isdir(list(s).name);
        %% Definition of the subject
        cd(list(s).name);
        subjID=list(s).name(1:13);
        subjmapfile=([subjID,'_map_data.mat']);
        fprintf(['SUBJECT: ',num2str(subjID)]);
        patient = false;
        control = false;
        if subjID(1)==('P')
            patient=true;
            disp(' >> PATIENT');
        end
        if subjID(1)==('C')
            control=true;
            disp(' >> CONTROL');
        end
        
         %% LETTURA file PET
        % ListDynamicFile=dir([subjID,'_i*.nii']);
        ListDynamicFile=dir([subjID,'_i10_smth.nii']);
        for it=1:length(ListDynamicFile)
            disp(['   - ',ListDynamicFile(it).name]);
            subID_study=strtrim(ListDynamicFile(it).name);
            subID_study=subID_study(1:end-4);
            dynPET_file=strtrim(ListDynamicFile(it).name);
            subjclusterfile=([subID_study,'_clust_data.mat']);
            
            %Loading data
            niiPET=load_nii(dynPET_file);
            dynPET=double(niiPET.img)/1000; %from Bq to KBq
            load(subjclusterfile);
            load(subjmapfile);
            
            %% RPM - VOXEL LEVEL
           disp('>> INFO: RPM at VOXEL level');
            Cref=cluster(end).tac;
            time_ref=time;
            
            %Pre-processing
            endOfExperiment=floor(time(end));
            tv=(0:0.5:endOfExperiment)';
            
            %Cref
            n=length(time);
            if endOfExperiment>time_ref(end)
                Cref(n+1)=Cref(n);
                time_ref(n+1)=time_ref(n);
            end
            if time_ref(1)~=0;
                Cref=[0;Cref];
                time_ref=[0;time_ref];
            end
            Cref_virtual=interp1(time_ref,Cref,tv);
            
            %Analysis
            rRPM_roi=[];
            theta3_in=0.01; %Suggested from Gunn 0.06 - 0.0341 (tracer decay time for C11) min-1
            theta3_end =0.5;  %Suggested from Gunn 0.6 - 0.0341(tracer decay time for C11) min-1 ==> the number of components was fixed to 100 automatically
            
            %Analysis - VOXEL
            mDVR=zeros(size(MASK));
            mSD_DVR=mDVR;
            mCV_DVR=mDVR;
            mRi=mDVR;
            mk2=mDVR;
            DIM=size(dynPET);
            disp('WAITING:');
            
            for z=100%:DIM(3),
                fprintf(1,'\n       ___Plane no. %d',z);
                for x=1:DIM(1),
                    for y=1:DIM(2),
                        if(MASK(x,y,z)>0)
                            [param,fit,SD_param,CV_param]=execute_RPM(tv,Cref_virtual,time,squeeze(dynPET(x,y,z,:)),cluster(3).NSD,1,false,theta3_in,theta3_end); %PUT false to avoid graphic
                            mDVR(x,y,z) = param(3);
                            mSD_DVR(x,y,z) = SD_param(3);
                            mCV_DVR(x,y,z) = CV_param(3);
                            mRi(x,y,z)=param(1); 
                            mk2(x,y,z)=param(2);
                        end
                    end
                end
            end
            fprintf(1,'\n');
            %     matlabpool close
            RESULTS.RPM.mDVR=mDVR;
            RESULTS.RPM.mSD_DVR=mSD_DVR;
            RESULTS.RPM.mCV_DVR=mCV_DVR;
            RESULTS.RPM.mRi=mRi;
            RESULTS.RPM.mk2=mk2;
            
            % Cleaning the redundant workspace
            clear tt* c modified
            disp('>> INFO: end of analysis');
            
            %Transfer to Region Analysis
            rRPM_voxel=zeros(size(rLOGAN_roi,1),size(rLOGAN_roi,2)+2);
            physio=intersect(intersect(find(mDVR>=0),find(mCV_DVR<100)),find(mDVR<10));
            
            for c=1:length(cluster)-1
                ind_roi=intersect(physio,cluster(c).index);
                mm=mean(mDVR(ind_roi));
                sd=std(mDVR(ind_roi));
                out=(1-length(ind_roi)./length(cluster(c).index))*100;
                Ri=mean(mRi(ind_roi));
                k2=mean(mk2(ind_roi));
                rRPM_voxel(c,:)=[mm,sd,out,Ri,k2];
            end
            
            %% SAVE DATA
            save(subjclusterfile,'RESULTS','rRPM_voxel','-append')
            niiDVR=subj_nii;
            niiDVR.img=RESULTS.RPM.mDVR;
            fileDVR=([subID_study,'_RPM_DVR.nii']);
            save_nii(niiDVR,fileDVR);
        end
        disp('>> INFO: end of SUBJECTS analysis');
        cd(mainDir);
    end
end
disp(' ')
disp('>>>>>> END OF ANALYSIS <<<<<')
disp(' ')
%REMOVE PATH
rmpath(mainDir);
rmpath(phwd);










