function module = ModuleGradient(image,variance)
    
    masque = (-3*variance):(3*variance);
    t_m = size(masque,2);
    Gx = zeros(t_m,t_m);
    Gy = zeros(t_m,t_m);
    for i = 1:t_m
        for j = 1:t_m
            Gx(i,j) = -(masque(i)/(2*pi*variance^4))*exp(-(masque(i)^2+masque(j)^2)/(2*variance^2));
            Gy(i,j) = -(masque(j)/(2*pi*variance^4))*exp(-(masque(i)^2+masque(j)^2)/(2*variance^2));
        end
    end

    Ix = conv2(image,Gx,'same');
    Iy = conv2(image,Gy,'same');
    
    module = sqrt(Ix.^2+Iy.^2);
end