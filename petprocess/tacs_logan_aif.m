function r=tacs_logan_aif(tacs,timeaif,aif,tstar)

if ~exist('name','var')
    name='';
    disp=0;
end

if ~exist('disp','var')
    disp=0;
end

time=tacs(1).time;

if time(end)>300
    time=time./60; % conversion to minutes
end
if timeaif(end)>300
    timeaif=timeaif./60; % conversion to minutes
end

if max(aif) > 1000
    aif=aif./1000;
end

for n=1:numel(tacs)
    tac=tacs(n).tac;
    if max(tac) > 1000
        tac=tac./1000;
    end
    [r(n).Vt,r(n).SD_Vt,r(n).CV_Vt]= execute_Logan_PLASMA_DGL(time,tac,timeaif,aif,tstar,0,'');
    r(n).name=tacs(n).name;
end
