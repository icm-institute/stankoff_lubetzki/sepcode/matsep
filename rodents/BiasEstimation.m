function [correctedimg,B] = BiasEstimation(inimg,outimg,biasfct,nbr,sigma,options,pts)
% This function allows to estimate the bias field on the inimg
% Inputs:
%   inimg: image not corrected
%   biasfct: objectif function (b = @(a) a(1)*x, where a is the estimated
%   parameter and x is the data values like x or y
%   pts: matrix 4x2 contaning (x,y) of selected area for estimation
%   nbr: number of parameters would estimate -> size(a,1)
%   sigma: weight of the gaussian filter (default: 2/3 of the inimg size)
%   options: choose options for optimization with "optimset"
%
% Outputs:
%   Bias: image of the bias
%   correctedimg: inimg corrected for bias by division

    [inimg,hdr] = readNIFTI(inimg);
    [l,c,p] = size(inimg);

    if ~exist('pts','var')
        % Find area for processing
        tmpimg = inimg(:,:,floor(p/2));
        pts = zeros(2,1);
        for y = 1:c-1
           if tmpimg(l/2,c-y) > 1e5
               pts = [c-y;...
                      c-y-100];
               break
           else
               pts = [ 235 ; 90 ];
           end
        end
    end

    
    if ~exist('options','var') || options == -1
        options = optimset('Algorithm','levenberg-marquardt','Display','off');
    end
    if ~exist('sigma','var') || sigma == -1
        sigma = l/3;
    end
    if ~exist('nbr','var') || nbr == -1
        x0 = zeros(4,1);
    else 
        x0 = zeros(nbr,1);
    end
    if ~exist('biasfct','var') || biasfct == -1
        biasfct = @(a,X) a(1)*X(:,2).^3 + a(2)*(X(:,2)-128).^2 + a(4)*(X(:,1)-128).^2 + a(3)*X(:,2)./X(:,1) + 1;
    end

    correctedimg = zeros(l,c,p);
    
    %% For each slice, compute the bias
    for z = 1:p
        [tmp,D,cv,cv_post,x_in,B] = Bias(inimg(:,:,z),x0,sigma,pts,biasfct,options);
        if abs(cv - cv_post) < 0.2
            x = [];
            for i = 1:10
                [tmp,D,cv,cv_post,x(:,i),B] = Bias(inimg(:,:,z),x0,sigma,pts,biasfct,options);   
            end
            [tmp,D,cv,cv_post,x_fin,B] = Bias(inimg(:,:,z),median(x,2),sigma,pts,biasfct,options);
        else 
            x_fin = x_in;
        end
        correctedimg(:,:,z) = tmp;
        
        disp(['Cv initial image : ' num2str(cv)]);
        disp(['Cv corrected image : ' num2str(cv_post)]);
        dx = '';
        for i =1:size(x0,1)
            dx = strcat(dx,' %d ');
        end
    end
   
    correctedimg(:,1:min(pts(:,2))-10,:) = 0;
    writeNIFTI(outimg,correctedimg,hdr);
    
function [correctedimg, D, cv, cv_post,x,Bias] = Bias(inimg,x0,sigma,pts,biasfct,options)
    [l,c] = size(inimg);
    Bias = zeros(l,c);
    
    % Extract background
    background = GaussianFilter(inimg,sigma); 

    % Extract principals edges in the slice
    laplacian = LaplacienGaussien(inimg,2);

    % Select points in data to generate artificial data
    n = 30;
    D = [];
    pts_exclus = [];
    I = [];
    if size(pts,1) <= 2
        rpl = floor(min(pts(:)) + (max(pts(:)) - min(pts(:)))*rand(n,1));
        rpc = floor(40 + (80 - 40)*rand(n,1));
    else
        rpl = pts(:,1);
        rpc = pts(:,2);
    end

    for i = 1:size(rpc,1)
        if laplacian(rpl(i),rpc(i)) >= 0
            D = [D ; rpl(i) rpc(i) background(rpl(i),rpc(i))];
            I = [I ; inimg(rpl(i),rpc(i))];
        else
            pts_exclus = [pts_exclus ; rpl(i) rpc(i)];
        end
    end
    % Coefficients of variations before correction
    cv = std(I)/mean(I);

    % Parametric equation
    objfcn = @(a)sum((D(:,3)-biasfct(a,D)).^2);

    % Estimate parameters
    [x,resnorm,res,eflag,output1] = lsqnonlin(objfcn,x0,[],[],options);
    
    % Bias Field signal
    for i = 1:l
        for j = 1:c
            tmp = [i j];
            Bias(i,j) = biasfct(x,tmp);  
        end
    end

    % Divide MRI image
    correctedimg = inimg ./ Bias;
    I_post = [];
    for i = 1:size(D,1) 
        I_post  = [ I_post ; correctedimg(D(i,1),D(i,2)) ];
    end
    cv_post = std(I_post)/mean(I_post);
