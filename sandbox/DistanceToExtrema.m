function DistanceToExtrema(pib,distance,label,lab_str,ofile)
% function to extract the distance from the edge of a lesion to the minimal and maximal values of PIB
% INPUTS:
% pib: absolute path to the PIB image
% distance: absolute path to the distance map into the selected largest
% lesions
% label: labelling image of t2 lesions mask selected
% lab: list of the differents labels extracted from labelling images of
% lesions
% ofile: file where we write values extracted
    
    [img1,hdr1] = readNIFTI(pib,'0');
    [img2,hdr2] = readNIFTI(distance,'0');
    [img3,hdr3] = readNIFTI(label,'0');
    fid = fopen(char(ofile),'a');
    
    lab = str2num(lab_str);
    
    for i = lab
        tmp2 = zeros(size(img1));
        for l = 1:size(img1,1)
            for m = 1:size(img1,2)
                for n = 1:size(img1,3)
                    if img3(l,m,n)==i
                        tmp2(l,m,n) = img1(l,m,n);% pib value into the selected lesion
                    end
                end
            end
        end
        
        max_value = max(tmp2(:));
        max_dist = img2(img1==max_value);
        max_dist = max(max_dist);
        if size(max_dist,2)>1
            fin_max_dist = max_dist(1);
        else
            fin_max_dist = max_dist;
        end
        
        tmp2(~tmp2) = Inf;
        min_value = min(tmp2(:));
        min_dist = img2(img1==min_value);
        min_dist = min(min_dist);
        if size(min_dist,2)>1
            fin_min_dist = min_dist(1);
        else
            fin_min_dist = min_dist;
        end
        
        fprintf(fid,'%d %f %f %f %f\n', i, min_value, fin_min_dist, max_value, fin_max_dist);
  
    end
    
    fclose(fid);

        
%         % Extract min and max values of PIB in the lesion
%         Max = max(tmp2(:));
%         
%         Min = min(tmp2(:));
%         % Find corresponding distance
%         max_dist = img2(img1==Max);
%         min_dist = img2(img1==Min);
%        
%         
%             
%         if (size(max_dist,1) == size(min_dist,1))
%             for o = 1:size(max_dist,1)
%                 fprintf(fid,'%d %f %f %f %f\n', i, Min, min_dist(o), Max, max_dist(o));
%             end
%         else if (size(max_dist,1) ~= size(min_dist,1))
%                 minsize = min(size(max_dist,1),size(min_dist,1));
%                 
%                 if minsize == size(max_dist,1)
%                     while size(min_dist,1)~=minsize
%                         test1 = min_dist == max(min_dist);
%                         
%                         if numel(test1(test1~=0))==1
%                             min_dist(min_dist == max(min_dist)) = [];
%                         else if numel(test1(test1~=0))>1
%                                 for p = 1:size(test1,2)
%                                     if test1(p) == 1
%                                         min_dist(p) = [];
%                                         break;
%                                     end
%                                 end
%                                 
%                             end
%                         end
%                     end
%  
%                     
%                 else if minsize == size(min_dist,1)
% 
%                         while size(max_dist,1)~=minsize
%                             test2 = max_dist == min(max_dist);
% 
%                             if numel(test2(test2~=0))==1
%                                 max_dist(max_dist == min(max_dist)) = [];
%                             else if numel(test2(test2~=0))>1
%                                     for p = 1:size(test2,2)
%                                         if test2(p) == 1
%                                             max_dist(p) = [];
%                                             break;
%                                         end
%                                     end
% 
%                                 end
%                             end
%                         end                        
%                     end
%                 end
%                 for q = 1:minsize
%                     fprintf(fid,'%d %f %f %f %f\n', i, Min, min_dist(q), Max, max_dist(q));
%                 end 
%             end
%         end
%                 
%     end
%     fclose(fid);
 
    
end