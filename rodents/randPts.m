function pts = randPts(mask,n)
%% This script allows to select randomly n points into a mask
    
    [mask,~] = readNIFTI(mask);
    ind = find(mask==1);
    [i,j,k] = ind2sub(size(mask),ind);
    
    r = round(1 + (length(i)-1)*rand(n,1));
    pts = [i(r) j(r) k(r)];
    


end