function [Rsquared,resultat_mean_clean] = ComputeStat(distancemap_name,pib_name,ofile,id)

    distancemap = readNIFTI(distancemap_name,'1');
    pib = readNIFTI(pib_name,'1');
    
    [dx,dy,dz]=size(pib);
    
    index = 0;
    resultat = zeros(numel(distancemap(distancemap>0)),1);
  
    for i = 1:dx
        for j = 1:dy
            for k = 1:dz
                if (distancemap(i,j,k)~=0)
                    index  = index + 1;
                    resultat(index,1) = distancemap(i,j,k); % distance value in voxel (i,j,k)
                    resultat(index,2) = pib(i,j,k); % pib value in voxel (i,j,k)
                end
            end
        end
    end
    
    max_distance = max(resultat(:,1));
    pas = 0.01;
    
    resultat_mean =zeros(ceil(max_distance/pas),3);
    
    index = 0;
    for l = 0:pas:max_distance
        index = index+1;
        mask1 = resultat(:,1) > l;
        mask2 = resultat(:,1) <= (l+pas);
        mask = mask1.*mask2;
        resultat_mean(index,1) = l;
        resultat_mean(index,2) = mean(resultat(mask>0,2));
        resultat_mean(index,3) = std(resultat(mask>0,2));
    end

    resultat_mean_clean = zeros(size(resultat_mean));
    index=0;
    for m = 1:ceil(max_distance/pas)
        if (sum(isnan(resultat_mean(m,:)))>0)
        else
            index = index + 1;
            resultat_mean_clean(index,:) = resultat_mean(m,:);
        end
    end
    [a,b,c,d] = corrcoef(resultat_mean_clean(:,1),resultat_mean_clean(:,2));
    
    Rsquared = a(2,1)*a(2,1);
    Pvalue = b(2,1)*b(2,1);
    
    if strfind(distancemap_name,'dilated') ~=0
        test1 = resultat_mean_clean(:,1) >= 3;
        tmp1 = resultat_mean_clean(test1,:);
        tmp1 = tmp1(tmp1(:,1)>0,:);
        coef1 = polyfit(tmp1(:,1),tmp1(:,2),1);
            
        test2 = resultat_mean_clean(:,1) < 3;
        tmp2 = resultat_mean_clean(test2,:);
        tmp2 = tmp2(tmp2(:,1)>0,:);
        coef2 = polyfit(tmp2(:,1),tmp2(:,2),1);

        fid = fopen(char(ofile),'a');
        fprintf(fid,'%s %f %f %f %f %f %f\n',char(id),Rsquared,Pvalue,coef2(1),coef2(2),coef1(1),coef1(2));
        fclose(fid);
    else
        tmp3 = resultat_mean_clean;
        tmp3 = tmp3(tmp3(:,1)>0,:);
        coef3 = polyfit(tmp3(:,1),tmp3(:,2),1);

        fid = fopen(char(ofile),'a');
        fprintf(fid,'%s %f %f %f %f\n',char(id),Rsquared,Pvalue,coef3(1),coef3(2));
        fclose(fid);        
        
    end
end

