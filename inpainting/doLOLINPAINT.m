
function doLOLINPAINT(ima,les,classif,outimage,minsize,inpmethod,wmmean,wmstd)
% doRNDINPAINT  inpaint using random wm voxels
%
% usage: doRNDINPAINT (ima,les,classif,outimage)
try
if strcmp(inpmethod,'mixed') && nargin<7
    disp(' -- Mixed method requires wmmean and wmstd arguments')
    disp('         CHANGING METHOD TO KERNEL')
    inpmethod='kernel'
end
    

% t1 image
if exist(ima) == 0
    disp(sprintf(' -- ERROR image does not exist: %s',ima))
%     exit
end
[t1,Vima] = readNIFTI(ima);
%Vima=spm_vol(ima);
%[t1 xyz1] = spm_read_vols(Vima);

% lesion binary image
if exist(les) == 0
    disp(sprintf(' -- ERROR lesion does not exist: %s',les))
%     exit
end
[lesions,Vles] = readNIFTI(les);
%Vles=spm_vol(les);
%[lesions xyz2] = spm_read_vols(Vles);

% classification image
if exist(classif) == 0
    disp(sprintf(' -- ERROR classif does not exist: %s',classif))
%     exit
end
[wm,Vclas] = readNIFTI(classif);
%Vclas=spm_vol(classif);
%[wm xyz3] = spm_read_vols(Vclas);

inp=[];

if max(wm(:))  > 2.5
  disp(' - Classif file value higher than 2.5 .... considering hard classification');
  wm(wm<2.5 | wm>=3.5)=0;
  wm(wm>=2.5 & wm<3.5)=1;
else
  disp(' - Considering as fuzzy wm.. thresholding at 0.9');
  wm(wm>=0.9)=1;
  wm(wm<0.9)=0;
end

wmall=sum(wm(:));
disp(sprintf(' -- Total white matter %d',wmall))
disp(sprintf(' -- Min candidate size %d',minsize))

if strcmp(inpmethod,'random')
  disp(' -- Random inpainting');
  inp = LOLINPAINT(t1,lesions,wm,minsize,0);
elseif strcmp(inpmethod,'gaussian')
  disp(' -- Gaussian inpainting');
  inp = LOLINPAINT(t1,lesions,wm,minsize,1);
elseif strcmp(inpmethod,'kernel')
  disp(' -- Kernel inpainting');
  inp = LOLINPAINT(t1,lesions,wm,minsize,2);
elseif strcmp(inpmethod,'mixed')
  disp(' -- Mixed inpainting');
  inp = LOLINPAINT(t1,lesions,wm,minsize,3,wmmean,wmstd);
end


%% write inpainting
writeNIFTI(outimage,inp,Vima);
%Vinp=Vima;
%Vinp.fname=outimage;
%spm_write_vol(Vinp,inp);

catch err
    disp (' -- Error in the inpainting process');
    disp (err.identifier)
    disp (err.message)
%     exit
end

% exit
