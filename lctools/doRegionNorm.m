function doRegionNorm(imafile,regfile,regnorm)

%% open images
%%%%%%%%%%%%%%
[ima,Vima]=readNIFTI(imafile);
reg=readNIFTI(regfile);

%% Do processing
%%%%%%%%%%%%%%%%
norm=regionNormalization(reg,ima);

%% Write images
%%%%%%%%%%%%%%%%
writeNIFTI(regnorm,norm,Vima);
