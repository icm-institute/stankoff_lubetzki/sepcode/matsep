function [denoised,nweights]=temporal_nlm_cout_perm(permPET, mask, beta,searchsize,tzero,cout,mpet)

if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('cout','var')
    cout=1;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end
if ~exist('mpet','var')
    %% Create average image
    fsize=5;
    mpet = convn(permute(permPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
    mpet = single(permute(mpet,[4 1 2 3]));
    
end

%% 
dims   =size(permPET);
nframes=dims(1)-tzero;

% output image
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 
nweights=zeros(dims(1:3));

%% Start processing voxels
for k=1:dims(4) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(3)
for i=1:dims(2)
    
    if mask(i,j,k)==0
        continue
    end
    tac=permPET(:,i,j,k);
    mtac= mpet(:,i,j,k);
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(2),i+searchsize);
    Jmax=min(dims(3),j+searchsize);
    Kmax=min(dims(4),k+searchsize);
    %% First: estimate min distance
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        
        %% skipping center of the square
        if (i-x) <= cout && (x-i) <=cout && ...
           (j-y) <= cout && (y-j) <=cout && ...
           (k-z) <= cout && (z-k) <= cout
            continue
        end
        
        d=(tac - permPET(:,x,y,z) - mtac + mpet(:,x,y,z)).^2;
        
        mindist=min([mindist,sum(d(tzero:end))]);

    end
    end
    end


    %% Apply weights
    wmax=0;
    dtac=zeros(dims(1),1);
    wmean=0;

    betamin=beta*mindist;
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        %% skipping center of the square
        if (i-x) <= cout && (x-i) <=cout && ...
           (j-y) <= cout && (y-j) <=cout && ...
           (k-z) <= cout && (z-k) <= cout
            continue
        end

        d=(tac - permPET(:,x,y,z)).^2;

        w=exp(-sum(d(tzero:end))./betamin);
        if w > wmax
            wmax=w;
        end
        

        dtac =dtac+ w .*  permPET(:,x,y,z);
        wmean=wmean + w;
        
    end
    end
    end
    
    nweights(i,j,k)=wmean;
    
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) ./(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end

