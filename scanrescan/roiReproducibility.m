
function out=roiReproducibility(ima1,ima2,aseg1,aseg2,lut,regsizes,iters)
% %% DEPRECATED
% ima1=;
% ima2=;
%
% aseg1=;
% aseg2=;
%
% lutfile=;
% regsizes=;
% iter =;

% create output structure
out.regsizes=regsizes;
out.iterations=iters;
out.labels =lut.textdata;
out.mean   =zeros(numel(out.labels),numel(regsizes));
out.absmean=zeros(numel(out.labels),numel(regsizes));
out.quant95=zeros(numel(out.labels),numel(regsizes));
out.absqu95=zeros(numel(out.labels),numel(regsizes));


% compute for each label
for y=1:size(lut.data,1)
    
    
    val  =lut.data(y);
    fprintf(' -- Doing Region %s => %d\n',char(lut.textdata(y)),val);
    themask1=(aseg1==val & ima1>0);
    themask2=(aseg2==val & ima2>0);

    if sum(themask1(:))==0 || sum(themask2(:))==0
        fprintf(' xx Region not found with value %d\n',val);
        continue
    end
    % compute statistis for each region size
    for r=1:numel(regsizes)
        % get the random averages
        p1=getAveragePoints(ima1,themask1,regsizes(r),iters,themask1);
        p2=getAveragePoints(ima2,themask2,regsizes(r),iters,themask2);
        
       
        % compute difference
        % %relative_difference=difference/average
        diff=200.*(p1-p2)./(p1+p2);

        % option b: more stable?
        % diff=200.*(p1-p2)./(mean(p1(:))+mean(p2(:)));
        
        
        %% compute the final statistics
        out.mean(y,r)   = mean(diff);
        out.absmean(y,r)= mean(abs(diff));
        out.quant95(y,r)= quantile(    diff ,0.95);
        out.absqu95(y,r)= quantile(abs(diff),0.95);
        fprintf('    %d.  mean= %f2.2 absmean= %f2.2 q95= %f2.2  aq95= %f2.2\n', ...
                        regsizes(r),out.mean(y,r),out.absmean(y,r),out.quant95(y,r),out.absqu95(y,r))
       
       
        
    end
end



