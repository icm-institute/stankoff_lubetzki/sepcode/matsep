function exception=SEPextractTACS(petname,listname,projectdir,useraw)
exception='';

if nargin<2 || isempty(listname)
    listname = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

if nargin < 4
    useraw=0;
end
%% REGION INFO
ids=    [  1,   2,     10,    11,    12,    13,   17,     18,    26, ...
                       49,    50,    51,    52,    53,    54,    58];
labels={'wm','gm','ThalL','CauL','PutL','StrL','HipL','AgmL','HCaL', ...
                  'ThalR','CauR','PutR','StrR','HipR','AgmR','HCaR'};  
          



%% Read the list
subjects=readList(listname);


for n=1:size(subjects,2)

    %% setting output
    outpetfolder=[petname 'TACs'];    
    outdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},outpetfolder,'');
    clusterfile=sprintf('%s/%s_%s_%s_tacs.mat',outdir,subjects{n}{1},subjects{n}{2},petname);
    
   
    if exist(clusterfile,'file')
        fprintf(' -- Cluster file exists!\n ')
        continue
    end
   
    
    
    inpetfolder=[petname 'preproc'];

    
    % Find data acquisition sif information
    rawpetdir=get_subdir_regex(projectdir,'raw',subjects{n}{1},subjects{n}{2},['^' petname]);
    if isempty(rawpetdir)
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end

    siffile =get_subdir_regex_files(rawpetdir,'sif$');
    if isempty(siffile)
        fprintf('-- Subject %s/%s missing %s sif file\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    [time,~,~,delta]=readSIF(siffile);
    
    
    % Find petfolder in the raw folder
    if useraw
        % Find pet image, corrected for movement
        pet=get_subdir_regex_files(rawpetdir,[ petname '.nii']);
        if isempty(pet)
            fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
            continue
        end
        pet=char(pet);
    
    else
        petdir=get_subdir_regex(projectdir,'process',subjects{n}{1},subjects{n}{2},['^' inpetfolder]);
        if isempty(petdir)
            fprintf('-- Subject %s/%s missing %spreproc folder\n',subjects{n}{1},subjects{n}{2},petname)
            continue
        end
        
        % Find pet image, corrected for movement
        pet=get_subdir_regex_files(petdir,'movcorr.nii');
        if isempty(pet)
            fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
            continue
        end
        pet=char(pet);

    end
     
    %%% IN PROGRESS
    % Find mask 
    roidir = get_subdir_regex(projectdir,'process',subjects{n}{1},subjects{n}{2},['^' petname 'ROI'])
    roifile= get_subdir_regex_files(roidir,'ROIS');
    if numel(roifile) ==0
        fprintf(' -- ERROR ROIS not found! %s/%s\n\n',subjects{n}{1},subjects{n}{2});
        continue
    end
    
    % get output dir
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    
    % read 4D pet
    fprintf(' -- Reading dynPET\n');
    dynPET=readNIFTI(pet);
    
     % open rois
    fprintf(' -- Reading ROIs\n');
    [ROIS,hdr]=readNIFTI(roifile);
    
    
    tacs=multitacExtraction(dynPET,ROIS,labels,time,delta,ids);
    save(clusterfile,'tacs')
   
end