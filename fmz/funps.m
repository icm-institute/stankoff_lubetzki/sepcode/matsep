%	FUNps
%	fit du Scatchard dans la methode a saturation partielle

function dist = funps(xxx,mat)

    [n,~] = size(mat);
    w = [ 1 2 2 2 1.5 ones(1,n-5) ];
    dist = 0;
    for i = 1:n
        dist = dist + (((mat(i,2)-xxx(1)*mat(i,1)-xxx(2))*5)^2 + 0.5*(mat(i,1)-mat(i,2)/xxx(1)+xxx(2)/xxx(1))^2)*w(i);
    end;
    
