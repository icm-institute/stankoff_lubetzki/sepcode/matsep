function mtr_correct_mask(in,out)

	[img,hdr] = readNIFTI(in,1);

	for j = 1 : size(img,2)
    
		tmp   = squeeze(img(:,j,:));
		[r,c] = find(tmp);
		tmp(:,max(c)-5 : max(c)) = 0;
		img(:,j,:) = tmp;
	end
        
        writeNIFTI(out,img,hdr);

end
