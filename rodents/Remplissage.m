function outimg = Remplissage(inimg)

    [B,L,N,A] = bwboundaries(inimg, 8, 'holes');
    [r,i] = find(A(:,N+1:end));
    [rr,ii] = find(A(:,r));
    idx = setdiff(1:numel(B), [r(:);rr(:)]);
    outimg = ismember(L,idx); 
    
end