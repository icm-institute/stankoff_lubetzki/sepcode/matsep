function [I,J]=triexponential(x,xdata)

I=x(1).*exp(-x(2)*xdata)+x(3).*exp(-x(4)*xdata)+x(5).*exp(-x(6)*xdata);

if nargout>1
    d1=exp(-x(2)*xdata);
    d2=x(1).*(-xdata).*exp(-x(2)*xdata);
    d3=exp(-x(3)*xdata);
    d4=x(3).*(-xdata).*exp(-x(4)*xdata);
    d5=exp(-x(5)*xdata);
    d6=x(5).*(-xdata).*exp(-x(6)*xdata);
    
    J=[d1 d2 d3 d4 d5 d6];
    
end