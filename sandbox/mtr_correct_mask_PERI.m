function mtr_correct_mask_PERI(in,out)

	[img,hdr] = readNIFTI(in,1);

	for j = 1 : size(img,2)
    
		tmp   = squeeze(img(:,j,:));
		[r,c] = find(tmp);
		tmp(:,max(c)-5 : max(c)) = 0;
		img(:,j,:) = tmp;
	end
        img(:,:,1:26) = 0;
        writeNIFTI(out,img,hdr);

end
