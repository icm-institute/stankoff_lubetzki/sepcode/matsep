function denoised=tnlm_basic_subsample(permPET, mask, beta,searchsize,tzero,subsample,mpet)

if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('subsample','var')
    subsample=2;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end

searchsize=round(searchsize/subsample);

if ~exist('mpet','var')
    %% Create average image
    fsize=3;
    mpet = convn(permute(permPET,[2 3 4 1]),ones(fsize,fsize),'same') ./ (fsize^2);
    mpet = single(permute(mpet,[4 1 2 3]));
end

ss=subsample;

%% 
dims   =size(permPET);
denoised=zeros(dims(2),dims(3),dims(4),dims(1)); 

for sx=1:ss
for sy=1:ss
for sz=1:ss
    fprintf('Subblock: %d,%d,%d\n',sx,sy,sz);
    denoised(sx:ss:dims(2),sy:ss:dims(3),sz:ss:dims(4),:)= ...
        temporal_nlm_perm(permPET(:,sx:ss:dims(2),sy:ss:dims(3),sz:ss:dims(4)), ...
        mask(sx:ss:dims(2),sy:ss:dims(3),sz:ss:dims(4)),beta,searchsize,tzero, ...
        mpet(:,sx:ss:dims(2),sy:ss:dims(3),sz:ss:dims(4)));
end
end
end


