clc
clear
colormap(gray)

%% read T1 volume
%name = '/nasDicom/spm_raw/STABILITE_QC_TRIO/2011_10_31_QC_Morning_Ant12/S05_T1fl3D_1mm_iso_ND/sQC_Morning_Ant12-0005-00001-000192-01.img';

%V= spm_vol(name);
%[ima xyz] = spm_read_vols(V);
%ima=double(ima);

%% read many t1 volumes
% read acquisitions
path='/nasDicom/spm_raw/STABILITE_QC_TRIO/';
regx='.*';
pp=get_subdir_regex(path,regx);
% read T1fl3D sequences only
regxT1='.*T1fl3D*';
regxEP='.*EP2D*';

regx2=regxEP;

seqs=get_subdir_regex(pp,regx2);

% all images in the previous files
par.verbose=0;
select_img = '.*.img'; %if you have nifti img images
ff = get_subdir_regex_files(seqs,select_img,par);

% 
text_file='noise_test_T1fl3D.csv';
if regx2==regxEP
    text_file='noise_test_EP2D.csv';
end

% if more than one image

fid = fopen(text_file,'a+');
estimated=[];
maxvals  =[];
nbff=size(ff);
nbff=nbff(2)

for f=1:nbff
    f
    
    lines=size(ff{f},1)
    for i=1:lines
        i
        
        
        
        V=spm_vol(ff{f}(i,:));
        
        
        [ima xyz] = spm_read_vols(V);
        ima=double(ima);

        mval=max(ima(:));
        estit=RicianSTD(ima);
        percent=100*estit/mval;
        fprintf(fid,'%s,%f,%f,%f\n',ff{f}(i,:),estit,mval,percent);
        
        estimated=[estimated(:); estit];
        maxval=[maxval(:); mval];
    
%    else
%         
%         maxval(f)=max(ima(:));
%         estimated(f)=RicianSTD(ima);
%         percent=100*estimated(f)/maxval(f);
%         fprintf(fid,'%s,%f,%f,%f\n',ff{f},estimated(f),maxval(f),percent);
%     end
    end
end

fclose(fid);

