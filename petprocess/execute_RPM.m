%% Revision Code
%  From: Click fit, version PET course (Feb 2013)
%  Reviewed by: MV (April 2013)
%----------------------------

function  [param,fit,SD_param,CV_param]= execute_RPM(time_ref,Cref,time_PET,Ctarget,NSD,flag,displ,theta3_in,theta3_end)

%% Preparation of the Reference Tissue and Virtual grid definition
if flag~=1; %REFERENCE REGION NOT OPTMIZED
    %Check minutes
    if time_PET(end)>200
        time_PET=time_PET./60; %in minutes
        time_ref=time_ref./60; %in minutes
    end
    
    %Virtual grid
    endOfExperiment=floor(time_PET);
    tv=0:0.5:endOfExperiment;
    
    %Cref
    n=length(time_ref);
    if endOfExperiment>time_ref(end)
        Cref(n+1)=Cref(n);
        time_ref(n+1)=time_ref(n);
    end
    if time_ref(1)~=0;
        Cref=[0;Cref];
        time_ref=[0;time_ref];
    end
    Cref_virtual=interp1(time_ref,Cref,tv);
else  %REFERENCE REGION OPTIMIZED
    tv=time_ref;
    Cref_virtual=Cref;
end

%setting weights
weights = setweights(NSD,Ctarget);

%% CONVOLUTION AND GRID DEFINITION
%RANGE OF THETA3
theta3=logspace(log10(theta3_in),log10(theta3_end),100);
B1v = Cref_virtual;
B2v = RPM_Convolution_B2(Cref_virtual,tv,theta3);

% Interpolation to PET TIME
B1 = interp1(tv,B1v,time_PET);
B2 = interp1(tv,B2v,time_PET);


%% LINEAR ESTIMATION
WRSS=[]; RSS=[];
ALFA=[]; SD_ALFA=[]; CV_ALFA=[];
for i=1:length(theta3);
    [WRSS(i),RSS(i),ALFA(:,i),SD_ALFA(:,i),CV_ALFA(:,i)]=RPM_Solve_LinearModel(B1,B2(:,i),Ctarget,weights);
end

%% SOLUTION RESEARCH

% Find best fit for the best set of parameters: param return Ri, k2 and DVR
[indSolution,param,SD_param,CV_param] = RPM_FindSolution(WRSS,RSS,ALFA,SD_ALFA,CV_ALFA,theta3);
% Fit for the best solution
fit = [B1,B2(:,indSolution)]*ALFA(:,indSolution);


if  displ
        fit_virtual = [B1v,B2v(:,indSolution)]*ALFA(:,indSolution);
        figure
        plot(time_PET,Ctarget,'+r','linewidth',4)
        hold on
        plot(tv,fit_virtual,'b')
        xlabel('TIME(minutes)')
        ylabel('Concentration')
        title(['DVR: ',num2str(param(3),3)]);
        pause
end


