function out=randomcorois(mask1,mask2,roisize,iters,codist_contrast)

out.roisize=roisize;
out.iterations=iters;
out.codist_contrast=codist_contrast;
out.rois1 = zeros(iters,roisize);
out.rois2 = zeros(iters,roisize);

if nargin < 5 
    codist_contrast = 0;
end

for i=1:iters
    
    %% Get random rois
    roi1=[];
    roi2=[];
    
    errors=1;
    while 1
    
        %% get random point in mask1
        idx1=getRandomIndexes(mask1,1);
        
        %% Compute roi in mask 1
        roi1=randomROI(idx1,mask1,roisize);
        if numel(roi1)==0
            errors=errors+1;
            continue;
        end

        % check if we do the contrast
        if codist_contrast>0
            % get points around the sphere on point1
            neighIdxs=getSphereIdxs(idx1,codist_contrast,mask2);

            % select only the idxs in the mask
            maskIdx=neighIdxs(mask2(neighIdxs)>0.5);

            % random point from the mask
            idx2=maskIdx(random('unid',numel(maskIdx),1));
            
            roi2=randomROI(idx2,mask2,roisize);
            
            if numel(roi2)==0
                errors=errors+1;
                continue
            end
        end
        % Everything when well
        break
    end
    
    
    %% Once we have the two regions 
    out.rois1(i,:)=roi1(:);
    out.rois2(i,:)=roi2(:);
    
end

