function segmentation=sizeFilter(mask,minsize)
%
% segmentation=sizeFilter(mask,minsize)
%
% Basic function to remove small regions from a binary mask
%


segmentation=bwareaopen(mask,minsize,6);

end