function [reffile,reftacfile]=do_superdpa_perm(petfile,siffile,maskfile,profiletxt,outputprefix)

[time,~,~,delta]=read_SIF(siffile);
    
    
% output images
reffile  =[outputprefix '_refregion.nii'];
grayfile =[outputprefix '_grayratio.nii'];
bindfile =[outputprefix '_bindratio.nii'];
whitefile=[outputprefix '_whiteratio.nii'];
bloodfile=[outputprefix '_bloodratio.nii'];
reftacfile=[outputprefix '_refregion_tac.mat'];
% read 4D pet
fprintf(' -- Reading dynPET\n');
%dynPET =spm_read_vols(spm_vol(petfile));
dynPET = readNIFTI(petfile,1); %% Correction for compressed nifti
permPET=permute(dynPET,[4 1 2 3]);
clear dynPET;

% open rois
fprintf(' -- Reading ROIs\n');
%hdr=spm_vol(maskfile);
%brainmask= spm_read_vols(hdr);
[brainmask,hdr]= readNIFTI(maskfile,1);


% do super
[ref,gray,bind,white,blood]=superdpa_perm(permPET,brainmask,time,profiletxt);

%
if nargout == 2
    %% Computing cluster tac
    tacs(1).time=time;
    [tacs(1).tac,tacs(1).TACs]=tacExtraction_perm(permPET,ref);
    tacs(1).NSD=sqrt(tacs(1).tac./delta);
    tacs(1).name='REFREGION';
    tacs(1).index=find(ref>0);
    
    save(reftacfile,'tacs');
end
    
      
%% write output images
write_NIFTI(reffile  ,ref  ,hdr,0);
write_NIFTI(grayfile ,gray ,hdr,1);
write_NIFTI(bindfile ,bind ,hdr,1);
write_NIFTI(whitefile ,white ,hdr,1);
write_NIFTI(bloodfile,blood,hdr,1);

