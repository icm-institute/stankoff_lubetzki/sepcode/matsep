function [ new_weights ] = setweights( NSD,TAC )
%setweights
% This function converts NSD (noise standard deviation) in 1/NSD depending
% on TAC value. It is normalized to the maximum value.
% > INPUT:
% NSD: (1-D vector)
% TAC: data (1-D vector)
% > OUTPUT:
% new_weights: 1/NSD. w=0 if TAC<=0
%---------------------------------------
%% MAIN
warning off
w=1./NSD;
warning on
w2 = w; % secondo vettore per la ricerca del massimo
vv=~isinf(w);
[primo_max, indmax]=max(w(vv));
w2(indmax)=0;
[secondo_max]=max(w2(vv));

if primo_max/secondo_max>10    % check if there are weights "enormous"
    primo_max=secondo_max;
end

for i=1:length(w)
    if w(i)<=0 || isnan(w(i))
        w(i)=0;
    end
    if isinf(w(i))
        w(i)=primo_max;
    end
    if w(i)>primo_max
        w(i)=primo_max;
    end
end
w=w./primo_max;

%Correct for negative value
new_weights=w;
i=union(find(TAC<=0),find(TAC>20));
if ~isempty(i)
    new_weights(i)=0;
end
end

