%% read many t1 volumes
% read acquisitions
path='/nasDicom/spm_raw/STABILITE_QC_TRIO/';
regx='.*';
pp=get_subdir_regex(path,regx);
% read T1fl3D sequences only
regxDTI='.*DTI*';
regx2=regxDTI;

seqs=get_subdir_regex(pp,regx2);

% all images in the previous files
par.verbose=0;
select_img = '.*.img'; %if you have nifti img images
ff = get_subdir_regex_files(seqs,select_img,par);

text_file='noise_test_DTI.csv';


fid = fopen(text_file,'a+');
nbff=size(ff);
nbff=nbff(2)


% pour chaque acquisition
for f=1:nbff
    f
    
    % we take firs image as example to take the number of slices
    testvol=spm_vol(ff{f}(1,:));
    slices=testvol.dim(3);
    
    patient
    
    for s=5:slices
        estimated=[];
        %maxval=[];

        % pour chaque orientation
        lines=size(ff{f},1)
        for i=2:lines
        
           % open image
           V=spm_vol(ff{f}(i,:));

           % open slice
           Mi      = spm_matrix([0 0 s]);
           X       = spm_slice_vol(V,Mi,V.dim(1:2),0);
        
           % stimate noise    
           ima=double(X);

           %mval=max(ima(:));
           estit=RicianSTD2D(ima);
           %percent=100*estit/mval;
           %fprintf(fid,'%s,%f,%f,%f\n',ff{f}(i,:),estit,mval,percent);
        
           estimated=[estimated(:); estit];
           %maxval=[maxval(:); mval];
        end
        estimated
    end
  
end

fclose(fid);
