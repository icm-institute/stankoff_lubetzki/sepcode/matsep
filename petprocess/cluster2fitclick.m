
function cluster2fitclick(clusters,filename)
% writeSTATS
%
% Write a vector of cluster structs into a file.stats
%
% THe output file is compatible with fitclick
p=fopen(filename,'w');
fprintf(p,'# created by D. Garcia code\n');
fprintf(p,'# %s \n',datestr(now))
fprintf(p,'#\n');
fprintf(p,'#Vol_# Slice             Name      MaxVal      MinVal          Mean      Std.Dev.     Voxels      Area_mm2       Vol_mm3\n');
fprintf(p,'#\n');



for n=1:length(clusters)
    thecluster=clusters(n);
    
    name=thecluster.name;    
    fprintf(' -- %s \n',name);
    Voxels=size(thecluster.TACs,2);
    
    thecluster.TACSs=thecluster.TACs./1000;
    thecluster.tac=thecluster.tac./1000;
    
    if Voxels > 0
        
        MaxVal=max(thecluster.TACs,[],2); % convert to KBq
        MinVal=min(thecluster.TACs,[],2); % convert to KBq
        Std   =std(thecluster.TACs,0 ,2);
    
    else % faking some values
        fprintf(' -- WARNING: missing TACs in the cluster %s... faking values\n',name);
        MaxVal=1.3 .*thecluster.tac;
        MinVal=0.7 .*thecluster.tac;
        Std   =0.1 .*thecluster.tac;
        Voxels=10000;
    end
        
    Areamm2= 1.21875.^2 .* Voxels;
    Volmm3 = 1.21875.^3 .* Voxels;
       
    
    for s=1:length(thecluster.tac)
        vol=s;
        slice=95; % I don't know
  
        Mean=thecluster.tac(s);
    
        fprintf(p,'   %d    %d   %s   %1.4f    %1.4f    %1.4f    %1.4f    %d   %1.3f   %1.3f \n', ...
                    vol,slice,name,MaxVal(s),MinVal(s),Mean,Std(s),Voxels,Areamm2,Volmm3);
    end
end
fclose(p);