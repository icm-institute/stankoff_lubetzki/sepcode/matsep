%% Initialization

clear all;
% Start directory
pwd='/servernas/home/poirion/data/TEST/FLUMATEP2/data/tracula/';
% Path directories
statdir = get_subdir_regex(pwd,'stats');
track_lh_atr = char(get_subdir_regex_files(statdir,'lh.atr.*bbr.FA.txt'));
% Dataset
M=importdata(track_lh_atr);
ds=dataset(M.textdata', M.data');
ds.Properties.VarNames{1}='Subject';
ds.Properties.VarNames{2}='FA';
ds.Group=0;
ds.Group(1:9)= 1;
summary(ds);

%% Nonlinear mixed-effects estimation

%X=M.data';
%y=M.textdata';
%group=[zeros(1,9) ones(1,7)]';


%% Linear mixed-effects model






