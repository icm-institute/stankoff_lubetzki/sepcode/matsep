function inp = LOLINPAINT(t1, lesions, wm, minsize,method,wmmean,wmstd)
% Inpaint using random voxels from the surrouding WM
% method == 0 : just random selection from the voxels
% method == 1 : gaussian estimation using the voxels
% method == 2 : non parametric estimation using the voxles
if method==3
    % If method 3, create once the distribution of the white matter
    if nargin < 7
        disp(' Error: to use method 3 we need wmmean and wmstddev')
%         exit
    else
        %globaldist=ProbDistUnivParam('normal',[wmmean wmstd]);
	globaldist = makedist('Normal','mu',wmmean,'sigma',wmstd);
    end
end


inp=t1;
wm(lesions > 0.5)=0;

remaining=sum(lesions(:)>0.5);

% iterate till no more lesionpoints
while remaining
    disp(sprintf('- remaining: %d',remaining))
    % use all image
    for z=1:size(lesions,3)
    for y=1:size(lesions,2)
    for x=1:size(lesions,1)
        
        % check if voxel is a candidate point
        % TODO TO CHANGE FOR MORE THAN ONE LABEL
        if lesions(x,y,z) < 0.5        
            % voxel not a lesion
            continue
        elseif lesions(x,y,z+1) > 0.5 && lesions(x,y,z-1) > 0.5 ...
          && lesions(x,y+1,z) > 0.5 && lesions(x,y-1,z) > 0.5 ...
          && lesions(x+1,y,z) > 0.5 && lesions(x-1,y,z) > 0.5 
          % lesion is not in the border
          continue
        end
        
        % select the adapted box to have enough points
        for s=1:50
            % TODO TO CHANGE IF SPHERICAL
            xx=x + (-s:s);
            yy=y + (-s:s);
            zz=z + (-s:s);
            
            boxwm =wm     (xx,yy,zz); % it works??
            boxles=lesions(xx,yy,zz); % it works??
            boxt1 =t1     (xx,yy,zz); % it works??
            
            candidates= boxt1 (boxles < 0.5 & boxwm > 0.5);
            
            nbcand=length(candidates);
            if minsize > nbcand
                % found box size
                if s==50
                    disp(' -- Found no point for this lesion!')
                    lesions(x,y,z)= 998;
                    remaining = remaining -1;
                    break
                end
                continue
            end
            
            % estimate gaussian and create a random gaussian value
            if method == 0
                inp(x,y,z)=candidates(randi(nbcand,1));
            elseif method == 1
                [mean, std]=normfit(candidates);
                inp(x,y,z)=mean+std*randn(1);
            elseif method == 2
                ksd=fitdist(candidates,'kernel');
                inp(x,y,z)=random(ksd);
            elseif method == 3
                % compute also a kernel fitting
                ksd=fitdist(candidates,'kernel');
                % Create a discrete multiplication of the two distributions
                vals=min(candidates(:)):ksd.BandWidth()/2:max(candidates(:));
                newpdf=pdf(ksd,vals).*pdf(globaldist,vals);
                newpdf = round(newpdf*1000/min(newpdf));
                % Construct the new distribution
                newdistribution=fitdist(vals','kernel','frequency',newpdf');
                
                inp(x,y,z)=random(newdistribution);
                
                
            end
            
            
            lesions(x,y,z)= 999; % arbitrary number to remove this values for next iteration
            remaining = remaining -1;
            break % exit of boxes
        end
         
        
        

    end
    end
    end
    
    % once check all the iteration, we remove all processed voxels of the lesion mask
    lesions(lesions==999)=0;
    
end 



