function exception=SEPrealignPET(petname,list,projectdir,verbose)
   
fr_fixed=1;
fwhm    =3;

if strcmp(petname,'fdg')
    fr_fixed=1:2;
end


exception='';
if nargin < 4
    verbose=0;
end

if nargin<2
    list = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
        
    end
end


subjects=readList(list);

%% DOING THE PROCESSING

for n=1:size(subjects,2)
    
    sid  =subjects{n}{1};
    visit=subjects{n}{2};
    
    if verbose
        fprintf('\n\n\n\n  +++ Processing subject %s / %s\n\n',sid,visit);
    end
    % Find petfolder in the raw folder
    petdir=get_subdir_regex(projectdir,'raw',subjects{n}{1},subjects{n}{2},['^' petname '$']);
    if isempty(petdir) 
        fprintf('-- Subject %s/%s missing %s folder\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    petdir=char(petdir);
    
    % Find pet image
    pet=get_subdir_regex_files(petdir,['^' sid '.*.nii']);
    if isempty(pet)
        fprintf('-- Subject %s/%s missing %s image\n',subjects{n}{1},subjects{n}{2},petname)
        continue
    end
    
    pet=char(pet);
    if verbose
        fprintf(' -- Reading image %s\n',pet);
    end
    
    
    % get output dir
    outdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},[petname 'preproc'],'');
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    others={};
    try 
        
        
        rezip=false;
        [path,name,ext]=fileparts(pet);
        if strcmp(ext,'.gz')
            if verbose
                fprintf('         Image is compressed\n');
            end

            [~,name,ext]=fileparts(name);
            rezip=true;
        end
        
        % check if output exists
        realigned=fullfile(outdir,[name '_movcorr' ext]);
        if exist(realigned,'file') || exist(fullfile(outdir,[name '_movcorr' ext '.gz']),'file')
            fprintf(' -- Image exists: %s\n',realigned)
            continue
        end
        
        % First, gunzip image
        if rezip
            cmd=sprintf('gunzip %s',pet);
            if verbose
                fprintf('%s\n',cmd)
            end
            system(cmd);
            pet         =fullfile(path,[name ext]);
        end
        
        % Create Symbolic link to avoid duplications
        sfile=fullfile(outdir,[name ext]);
        if ~exist(sfile,'file')
            cmd2=sprintf('ln -s %s %s',pet,sfile);
            if verbose
                fprintf('%s\n',cmd2);
            end
            system(cmd2);
        end
    
        % Do pet preprocess
        [rpetfile,~,others]=petpreprocess(sfile,fr_fixed,fwhm);
        
        %rename
        if verbose
            fprintf(' -- Renaming image to %s\n',realigned);
        end
        system(sprintf('fslmaths %s -nan %s',rpetfile,realigned));
        system(sprintf('rm -f %s',rpetfile))
    catch exception
        fprintf(' -- ERROR in the processing\n');
    end
    %% Cleaning stuff
    if exist('sfile')
        system(sprintf('rm -f %s',sfile));
    end
    if exist('others')
        for y=1:numel(others)
            system(sprintf('rm -f %s',others{y}));
        end
    end
    if 0 % right now... no rezip (it will slow next steps)
        system(sprintf('gzip %s',pet));
    end
end