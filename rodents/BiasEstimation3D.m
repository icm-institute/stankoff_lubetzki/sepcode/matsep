function [correctedimg,B] = BiasEstimation3D(inimg,outimg,mask,biasfct1,nbr1,biasfct2,nbr2,options)

%% HELP : [correctedimg,B] = BiasEstimation3D(inimg,outimg,mask,biasfct,nbr,options)
% This function allows to estimate the bias field on the inimg
% Inputs:
%   inimg: image not corrected
%   outimg: name of the output
%   mask: binary mask where the point used for the estimation were choosen
%   biasfct1: objectif function (b = @(a) a(1)*x, where a is the estimated
%   -> Initialize the parameter a
%   biasfct2: objectif function (b = @(a) a(1)*x, where a is the estimated; add only one parameter more than the first bias function
%   -> compute real bias
%   nbr: number of parameters would estimate -> size(a,1); default = 4
%   options: choose options for optimization with "optimset"; default = levenberg-marquardt
%
% Outputs:
%   Bias: image of the bias
%   correctedimg: inimg corrected for bias by division
%%

%% INITILIZATION
    [inimg,hdr] = readNIFTI(inimg);
    [l,c,p] = size(inimg);
    correctedimg = zeros(l,c,p);
    
%% OPTIONS

    if ~exist('options','var') || options == -1
        options = optimset('Algorithm','levenberg-marquardt','Display','off');
    end
    if ~exist('nbr1','var') || nbr == -1
        n1 = 4;
    else
	n1 = nbr1;
    end
    if ~exist('nbr2','var') || nbr == -1
        n2 = 5;
    else
	n2 = nbr2;
    end
    if ~exist('biasfct2','var') || biasfct == -1
        biasfct2 = @(a,X) a(5)*(X(:,2)-a(2)).^3 + a(4)*(X(:,2)-a(2)).^2 + a(3)*(X(:,2)-a(2)) + a(1);
    end
    if ~exist('biasfct1','var') || biasfct == -1
        biasfct1 = @(a,X) a(4)*(X(:,2)-a(2)).^2 + a(3)*(X(:,2)-a(2)) + a(1);
    end
    if ischar(class(mask))
        pts = randPts(mask,2000);
        
    else
        if strcmp(class(mask),'double')
            pts = mask;
        end
    end

%% CORRECTION

    x0 = zeros(n1,1);
    [tmp,cv,~,x_in,~] = Bias(inimg,x0,pts,biasfct1,options);
    x_in = [x_in ; zeros((n2-n1),1)];
    [tmp,~,cv_post,~,B] = Bias(inimg,x_in,pts,biasfct2,options);
    
    correctedimg(:,:,:) = tmp;
        
    disp(['Cv initial image : ' num2str(cv)]);
    disp(['Cv corrected image : ' num2str(cv_post)]);

    correctedimg(:,1:c/4,:) = 0;
    writeNIFTI(outimg,correctedimg,hdr);

function [correctedimg, cv, cv_post,x,Bias] = Bias(inimg,x0,pts,biasfct,options)
    [l,c,z] = size(inimg);
    Bias = zeros(l,c,z);
    
    % Extract background
    background = imfilter(inimg,fspecial3('gaussian'),'replicate','same');

    % Extract principals edges in the slice
    laplacian = imfilter(inimg,fspecial3('laplacian'),'replicate','same');

    % Select points in data to generate artificial data
    D = [];
    pts_exclus = [];
    I = [];
    
    rpl = pts(:,1);
    rpc = pts(:,2);
    rpz = pts(:,3);
    
    for i = 1:size(rpc,1)
        if laplacian(rpl(i),rpc(i),rpz(i)) >= 0
            D = [D ; rpl(i) rpc(i) rpz(i) background(rpl(i),rpc(i),rpz(i))];
            I = [I ; inimg(rpl(i),rpc(i),rpz(i))];
        else
            pts_exclus = [pts_exclus ; rpl(i) rpc(i) rpz(i)];
        end
    end

    % Parametric equation
    objfcn = @(a)sum((D(:,4)-biasfct(a,D)).^2);

    % Estimate parameters
    [x,~,~,~,~] = lsqnonlin(objfcn,x0,[],[],options);
    
    % Bias Field signal
    for i = 1:l
        for j = 1:c
            for k = 1:z
            tmp = [i j k];
            Bias(i,j,k) = biasfct(x,tmp);  
            end
        end
    end
    
    % Divide MRI image
    correctedimg = inimg ./ Bias;
    
    % Coefficients of variations before correction
    cv = std(I)/mean(I);
    I_post = zeros(size(D,1),1);
    for i = 1:size(D,1) 
        I_post(i,1)  = correctedimg(D(i,1),D(i,2),D(i,3));
    end
    cv_post = std(I_post)/mean(I_post);
