function gmclass=ordergmdistribution(gmdist)
%
% mclass=ordergmdistribution(gmdist)
%
% Sort gmdist according the means of the first component
% In practice, the first component should be T1-w data.
% Once ordered the classes are expected to be CSF,GM & WM
%

k  =gmdist.NComponents;
dim=size(gmdist.mu,2);


[~,idx]=sort(gmdist.mu(:,1));

gmclass=gmdistribution(gmdist.mu(idx,:),gmdist.Sigma(:,:,idx),gmdist.PComponents(idx));


end