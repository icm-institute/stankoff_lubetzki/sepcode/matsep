function mm=partialMean(filename,frames,outfile)
% From a 4D file, creates a mean of selected frames
%
%  return: 3D image with the partial mean
% 
% filename: name of the 4D image
% frames  : selected frames (starting with 1)
% outfile : name of the output image (optional)
files=char(select4Dframes(filename,frames));
V=spm_vol(files);

imas=spm_read_vols(V);

mm= mean(imas,4);

if nargin==3
    vout=V(1);
    vout.n=[1 1];
    vout.fname=outfile,
    spm_write_vol(vout,mm);
end