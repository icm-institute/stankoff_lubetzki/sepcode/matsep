

function [point1,point2,dist]=trk_findcrossing(t1,t2)




%bsxfun
if isstruct(t1)
    t1=t1(1).matrix;
end
if isstruct(t2)
    t2=t2(1).matrix;
end
    

dist=zeros(size(t1,1),size(t2,1));

for d=1:3
    dist=dist+bsxfun(@minus,t1(:,d),t2(:,d)').^2;
end


[~,point1]     =min(min(dist,[],1));
[dist,point2]  =min(min(dist,[],2));

dist=sqrt(dist);





    
    