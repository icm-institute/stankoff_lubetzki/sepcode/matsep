function [ti,tac_val]=getClosest(aif,tzero,error)
%% Extract time and value form a aif structrue
% [ti,tac_val]=getClosest(aif,tzero,error)
% error is the min time in seconds that is acceptable
%
    if nargin < 3
        error=4
    end
    time=aif.time;
    tac =aif.tac;
    
    [mval,idx]=min(abs(time-tzero));
    
    if mval>error
        fprintf(' -- Error bigger than threshold : %f\n',mval);
        ti=-1;
        tac_val=NaN;
    else
        ti=time(idx);
        tac_val=tac(idx);
    end