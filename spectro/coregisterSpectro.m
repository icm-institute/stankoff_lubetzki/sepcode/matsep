function coregisterSpectro(t1,mtoff,spectrodir)


    spectro = get_subdir_regex_files(spectrodir,'.*REF.nii');
    jobs={};

    for ns=1:length(spectro)
        jobs = do_coregister(mtoff,t1,char(spectro(ns)),'',jobs);
    end

    spm_jobman('run',jobs)  

end