function writeIF(filename,time,plasma,blood)
%% Write input function file
%
%  writeIF(filename,time,plasma,blood,unit_time,unit_act)
% OUTPUT UNITS: same as input%
%
%
% FILEFORMAT
% Includest 2 or 3 columns of data
% 1 column) time
% 2 column) plasma activity
% 3 column) blood activity (this one is optional)
%
%
% date: 09/02/2015
% author: D.Garcia-Lorenzo

if numel(time) ~= numel(plasma)
error('Different number of samples in time (%d) and plasma (%d)')
end


% Checking for blood samples
cols=2;
if numel(plasma)==numel(time)
cols=3;
end

% organize data
data=zeros(numel(time),cols);

data(:,1)=time(:);
data(:,2)=plasma(:);
data(:,3)=blood(:);

% write data
csvwrite(filename,data);

