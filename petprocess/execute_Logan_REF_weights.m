%% [Vt,SD_Vt,CV_Vt]= execute_Logan_REF_weights(time,tac,integral_ref,n_points,NSD,name_roi,displ)
% 
%   + Revision Code
%  From: Click fit, version PET course (Feb 2013)
%  Reviewed by: MV (March 2013)
%----------------------------

function [Vt,SD_Vt,CV_Vt]= execute_Logan_REF_weights(time,tac,integral_ref,n_points,NSD,name_roi,displ)

if ~exist('displ')
  displ=0;
end

%Initialization
N=length(time);
if (N/2)<=n_points
    n_points=round(N/2)-1;
end

%Integral evaulations of Ctissue
frames(1)=2*time(1);
for i=2:length(time)
    frames(i)=2*(time(i)-sum(frames));
end
integral_Ct=cumsum(tac.*frames');

%Weights
weights=setweights(NSD,tac);

% Checks tac negativity
ind=find(tac<=0);
tac(ind)=1;
weights(ind)=0;

% Regression line
y_axis=integral_Ct./tac;
x_axis=integral_ref./tac;

%indices for which t>t*
qq=(length(time)-n_points+1):1:length(time);

%Regression
y_axis2=y_axis(qq);
n_good_frames=length(y_axis2);
X=[x_axis(qq),ones(1,n_good_frames)'];
w=weights(qq);
n_good_data=length(find(w>0));
W=diag(w);
warning off
param=inv(X'*W*X)*X'*W*y_axis2;

% unusedy=x_axis*param(1)+param(2);
J=(y_axis2-X*param)'*W*(y_axis2-X*param);

covar=(J/(n_good_data-2))*inv(X'*W*X);
warning on
pstderr=sqrt(diag(covar));

%Output
Vt=param(1);
SD_Vt=pstderr(1,1);
CV_Vt=SD_Vt/Vt*100;

if isinf(CV_Vt) || isnan(CV_Vt) || (CV_Vt<0)
    Vt=-1;
    CV_Vt=-1;
    SD_Vt=-1;
end

if displ && nargin>4
    %arbitrarily exclude the first 3 frames from the plot of the Ct /Cref ratio
    figure
    plot(x_axis(4:length(x_axis)),y_axis(4:length(x_axis)),'+r','linewidth',4)
    hold on
    plot(x_axis(qq),x_axis(qq)*param(1)+param(2),'b')
    xlabel('INTEGRAL(REF) / Tissue  (minutes)')
    ylabel('INTEGRAL(Tissue) / Tissue  (minutes)')
    title(['ROI: ',name_roi,'DVR: ',num2str(param(1))],'Interpreter','None');
end
