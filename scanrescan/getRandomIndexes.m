
function idx=getRandomIndexes(mask,n)
% Get random samples from a region defined from a mask
%
% mask = binary image limiting the extent of the ROI of interest
% n    = number of samples (def=100)
%
% idx     = index of the random samples
if nargin==1
    n=100;
end

vals=find(mask);

idx=vals(random('unid',size(vals,1),[n 1]));


