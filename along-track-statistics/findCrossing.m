

%% add necessary paths
addpath('/servernas/home/dgarcia/src/cenir_irm/tools/matlab/along-tract-stats-master')
fslmatlab='/usr/cenir/src/fsl5-debian_64/etc/matlab/';
addpath(fslmatlab);


datadir='../process/';
subjects=get_subdir_regex(datadir,'CERTRE');



outliers=[];
for n=1:length(subjects)
    
    tracts=char(get_subdir_regex_files(subjects{n},'meantrack_i0.trk'));
    
    [h1, t1] = trk_read(deblank(tracts(1,:)));  
    [h2, t2] = trk_read(deblank(tracts(2,:)));  
    
    [p1,p2,dist]=trk_findcrossing(t1,t2);
    
    fprintf(' -- %d Crossing is between %d and %d, distance = %1.2f\n',n,p1,p2,dist);
    fprintf('               FA1 = %1.2f      FA2 = %1.2f\n',mean(t1(1).matrix(:,4)),mean(t2(1).matrix(:,4)))
    if dist >3
        outliers=[ outliers; n];
    end
    
    
end
%%
fprintf(' -- Check these tracks! %s\n',subjects{outliers})
    
    
    