function out=sameroiAnalysis(ima1,ima2,mask1,mask2,roisize,iters,codist_contrast)

out.roisize=roisize;
out.iterations=iters;
out.codist_contrast=codist_contrast;

mean1_mask1=zeros(iters,1);
mean2_mask1=zeros(iters,1);

if nargin > 6 && codist_contrast > 0
    mean1_mask2=zeros(iters,1);
    mean2_mask2=zeros(iters,1);
else
    codist_contrast = 0;
end


parfor i=1:iters
    
    %% Get random rois
    roi1=[];
    roi2=[];
    
    errors=1;
    while 1
    
        %% get random point in mask1
        idx1=getRandomIndexes(mask1,1);
        
        %% Compute roi in mask 1
        roi1=randomROI(idx1,mask1,roisize);
        if numel(roi1)==0
            errors=errors+1;
            continue;
        end

        % check if we do the contrast
        if codist_contrast>0
            % get points around the sphere on point1
            neighIdxs=getSphereIdxs(idx1,codist_contrast,mask2);

            % select only the idxs in the mask
            maskIdx=neighIdxs(mask2(neighIdxs)>0.5);

            % random point from the mask
            idx2=maskIdx(random('unid',numel(maskIdx),1));
            
            roi2=randomROI(idx2,mask2,roisize);
            
            if numel(roi2)==0
                errors=errors+1;
                continue
            end
        end
        % Everything when well
        break
    end
    
    
    %% Once we have the two regions 
    mean1_mask1(i)=mean(ima1(roi1));
    mean2_mask1(i)=mean(ima2(roi1));
    
    if codist_contrast > 0
        mean1_mask2(i)=mean(ima1(roi2));
        mean2_mask2(i)=mean(ima2(roi2));
    end
end

%% save data

out.mean1_mask1=mean1_mask1;
out.mean2_mask1=mean2_mask1;
if codist_contrast > 0
    out.mean1_mask2=mean1_mask2;
    out.mean2_mask2=mean2_mask2;
end

