function outimg = BrainMask(inimg,output)
%% Main to extract a brain mask
% inimg: path to the input image
% seuil: 
% taille: height of the brain (estimation) in pixel

    [inimg,hdr] = readNIFTI(inimg);
    
    [l,c,p] = size(inimg);
    i_otsu1 = zeros(size(inimg));
    i_otsu2 = zeros(size(inimg));
    interimg = zeros(size(inimg));
    outimg = zeros(size(inimg));
    
%%    
    for k = 1:p
         tmp = squeeze(inimg(:,:,k));
         i_otsu1(:,:,k) = reshape(otsu(tmp,3)>0,l,c,1);
    end
    
    se1 = strel('square',4);
    i_open1 = imopen(i_otsu1,se1);

    se2 = strel('rectangle',[10 6]);
    i_close1 = imclose(i_open1,se2);
    
%%    
    for j = 1:c
         tmp = squeeze(inimg(:,j,:));
         i_otsu2(:,j,:) = reshape(otsu(tmp,3)>0,l,1,p);
    end
    
    se3 = strel('square',3);
    i_open2 = imopen(i_otsu2,se3);
   
    se4 = strel('square',4);
    i_close2 = imclose(i_open2,se4);

%%
    for k = 1:p
        interimg(:,:,k) = i_close1(:,:,k).*i_close2(:,:,k);
    end
    
%     se5 = strel('square',5);
%     outimg = imopen(interimg,se5);
   
    
     outimg = bwareaopen(interimg,800,26); % realize morphological opening on a binary image
%     o = imdilate(o,strel('square',10)); % realize dilatation
%     
%     pos = find(inimg(floor(l/2),:,floor(p/2)) > seuil, 1, 'last' );
%     
%     o(:,pos+1:c,:) = 0;
%     o(:,1:pos-taille,:) = 0;
    
    writeNIFTI(output,outimg,hdr);

 

