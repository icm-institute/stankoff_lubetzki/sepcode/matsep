function exception=SEPcheckTACS(petname,listname,projectdir)
exception='';

if nargin<2 || isempty(listname)
    listname = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

if nargin < 4
    useraw=0;
end
%% REGION INFO
ids=    [  1,   2,     10,    11,    12,    13,   17,     18,    26, ...
                       49,    50,    51,    52,    53,    54,    58];
labels={'wm','gm','ThalL','CauL','PutL','StrL','HipL','AgmL','HCaL', ...
                  'ThalR','CauR','PutR','StrR','HipR','AgmR','HCaR'};  
          



%% Read the list
subjects=readList(listname);


for n=1:size(subjects,2)
    
    %% Getting cluster file
    tacsfolder=[petname 'TACs'];    
    tacsdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},tacsfolder,'');
    clusterfile=sprintf('%s/%s_%s_%s_tacs.mat',tacsdir,subjects{n}{1},subjects{n}{2},petname);
    
   
    if ~exist(clusterfile,'file')
        fprintf(' -- ERROR: Missing Cluster file !\n ')
        continue
    end
   
    %% If available getting SCA reference region tac
    scafolder=[petname 'super'];
    scadir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},scafolder,'');
    reftac=fullfile(scadir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_refregion_tac.mat']);

    
    
    tacs=load(clusterfile);
    time=tacs.tacs(1).time./60;
    wm=tacs.tacs(1).tac./1000;
    gm=tacs.tacs(2).tac./1000;
    thaL=tacs.tacs(3).tac./1000;

    if exist(reftac,'file')
        fprintf(' -- Including SCA clustering\n ')
        refs=load(reftac);
    
        figure
        ref=refs.tacs(1).tac./1000;
        plot(time,wm,time,gm,time,thaL,time,ref);
        legend('WM','GM','Thal L','SCA REF')
    else
        figure
        plot(time,wm,time,gm,time,thaL)
        legend('WM','GM','Thal L')
    end
    title(sprintf('%s Tacs from %s %s',petname,subjects{n}{1},subjects{n}{2}))
    xlabel('Time (min)')
    ylabel('Mbq')
    disp(' Hit a key to continue')
    pause;
    close all;
    
end