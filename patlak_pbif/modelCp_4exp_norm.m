function yCp = modelCp_4exp_norm(par,cal,t)

if isempty(cal)
    cal = 1;
end

% l0 = par(:,1);
% t0 = par(:,2);
% T  = par(:,3);
% A1 = par(:,4);
% l1 = par(:,5);
% A2 = par(:,6);
% l2 = par(:,7);
% A3 = par(:,8);
% l3 = par(:,9);

t0 = par(:,2);
T  = exp(par(:,3));
A1 = exp(par(:,4));
A2 = exp(par(:,6));
A3 = exp(par(:,8));
a3 = exp(par(:,9));

a2 = a3 + exp(par(:,7));
a1 = a2 + exp(par(:,5));
a0 = a1 + exp(par(:,1));


aPole = SimplePoleDelayConvRect([a0,t0,T],t);

yCp   = (A1.*a1.*a0)./(T.*(a0 - a1)).*(SimplePoleDelayConvRect([a1,t0,T],t) - aPole) + ...
        (A2.*a2.*a0)./(T.*(a0 - a2)).*(SimplePoleDelayConvRect([a2,t0,T],t) - aPole) + ...
        (A3.*a3.*a0)./(T.*(a0 - a3)).*(SimplePoleDelayConvRect([a3,t0,T],t) - aPole);
    
yCp   = cal*yCp;
       
    if any(isnan(yCp)) | any(isinf(yCp))
        keyboard
    end