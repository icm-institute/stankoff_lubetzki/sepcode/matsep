function FMZQuantification(vfile,sujid,outdir,rawdir)
%% Quantification of the Flumazenil pet image
% Input : 
% - vfile : pet image in .v or .nii/.nii.gz
% - sujid : label of your subject like C_XXX
% - outdir : directory where you want to store outputs
% default - if no outdir provided, outputs will be stored in the same
% directory as pet input
% - rawdir : raw directory containing Template directory with informations
% about pons on MNI for correction
% Output:
% - bmax : Quantification of changes in  benzodiazepine receptor density
% - kd : apparent FMZ equilibrium dissociation constante
    
    disp('');
    disp(' ---------------------------------------- ');
    fprintf('Start quantification for %s ',sujid);
    disp(' ---------------------------------------- ');
    disp('');
    tic;
    %% Input files
    [petv,pethd] = readNIFTI(char(vfile));
    bmaxf = fullfile(char(outdir),strcat(sujid,'_fmz_bmax_InitialMethod.nii.gz'));
    kdf = fullfile(char(outdir),strcat(sujid,'_fmz_kd_InitialMethod.nii.gz'));
    
    CVbmaxf = fullfile(char(outdir),strcat(sujid,'_fmz_bmax_CV.nii.gz'));
    CVkdf = fullfile(char(outdir),strcat(sujid,'_fmz_kd_CV.nii.gz'));
    
    dosef = get_subdir_regex_files(char(regexprep(regexprep(outdir,'fmzprocess','fmz'),'process','raw')),'.*infos.dat');
    dose = load(char(dosef));
    ponsf = get_subdir_regex_files(outdir,'.*pons.*values.dat');
    pons = load(char(ponsf));
    ponscorrdir = get_subdir_regex(rawdir,'Template');
    ponscorr = get_subdir_regex_files(ponscorrdir,'corr_pons.dat');
    pons_corr = load(char(ponscorr));

    %% Informations from input files
    ras = dose(1);
    doseC = dose(2);
    doseF = dose(3);
    rap = (doseC + doseF)/doseC;

    %% Scatchard Method

    [nx,ny,np,nt] = size(petv);

    % Lissage du pons mesuré
    betamin     = 0.0001;
    betamax     = 0.5;
    ncomp       = 1000;
    betagrid    = logspace(log10(betamin),log10(betamax),ncomp)';
    betagrid(1) = 0;  
    % matrix construction
    G = zeros(length(pons(:,1)),ncomp);
    for j = 1 : ncomp
        G(:,j) = exp(-betagrid(j)*pons(:,1));
    end
    % weights
    w     = sqrt(abs(1./(pons(:,2).*exp(0.034*pons(:,1)))));
    GW    = G .* repmat(w,1,size(G,2));
    ponsW = pons(:,2).*w;
    % non-negative estimation
    B = lsqnonneg(GW,ponsW);    
    % fitted pons
    pons(:,2) = G*B;
    
%     fct = @(fitpons) fitpons(1)*exp(-fitpons(2)*pons(:,1)) + fitpons(3)*exp(-fitpons(4)*pons(:,1));
%     w = sqrt(abs(1./pons(:,2)));
%     objfcn = @(a)(w.*(pons(:,2)-fct(a)));
%     x0 = [pons(1,2)/2;1;pons(1,2)/2;0.1];
%     [x,~,~,~] = lsqnonlin(objfcn,x0,zeros(size(x0)),inf(size(x0)));
    
    % Correction of pons values
    time = pons(:,1);
    F = pons(:,2);
    per_corr = interp1(pons_corr(:,1),pons_corr(:,2),time(:));
    F = ( 1 - per_corr ).*F;

    % RAS Correction
    petv = petv./ras;
    petv = petv.*rap;
    petv = petv/37.0;
    F = F./ras;
    F = F.*rap;

    % Parameter Calculation
    m_debut = 3;
    xo = [-0.4, 15];
    s1 = warning('error','MATLAB:nearlySingularMatrix');
	s2 = warning('error','MATLAB:singularMatrix');

    bmax = zeros(nx,nx,np);
    kd = zeros(nx,nx,np);
    CVbmax = zeros(nx,nx,np);
    CVkd = zeros(nx,nx,np);
    planbase = zeros(nx,ny,nt);
    plan = zeros(nx,ny,nt);

    for kp=1:np
        display([num2str(kp),'/',num2str(np)])
        %% Moyenne temporelle
        clear planbase; 
        clear plan;
        mask = [ 0 1 0 ; 1 4 1 ; 0 1 0 ];
        for kt = 1:nt
            planbase(:,:,kt) = petv(:,:,kp,kt);
            ind = 1;
            if(kp > 1) 
                planbase(:,:,kt) = planbase(:,:,kt) + petv(:,:,kp-1,kt);
                ind = ind + 1;
            end
            if(kp < nt) 
                planbase(:,:,kt) = planbase(:,:,kt) + petv(:,:,kp+1,kt);
                ind = ind + 1;
            end
            planbase(:,:,kt) = planbase(:,:,kt) / ind;
            % filtrage spatial dans le plan
            plan(:,:,kt) = conv2(planbase(:,:,kt),mask,'same');
            plan(:,:,kt) = plan(:,:,kt) / 8;
        end
        planbase = plan;

        %filtage temporel
        clear plan;
        for kt=1:nt
            ind = 3;
            plan(:,:,kt) = 3*planbase(:,:,kt);
            if(kt)>1
                plan(:,:,kt) = plan(:,:,kt) + planbase(:,:,kt-1);
                ind = ind + 1;
            end
            if(kt)>2 
                plan(:,:,kt) = plan(:,:,kt) + 0.5*planbase(:,:,kt-2);
                ind = ind + 0.5;
            end
            if(kt)<nt
                plan(:,:,kt) = plan(:,:,kt) + planbase(:,:,kt+1);
                ind = ind + 1;
            end
            if(kt)<nt-1
                plan(:,:,kt) = plan(:,:,kt) + 0.5*planbase(:,:,kt+2);
                ind = ind + 0.5;
            end
            plan(:,:,kt) = plan(:,:,kt)/ind;  
        end
        planbase = plan;

        % regroupement des pixels pour un plan 
        planbase(planbase < 0) = 0;
        % Calcul pour chaque pixel
        for kx = 1:nx
            for ky = 1:nx
                b = zeros(nt,1);
                
                b(:) = planbase(kx,ky,:);
                b(:) = b(:) - F(:);
                b(b < 0 ) = 0;
%                 for i=1:nt
%                     b(i) = planbase(kx,ky,i);
%                     b(i) = b(i) - F(i);
%                     if b(i)<0; b(i)=0; end
%                 end  

                % suppression des regions vides
                xx = mean(b);
                if xx > 2
                    mat = zeros(nt-(m_debut),2);
                    % Création de la courbe de Scatchard
                    mat(:,1) = b(m_debut:end);
                    mat(:,2) = b(m_debut:end) ./ F(m_debut:end);
%                     for i = m_debut+1: nt
%                             mat(i-m_debut,1) = b(i);
%                             mat(i-m_debut,2) = b(i) / F(i);
%                     end

                    % minimisation de (y - ax -b)^2 + (x -y/a +b/a)^2
                    
                    f = @(par)funps(par,mat);
                    %% ------------ MINIMISATION -------------------------
                    %[sol,~,~]  = fminsearch(@funps,xo,optimset('MaxFunEvals',500),mat);
                    [sol,RESNORM,RESIDUAL,EXITFLAG,OUTPUT,LAMBDA,JACOBIAN] = lsqnonlin(f,xo,[],[],optimset('MaxIter',500,'MaxFunEvals',5000,'Algorithm','levenberg-marquardt','Display','off'));
                    bmax(kx,ky,kp) = -sol(2)/sol(1);
                    kd (kx,ky,kp) = -1/sol(1);
                    sigmaq = RESNORM/(nt-m_debut-2);
                    

                    try
                        cov = full(sigmaq*inv(JACOBIAN'*JACOBIAN));
                        dbmax1 = sol(2)/(sol(1)^2);
                        dbmax2 = -1/sol(1);
                        dkd1   = 1/(sol(1)^2);
                        sdbmax = sqrt(dbmax1^2*cov(1,1) + dbmax2^2*cov(2,2) + 2*dbmax1*dbmax2*cov(1,2));
                        sdkd   = sqrt(dkd1^2*cov(1,1));
                        CVbmax(kx,ky,kp) = sdbmax*100/abs(bmax(kx,ky,kp));
                        CVkd(kx,ky,kp) = sdkd*100/abs(kd(kx,ky,kp));
                    catch
                        CVbmax(kx,ky,kp) = 0;
                        CVkd(kx,ky,kp)   = 0;
                    end

                else 
                    bmax(kx,ky,kp)   = 0;
                    kd(kx,ky,kp)     = 0;
                    CVbmax(kx,ky,kp) = 0;
                    CVkd(kx,ky,kp)   = 0;
                end
                
            end
        end
    end

    bmax(bmax < 0) = 0;
    kd(kd < 0) = 0;

    %% Output volumes
    pethd_fin = pethd(1);
    writeNIFTI(bmaxf,bmax,pethd_fin);
    writeNIFTI(kdf,kd,pethd_fin);
    writeNIFTI(CVbmaxf,CVbmax,pethd_fin);
    writeNIFTI(CVkdf,CVkd,pethd_fin);

    tSec = toc;
    tMin = tSec/60;
    disp('');
    disp(' -------------------- ');
    fprintf('Quantification done for %s in %u min',sujid,round(tMin));
    disp(' -------------------- ');
    disp('');
end

