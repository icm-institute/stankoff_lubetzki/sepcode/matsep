

function [time,plasma,blood]=readIF(filename,unit_time,unit_act)
%% Read Arterial input function file
%
%  [time,plasma,blood]=readIF(filename)
% OUTPUT UNITS: seconds and Kbq
%
%
% filename: file with aif data
% unit_time : 0 automatic:: try to guess units (if max < 200 is min)
%             1 minutes:: convert them to seconds
%             2 seconds:: do nothing
% unit_act : 0 automatic:: try to guess units (if max < 100  Kbq)
%             1 Bq :: Divide by 1000
%             2 Kbq:: Do nothing
%
% FILEFORMAT
% Includest 2 or 3 columns of data
% 1 column) time
% 2 column) plasma activity
% 3 column) blood activity (this one is optional)
%
%
% date: 09/02/2015
% author: D.Garcia-Lorenzo

% Read Data
rawdata=importdata(filename);

% Assign time and plasma
time=rawdata(:,1);
plasma =rawdata(:,2);

% Check if blood is available
if size(rawdata,2)>2
    blood=rawdata(:,3)
else
    blood=[];
end

%% Check unit time
if ~exist('unit_time','var') || unit_time==0
    unit_time=2;
    if max(time) < 200
        unit_time=1;
    end
end
%% Check unit activity
if ~exist('unit_act','var') || unit_act==0
    unit_act=2;
    if max(plasma) > 100
        unit_act=1;
    end
end

% Add zero if not in the input
if time(1) > 0
time   =[0; time];
    plasma =[0; plasma  ];
    if numel(blood)
      blood  =[0; blood  ];
    end
end

% Convert units
if unit_time==1
    time=time*60;
end
if unit_act==1
    plasma=plasma./1000;
    blood=blood./1000;
end