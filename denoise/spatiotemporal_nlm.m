function [denoised,nweights]=spatiotemporal_nlm(dynPET, mask, beta,patchsize,searchsize,tzero)


if ~exist('patchsize','var')
    searchsize=1;
end
if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end

%% 
dims   =size(dynPET);
nframes=dims(4)-tzero;



% output image
denoised=zeros(dims); 
nweights=zeros(dims(1:3));


%% Create average image
fsize=3;
mpet = convn(dynPET,ones(fsize,fsize),'same') ./ (fsize^2);


%% Start processing voxels

for k=1:dims(3) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(2)
for i=1:dims(1)
    
    if mask(i,j,k)==0
        continue
    end
    
    ip=max(1,i-patchsize):min(dims(1),i+patchsize);
    tac=dynPET(i,j,k,:);
    mtac=mpet(i,j,k,:);
    
    
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(1),i+searchsize);
    Jmax=min(dims(2),j+searchsize);
    Kmax=min(dims(3),k+searchsize);

    %% First: estimate min distance
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && y==j && z==k
            continue
        end

        d=distance2(dynPET,mpet,i,j,k,x,y,z,patchsize,tzero);        
        mindist=min(mindist,d);
    end
    end
    end







    %% Apply weights
    wmax=0;
    dtac=zeros(1,1,1,dims(4));
    wmean=0;

   
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        if i==x && y==j && z==k
            continue
        end


        d=distance(dynPET,i,j,k,x,y,z,patchsize,tzero);
        

        
        w=exp(-d/(beta*mindist));
        if w > wmax
            wmax=w;
        end
        dtac =dtac+ w .* dynPET(x,y,z,:);
        wmean=wmean + w;
        
    end
    end
    end
    
    %if dnum>0
        %dima(i,j,k)=dmean/dnum;
    %end
    nweights(i,j,k)=wmean;
    
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) /(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end



function d=distance(ima,i,j,k,x,y,z,psize,tzero)

dims=size(ima);

%% Checking borders
xmin=- min([psize,x-1,i-1]);
ymin=- min([psize,y-1,j-1]);
zmin=- min([psize,z-1,k-1]);

xmax= min([psize,dims(1)-x,dims(1)-i]);
ymax= min([psize,dims(2)-y,dims(2)-j]);
zmax= min([psize,dims(3)-z,dims(3)-k]);

%% do the sum
d= sum(sum(sum(sum((ima(i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax),tzero:end) ...
                  - ima(x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax),tzero:end)).^2))));    
d=d./((xmax-xmin)*(ymax-ymin)*(zmax-zmin));

function d=distance2(ima,mima,i,j,k,x,y,z,psize,tzero)

dims=size(ima);

%% Checking borders
xmin=- min([psize x-1 i-1]);
ymin=- min([psize y-1 j-1]);
zmin=- min([psize z-1 k-1]);
xmax= min([psize,dims(1)-x,dims(1)-i]);
ymax= min([psize,dims(2)-y,dims(2)-j]);
zmax= min([psize,dims(3)-z,dims(3)-k]);
%% do the sum
d= sum(sum(sum(sum((ima(i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax),tzero:end) ...
                  - ima(x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax),tzero:end) ...
                  -mima(i+(xmin:xmax),j+(ymin:ymax),k+(zmin:zmax),tzero:end) ...
                  +mima(x+(xmin:xmax),y+(ymin:ymax),z+(zmin:zmax),tzero:end) ).^2))));    
d=d./((xmax-xmin)*(ymax-ymin)*(zmax-zmin));


