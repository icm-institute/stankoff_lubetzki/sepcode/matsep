

function fsCleanLesions(sidpaths)

addpath '/usr/cenir/src/freesurfer5.3.0-centos6_x86_64/matlab/'

if nargin==0
  sidpaths=spm_select([1 inf],'dir','Select freesurfer Subject dirs');
end

if isstr(sidpaths)
  for l=1:size(sidpaths,1);
    tmp{l}=deblank(sidpaths(l,:));
  end
  sidpaths=tmp;
end

for s=1:numel(sidpaths)
  fprintf(' -- Processing %s\n',sidpaths{s})
  if ~exist(sidpaths{s},'dir')
    fprintf(' ** ERROR: Path not found, skipping');
    continue
  end
  % Read wm 
  wm   = MRIread(fullfile(sidpaths{s},'mri','wm.mgz'));
  
  % Verify changes
  totalchanges=sum(wm.vol(:)==255);
  fprintf(' -- Changes to be done %d\n',totalchanges);
  if totalchanges==0
    fprintf(' -- Nothing to be done... skipping\n');
    continue
  end


  %% Open images to be changed
  aseg = MRIread(fullfile(sidpaths{s},'mri','aseg.mgz'));
  norm = MRIread(fullfile(sidpaths{s},'mri','brain.mgz'));
  norm2 = MRIread(fullfile(sidpaths{s},'mri','brain.finalsurfs.mgz'));

  %% Keep backup copy
  %MRIwrite(aseg,fullfile(sidpaths{s},'mri','aseg.sep.mgz'));
  %MRIwrite(norm,fullfile(sidpaths{s},'mri','brain.sep.mgz'));
 
  
  %% create output
  outaseg=aseg;
  outnorm=norm;
  outnorm2=norm2;


  ss=size(wm.vol);

  
  idxs=find(wm.vol==255);

  outnorm.vol(idxs) =110;
  outnorm2.vol(idxs)=110;

  
  for i=idxs'
      v=[0; 0; 0; 1];
      [v(2),v(1),v(3)] = ind2sub(ss,i);

      ras= wm.vox2ras1 * v; % suing vox2ras1 (starting with 1 indexes as in matlab

      %% Deciding side only by matrix (to be checked)

      if ras(1) > 0
          outaseg.vol(i)=79, % right WM-hypo instead of 109; % Right WM abnomr :: instead of wm 41;
      elseif ras(1) < 0
          outaseg.vol(i)=78, % left wm-hyp instead of 100; % Left WM abnorm :: instead of 2;
      end
  end

  MRIwrite(outaseg ,fullfile(sidpaths{s},'mri','aseg.mgz'));
  MRIwrite(outnorm ,fullfile(sidpaths{s},'mri','brain.mgz'));
  MRIwrite(outnorm2,fullfile(sidpaths{s},'mri','brain.finalsurfs.mgz'));
  %system(sprintf('/usr/cenir/src/freesurfer5.3.0-centos6_x86_64/bin/mri_convert %s -odt uchar %s',outaseg.fspec,outaseg.fspec));
  %system(sprintf('/usr/cenir/src/freesurfer5.3.0-centos6_x86_64/bin/mri_convert %s -odt uchar %s',outnorm.fspec,outnorm.fspec));
  

end

