function [indSolution,param,SD_param,CV_param]=RPM_FindSolution(WRSS,RSS,ALFA,SD_ALFA,CV_ALFA,theta3)
%Sorting the data
[WRSS,indSort] = sort(WRSS);
RSS=RSS(indSort);
ALFA=ALFA(:,indSort);
CV_ALFA=CV_ALFA(:,indSort);
theta3 = theta3(indSort);

k=1;
trovato=false;
while ~trovato && k<=length(theta3);
    if max(CV_ALFA(:,k))<100 && min(CV_ALFA(:,k))>0
        trovato = true;
    else
        k=k+1;
    end
end

if k<=length(theta3)
    indSolution = k;
    [param,SD_param,CV_param] = RPM_KineticParameter(ALFA(:,indSolution),SD_ALFA(:,indSolution),CV_ALFA(:,indSolution),theta3(indSolution));
else
    indSolution = 1; %FAKE one as initial value
    param = zeros(size(ALFA(:,1))); param(3)=0;
    SD_param = -1*ones(size(param)); SD_param(3)=-1;
    CV_param = -1*ones(size(param)); CV_param(3)=-1;
end

    
    
    
  

