function [meanTac,TACs]=tacExtraction_perm(permPET,mask)
% Create tac from a 4D pet and a binary mask
% [meanTac,TACs]=tacExtraction(dyn,mask)
%
% Return meanTac and All individual tacs
%
n=size(permPET,1);
ind=find(mask>0);


TACs=permPET(:,ind);
meanTac=mean(TACs,2);


