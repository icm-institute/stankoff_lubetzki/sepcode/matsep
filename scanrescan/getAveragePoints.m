function [points roimean allpoints]= getAveragePoints(image,region,s,n,mask)
% From an image, create random samples in the roi and the compute
% new rois of size s inside mask
%  image: image to get the point from
%  region: binary region where we sample the center points
%  s: size of the roi 
%  n: number of random points
%  mask: points where the random roi is given, it should be at least == region 
%
% points = returns a list with the average of the rois computed
% roimean= visualize how many times each point was used.


if nargin<5
    mask=region;
end
if nargin<4
    n=100;
end
if nargin<3
    disp(' -- Error missing arguments');
    return
end


% extract random indexes
rndidx=getRandomIndexes(region,n);

% create an roi for each idx
points=zeros(n,1);
roimean    =zeros(size(mask));

% check output to reduce the memory used
if nargout==3
    allpoints=zeros(n,s);
end

repetitions=0;
for i=1:size(rndidx,1)
    %if mod(i,50)==0
    %    disp(sprintf(' -- Iteration:%d\n',i))
    %end
    
    roi=[];
    while numel(roi)==0
        idx=rndidx(i);
        repetitions=repetitions+1;
        % create the random roi
        roi=randomROI(idx,mask,s);
        if numel(roi)==0
            xx=getRandomIndexes(region,1);
            rndidx(i)=xx;
        end
        if mod(repetitions - i+1,n/10)==0
            fprintf(' **WARNING: %d repetitions, please check your regions\n',repetitions-i);
        end
    end
    
    % compute the image mean
    % TODO: include the option to add a "function"
    % in order to do the quantification along the 4D image
    points(i)=mean(image(roi));
    if nargout==3
        allpoints(i,:)=roi;
    end
    %disp(sprintf(i'%d -> %f\n',i,points(i)));

    % add to the roimean
    % roi me
    roimean(roi)=roimean(roi)+1;

end
