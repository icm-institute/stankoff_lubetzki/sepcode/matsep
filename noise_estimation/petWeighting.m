function [sig]=petWeighting(ima)

% Daniel Garcia
% Based on Indirect 4D Nonlocal means
% and from implementation of RicianSTD
%
% Compute standard deviation of an image by wavelets decomposition
%
% sigma=MAD(HHH) / 0.6745;
%
% For 4D image the results is a vecotr of sigmas for each 3D image
%
% 

double(ima);
s=size(ima);

if numel(s)==3
    s=[s 1];
end

sig=zeros(1,s(4));

% Estimation of the zeros pading size
p(1) = 2^(ceil(log2(s(1))));
p(2) = 2^(ceil(log2(s(2))));
p(3) = 2^(ceil(log2(s(3))));


for t=1:s(4)


    % Zeros Pading
    pad1 = zeros(p(1),p(2),p(3));
    pad1(1:s(1),1:s(2),1:s(3)) = ima(:,:,:,t);

    % Wavelet Transform
    [af, sf] = farras;
    w1 = dwt3D(pad1,1,af);

    % Removing region corresponding to zeros pading
    tmp = w1{1}{7};
    tmp2 = w1{2};
    tmp = tmp(1:round((s(1)-1)/2),1:round((s(2)-1)/2),1:round((s(3)-1)/2));
    tmp2 = tmp2(1:round((s(1)-1)/2),1:round((s(2)-1)/2),1:round((s(3)-1)/2));

    % Detection of the object in the LLL subband
    [mu,mask]=imagekmeans(tmp2,2);
    th=mean(mu);
    map = (tmp2(:,:,:)>th);

    % Detection of the High gradient area in the LLL subband
    [PX,PY,PZ] = gradient(tmp2);
    GR = sqrt(PX.^2 + PY.^2 + PZ.^2);
    m = median(GR(map));
    map2 = (GR(:,:,:)< (m));

    % Map containing Object without strong edges
    map = map & map2;

    % Estimation of the magnitude noise STD in HHH subband
    sig(t)= mad(tmp(:),1) / 0.6745;
    
end

