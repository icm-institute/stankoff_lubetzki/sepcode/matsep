
function nlm=prinlm(ima,level,blocksize,verbose)

if nargin==2
    blocksize=5;
end

doverbose=0
if nargin==4 && verbose
    doverbose=1
end
    
disp('1');
% Method 3: 3D ODCT filtering (proposed)
fima2=cM_ODCT3D(ima,level,1);

disp('2');
% Method 4: PRI-NLM3D filtering (proposed)
nlm=cM_RI_NLM3D(ima,blocksize,1,level,fima2,1); 

