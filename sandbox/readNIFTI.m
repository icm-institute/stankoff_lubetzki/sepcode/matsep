function [img,hdr]=readNIFTI(filename,rezip)
% Function to read .nii and .nii.gz files
%  [img,hdr]=readNIFTI(filename,rezip)
%
% if rezip=0, image is not gzip again, useful for several inputs
% It is based completely in SPM and Linux
% It checks if extension is .gz and gunzip image.
% If necessary it gunzip it at the end.
if nargin<2
    rezip=1;
end
filename=deblank(char(filename));
% Check if gunzip
[~,~,ext]=fileparts(filename);


% Open image dealing with exceptions to be sure image is rezip again
try
    if strcmp(ext,'.gz')
        system(['gunzip ' filename]);
        niftifile=filename(1:end-3);
    else
        niftifile=filename;
        rezip=0;
    end

    hdr=spm_vol(niftifile);
    img=spm_read_vols(hdr);

catch exception

    fprintf(' -- ERROR reading %s\n',niftifile);
    exception  %#ok<NOPRT>

end

% gzip image if necessary
if rezip && ~ strcmp(filename,nifti)
    system(['gzip ' niftifile]);
end
