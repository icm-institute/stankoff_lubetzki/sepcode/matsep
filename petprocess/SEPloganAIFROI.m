function exception=SEPloganAIFROI(petname,listname,projectdir)
exception='';

logtime=20; % logan time to start


if nargin<2
    listname = spm_select(1,'any','Select subjects list');
end

if nargin < 3 || isempty(projectdir)
    fprintf(' -- Searching for project_dir\n')
    if isempty(getenv('PROJECT_DIR')) 
        projectdir = spm_select(1,'dir','Select PROJECT_DIR folder');
    else
        fprintf('     ... Found enviroment variable\n')
        projectdir=getenv('PROJECT_DIR');
    end
end

%% Read the list
subjects=readList(listname);


for n=1:size(subjects,2)

    pdir=fullfile(projectdir,'process',subjects{n}{1},subjects{n}{2},'');
    %% setting output
    outpetfolder=[petname 'LoganAIFROI'];    
    outdir=fullfile(pdir,outpetfolder,'');
    outfile=fullfile(outdir,[subjects{n}{1} '_' subjects{n}{2} '_' petname '_loganAIFROI.csv']);
    
    aiffile=sprintf('../aifs/%s_aif.if',subjects{n}{1});
    if ~exist(aiffile,'file')
        fprintf(' ** Input Aif function not found! %s\n',aiffile)
        continue
    end
    
    if ~exist(outdir,'dir')
        mkdir(outdir);
    end
    
    if exist(outfile,'file')
        fprintf(' ** Skipping: logan file exists: %s\n',outfile);
        %continue
    end
    
    %% HELLO
    %% read input function
    [time,aif]        =readAIF(aiffile);
    
    %% Finding cluster file
    clusterfile=sprintf('%s/%s/%s_%s_%s_tacs.mat', ...
            pdir,[petname 'TACs'],subjects{n}{1},subjects{n}{2},petname);
    if ~exist(clusterfile,'file')
        fprintf (' ** ERROR : clusterfile not found: %s\n\n',clusterfile);
        continue
    end

    

    
    %% Find ref region if available
    reffile = get_subdir_regex_files([pdir '/' petname 'super'],'.mat$')
    if numel(reffile)~=1
        fprintf(' - super reference file not found...skipping')
    else
        b=load(char(reffile));
        ref=b.tacs;

        loganref=zeros(1,3);
        [loganref(1),loganref(2),loganref(3)]= ...
                execute_Logan_PLASMA_DGL(ref.time ./ 60, ref.tac./1000 ...
                        ,time./60,aif,logtime,1,char(ref.name));

        fprintf(' -- %s :: %s  ->  %1.2f    (%1.2f)\n',subjects{n}{1},char(ref.name),...
                        loganref(1),loganref(3));
                    
                    
        continue
    end

    
    %% Read clusters
    a=load(clusterfile);    
    tacs=a.tacs;
    
    loganvals=zeros(numel(tacs),3);
    p=fopen(outfile,'w');
    for m=1:numel(tacs)
        [loganvals(m,1),loganvals(m,2),loganvals(m,3)]= ...
            execute_Logan_PLASMA_DGL(tacs(m).time ./ 60, tacs(m).tac./1000 ...
                    ,time./60,aif,logtime,0,char(tacs(m).name));

        fprintf(' -- %s :: %s  ->  %1.2f    (%1.2f)\n',subjects{n}{1},char(tacs(m).name),...
                    loganvals(m,1),loganvals(m,3));
                
        fprintf(p,'%s,%s,%s,%1.3f,%1.3f,%1.3f\n', subjects{n}{1},subjects{n}{2},char(tacs(m).name), ...
                loganvals(m,1),loganvals(m,2),loganvals(m,3));

    end
    fclose(p);
    
    
    
end