function [denoised,nweights]=temporal_nlm_cout(dynPET, mask, beta,searchsize,tzero,cout,time,delta,ROI_tac,lambda,tdecay)

if ~exist('searchsize','var')
    searchsize=5;
end
if ~exist('cout','var')
    cout=1;
end
if ~exist('beta','var')
    beta=1;
end
if ~exist('tzero','var')
    tzero=5;
end

%% Half lives
% C11 = 1225 sec  20.4 min
% F18 = 6588     109.8 min
%
% lamda= ln2/ half-life




%% 
dims   =size(dynPET);
nframes=dims(4)-tzero;

% Create sigma2, with respect to time
sigma2= ones(1,1,1,dims(4));
if exist('time','var') && exist('delta','var')
  sigma2(:)= (ROI_tac .* exp(-(time-tdecay)*lambda) ./ delta);
end

% output image
denoised=zeros(dims); 
nweights=zeros(dims(1:3));


%% Create average image
fsize=3;
mpet = convn(dynPET,ones(fsize,fsize),'same') ./ (fsize^2);


%% Start processing voxels

for k=1:dims(3) % for each voxel
    %fprintf(' -- Slice %d\n',k)
    
for j=1:dims(2)
for i=1:dims(1)
    
    if mask(i,j,k)==0
        continue
    end
        
    tac=dynPET(i,j,k,:);
    mtac=mpet(i,j,k,:);
    
    
    
    Imin=max(1,i-searchsize);
    Jmin=max(1,j-searchsize);
    Kmin=max(1,k-searchsize);
    Imax=min(dims(1),i+searchsize);
    Jmax=min(dims(2),j+searchsize);
    Kmax=min(dims(3),k+searchsize);
    %% First: estimate min distance
    mindist=10^10;
    
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        
        %% skipping center of the square
        if abs(i-x) <= cout && abs(j-y) <= cout && abs(k-z) <= cout
            continue
        end

        d=sum(sum(sum(sum( sigma2(tzero:end) .* ((tac(tzero:end)-dynPET(x,y,z,tzero:end)-mtac(tzero:end)+mpet(x,y,z,tzero:end)).^2) ) ) ));
        mindist=min(mindist,d);

    end
    end
    end







    %% Apply weights
    wmax=0;
    dtac=zeros(1,1,1,dims(4));
    wmean=0;

   
    for z=Kmin:Kmax
    for y=Jmin:Jmax
    for x=Imin:Imax
        %% skipping center of the square
        if abs(i-x) <= cout && abs(j-y) <= cout && abs(k-z) <= cout
            continue
        end

        d=sum(sum(sum(sum( sigma2(tzero:end) .* ((tac(tzero:end)-dynPET(x,y,z,tzero:end)).^2) ) ) ));

        
        w=exp(-d/(beta*mindist));
        if w > wmax
            wmax=w;
        end
        dtac =dtac+ w .* dynPET(x,y,z,:);
        wmean=wmean + w;
        
    end
    end
    end
    
    %if dnum>0
        %dima(i,j,k)=dmean/dnum;
    %end
    nweights(i,j,k)=wmean;
    
    if wmean >0
        denoised(i,j,k,:)=(wmax * tac + dtac) /(wmean+wmax);
    else
        denoised(i,j,k,:)=0;
    end
end
end
end