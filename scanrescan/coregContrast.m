
function out=coregContrast(ima,mask1,mask2,s,codist,iters)
% Reproducibility measures using coregistered images 
%
% ima1,ima2 : images to compared resliced in the same space
% mask      : target region binary mask
% regsizes  : size of the regions
% codist    : distance between the two regions
% iters     : number of random regions

% create output structure
out.roisize=s;
out.iterations=iters;
out.codist=codist;
mean1 =zeros(iters,1);
mean2 =zeros(iters,1);
cross1 =zeros(iters,1);
cross2 =zeros(iters,1);




parfor i=1:iters
    
    %% Get random rois
    roi1=[];
    roi2=[];
    
    errors=1;
    while numel(roi1)==0 || numel(roi2)==0
    
        %% get random point1 in ima1
        idx1=getRandomIndexes(mask1,1);

        %% get random point2

        % get points around the sphere on point1
        neighIdxs=getSphereIdxs(idx1,codist,mask1);

        % select only the idxs in the mask
        maskIdx=neighIdxs(mask2(neighIdxs)>0.5);

        % random point from the mask
        idx2=maskIdx(random('unid',numel(maskIdx),1));

        %fprintf(' -- Point1: ');
        %[p1(1),p1(2),p1(3)]=ind2sub(size(mask),idx1);
        %[p2(1),p2(2),p2(3)]=ind2sub(size(mask),idx2);
        %fprintf('%d\t',p1);
        %fprintf('\n -- Point2: ');
        %fprintf('%d\t',p2);
        %fprintf('\n');
        if numel(idx2)==0
            %fprintf(' -- ERROR: %d iteration\n',i)
            errors=errors+1;
            continue
        end

        %% compute roi1 and roi2
        roi1=randomROI(idx1,mask1,s);
        roi2=randomROI(idx2,mask2,s);
        
        if numel(roi1)==0 || numel(roi2)==0
            %fprintf(' -- ERROR: %d iteration\n',i)
            errors=errors+1;
            
        end
        %if mod(errors,iters/10)==0
            %fprintf(' ** WARNING: %d repetitions %d iters, please check your regions\n',errors,iters);
        %end
    end
    
    
    %% Once we have the two regions 
    mean1(i)=mean(ima(roi1));
    mean2(i)=mean(ima(roi2));
                
end

out.mean1 =mean1;
out.mean2 =mean2;

