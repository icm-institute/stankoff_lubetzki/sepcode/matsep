function ExtractValues(x,y,z,inimg,ofile)
    
    [img,hdr] = readNIFTI(inimg,'1');
    
    cx = str2num(x);
    cy = str2num(y);
    cz = str2num(z);
    
    for i=1:length(cx)
        
        Voxel = img(cx(i),cy(i),cz(i));
        dlmwrite(char(ofile),[cx(i) cy(i) cz(i) Voxel],'delimiter',' ','-append');
    end

end
